#include "mogli/core/Storage.h"

#include <cassert>
#include <numeric>

#include "mogli/Version.h"

namespace mogli {
namespace core {

Storage::Storage(const std::string& filename) : filename(filename) {
  num_Trajectories = 0;
  numStateVectorLists = 0;
  openFile(filename);
}

Storage::~Storage() {
  if (closeFile() < 0) {
    std::cerr << "Error closing file " << filename << std::endl;
  }
}

hid_t Storage::openFile(const std::string& filename) {
  hdf5file =
      H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  trajectoryGroup = H5Gcreate2(hdf5file, "/Trajectories", H5P_DEFAULT,
                               H5P_DEFAULT, H5P_DEFAULT);
  stateVectorListGroup = H5Gcreate2(hdf5file, "/StateVectorList", H5P_DEFAULT,
                                    H5P_DEFAULT, H5P_DEFAULT);
  writeVersionInfo(hdf5file, "/Version");
  return hdf5file;
}

herr_t Storage::closeFile() {
  // Store number of trajectories when closing the file
  h5details::addAttribute(trajectoryGroup, "NumTrajectories", num_Trajectories);

  H5Gclose(trajectoryGroup);
  H5Gclose(stateVectorListGroup);
  herr_t f_err = H5Fclose(hdf5file);
  return f_err;
}

herr_t Storage::writeVersionInfo(hid_t hdf5file, const std::string& groupName) {
  hid_t versionGroup = H5Gcreate2(hdf5file, groupName.c_str(), H5P_DEFAULT,
                                  H5P_DEFAULT, H5P_DEFAULT);

  const std::string version = Version::getVersion();
  h5details::addAttribute(versionGroup, "Version", version);

  const std::string gitCommitSHA1 = Version::getGitCommitSHA1();
  h5details::addAttribute(versionGroup, "GitCommitSHA1", gitCommitSHA1);

  const auto gitPopulated = static_cast<int>(Version::isGitPopulated());
  h5details::addAttribute(versionGroup, "GitPopulated", gitPopulated);

  const auto gitDirty = static_cast<int>(Version::isGitDirty());
  h5details::addAttribute(versionGroup, "GitDirty", gitDirty);

  herr_t error = H5Gclose(versionGroup);
  return error;
}

h5details::h5Dataset Storage::writeBuffer(const std::string& datasetName,
                                          const hid_t group,
                                          const Dimensions& dimensions,
                                          const Buffer<double>& buffer) {
  assert(buffer.size() == std::accumulate(dimensions.begin(), dimensions.end(),
                                          hsize_t(1), std::multiplies<>()));

  // Make dataspace
  hid_t dataspace =
      H5Screate_simple(dimensions.size(), dimensions.data(), nullptr);

  hid_t datatype = H5Tcopy(H5T_NATIVE_DOUBLE);
  hid_t dataset = H5Dcreate2(group, datasetName.c_str(), datatype, dataspace,
                             H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  H5Dwrite(dataset, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data());

  return h5details::h5Dataset(dataset, dataspace, datatype);
}

namespace h5details {
h5Dataset::h5Dataset(hid_t dataset, hid_t dataspace, hid_t datatype) {
  this->dataset = dataset;
  this->dataspace = dataspace;
  this->datatype = datatype;
}
h5Dataset::~h5Dataset() {
  H5Dclose(dataset);
  H5Sclose(dataspace);
  H5Tclose(datatype);
}
}  // namespace h5details

}  // namespace core
}  // namespace mogli