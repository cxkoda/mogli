//
// Created by patrick on 05.03.20.
//
#include "mogli/ode/MetricMinkowski.h"

#include <cmath>

namespace mogli {
namespace ode {

MetricMinkowski::MetricMinkowski() {
  /**
   * shift is contravariant
   * spatial metric is covariant
   * inverse spatial metric is contravariant
   *
   */
  lapse = 1;
  for (int i = 0; i < 3; i++) {
    shift[i] = 0;
    dLapse[i] = 0;
    for (int j = 0; j < 3; j++) {
      dShift[i][j] = 0;
      spatialMetric[i][j] = 0;
      spatialMetricInv[i][j] = 0;
      for (int k = 0; k < 3; k++) {
        dSpatialMetricInv[i][j][k] = 0;
      }
    }
  }
  spatialMetric[0][0] = 1;
  spatialMetric[1][1] = 1;
  spatialMetric[2][2] = 1;

  spatialMetricInv[0][0] = 1;
  spatialMetricInv[1][1] = 1;
  spatialMetricInv[2][2] = 1;
}

std::array<double, 4>
MetricMinkowski::transformLif2Lab(  // uebernommen von schwarzschild!!
    const std::array<double, 4>& u_lif) {
  std::array<double, 4> u_lab = {0, 0, 0, 0};

  transformMatrix[0][0] = 1 / lapse;                      // sqrt(r/(r-rs));
  transformMatrix[1][1] = lapse;                          // sqrt((r-rs)/r);
  transformMatrix[2][2] = 1 / sqrt(spatialMetric[1][1]);  // 1/r;
  transformMatrix[3][3] = 1 / sqrt(spatialMetric[2][2]);  // 1/(r*sin(theta));

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      u_lab[i] += transformMatrix[i][j] * u_lif[j];
    }
  }

  return u_lab;
}

void MetricMinkowski::setPosition(
    [[maybe_unused]] const double time,
    [[maybe_unused]] const mogli::ode::Metric::Position& position) {
  lapse = 1;
  for (int i = 0; i < 3; i++) {
    shift[i] = 0;
    dLapse[i] = 0;
    for (int j = 0; j < 3; j++) {
      dShift[i][j] = 0;
      spatialMetric[i][j] = 0;
      spatialMetricInv[i][j] = 0;
      for (int k = 0; k < 3; k++) {
        dSpatialMetricInv[i][j][k] = 0;
      }
    }
  }
  spatialMetric[0][0] = 1;
  spatialMetric[1][1] = 1;
  spatialMetric[2][2] = 1;

  spatialMetricInv[0][0] = 1;
  spatialMetricInv[1][1] = 1;
  spatialMetricInv[2][2] = 1;
}

}  // namespace ode
}  // namespace mogli
