//
// Created by patrick on 05.03.20.
// dummy file for correct structure from Exponential.cpp
// note: this should be 2d (exp was 1d)
//
#include "mogli/ode/LotkaVolterra.h"

#include <cmath>

namespace mogli {
namespace ode {

LotkaVolterra::LotkaVolterra(const double& eps1,
                             const double& eps2,
                             const double& gamma1,
                             const double& gamma2) {
  this->eps1 = eps1;
  this->eps2 = eps2;
  this->gamma1 = gamma1;
  this->gamma2 = gamma2;
}

LotkaVolterra::RhsVector LotkaVolterra::getRhs(
    const StateVector& state,
    [[maybe_unused]] const double& time) {
  RhsVector RHS;
  // RHS[0] = lambda * state[0];
  RHS[0] = state[0] * (eps1 - gamma1 * state[1]);
  RHS[1] = -state[1] * (eps2 - gamma2 * state[0]);
  return RHS;
}

}  // namespace ode
}  // namespace mogli
