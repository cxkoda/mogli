#include "mogli/ode/Polynomial.h"

#include <numeric>

namespace mogli {
namespace ode {

Polynomial::Polynomial(const Coefficients& coefficients)
    : coefficients(coefficients),
      integralCoefficients(getIntegratedCoefficients(coefficients)) {}

Polynomial::RhsVector Polynomial::getRhs(
    [[maybe_unused]] const StateVector& state,
    const double& time) {
  RhsVector rhs;
  rhs[0] = applyHornersScheme(time, coefficients);
  return rhs;
}

Polynomial::StateVector Polynomial::getAnalytical(
    [[maybe_unused]] const StateVector& initialState,
    const double& time) const {
  StateVector analyticalSolution;
  analyticalSolution[0] = initialState[0];
  analyticalSolution[0] += applyHornersScheme(time, integralCoefficients);
  return analyticalSolution;
}

double Polynomial::applyHornersScheme(const double position,
                                      const Coefficients& coefficients) {
  return std::accumulate(std::next(coefficients.rbegin()), coefficients.rend(),
                         coefficients.back(), [=](auto sum, auto coeff) {
                           return coeff + position * std::move(sum);
                         });
}

Polynomial::Coefficients Polynomial::getIntegratedCoefficients(
    const Coefficients& coefficients) {
  Coefficients integralCoefficients(coefficients.size() + 1);
  integralCoefficients[0] = 0;

  size_t idx = 1;
  for (const auto coeff : coefficients) {
    integralCoefficients[idx] = coeff / static_cast<double>(idx);
    ++idx;
  }

  return integralCoefficients;
}

}  // namespace ode
}  // namespace mogli