//
// Created by patri on 21.03.2020.
//

#include "mogli/ode/Geodesic.h"

#include <cmath>

namespace mogli {
namespace ode {

Geodesic::Geodesic(const int& epsilon, Metric& metric) {
  this->epsilon = epsilon;
  this->metric = &metric;
}

Geodesic::RhsVector Geodesic::getRhs(const StateVector& state,
                                     [[maybe_unused]] const double& time) {
  RhsVector RHS;
  Metric::Position position;
  position[0] = state[0];
  position[1] = state[1];
  position[2] = state[2];

  metric->setPosition(time, position);

  auto l = metric->getLapse();               // alpha
  auto s = metric->getShift();               // beta^i
  auto smi = metric->getSpatialMetricInv();  // gamma^ij
  auto dl = metric->getDLapse();
  auto ds = metric->getDShift();
  auto dsmi = metric->getDSpatialMetricInv();

  double u0 = epsilon;
  double u[3] = {0, 0, 0};

  for (int i = 0; i < 3; i++) {
    u[i] = state[i + 3];
  }

  for (int j = 0; j < 3; j++) {
    for (int k = 0; k < 3; k++) {
      u0 += smi[j][k] * u[j] * u[k];
    }
  }

  u0 = sqrt(u0) / l;
  for (int i = 0; i < 3; i++) {
    RHS[i] = -s[i];
    for (int j = 0; j < 3; j++) {
      RHS[i] += smi[i][j] * u[j] / u0;
    }
  }

  for (int i = 0; i < 3; i++) {
    RHS[i + 3] = -l * u0 * dl[i];

    for (int k = 0; k < 3; k++) {
      RHS[i + 3] += u[k] * ds[i][k];
    }

    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 3; k++) {
        RHS[i + 3] += -u[j] * u[k] / (2 * u0) * dsmi[i][j][k];
      }
    }
  }
  if (time == 0 && false) {
    std::cout << "GEODESIC.CPP_DEBUG_OUTPUT:\n";
    std::cout << "dsmi[2][2][2]=";
    std::cout << dsmi[2][0][0];
    std::cout << "\n";
    std::cout << "RHS[3]=";
    std::cout << RHS[3];
    std::cout << "\n";
    std::cout << "RHS[4]=";
    std::cout << RHS[4];
    std::cout << "\n";
    std::cout << "RHS[5]=";
    std::cout << RHS[5];
    std::cout << "\n---------------\n";
  }
  return RHS;
}

}  // namespace ode
}  // namespace mogli
