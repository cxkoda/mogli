//
// Created by patrick on 05.03.20.
//
#include "mogli/ode/MetricSchwarzschild.h"

#include <cmath>

namespace mogli {
namespace ode {

MetricSchwarzschild::MetricSchwarzschild(const double mass) : mass(mass) {
  // initialize as a minkowski
  lapse = 1;
  for (int i = 0; i < 3; i++) {
    shift[i] = 0;
    dLapse[i] = 0;
    for (int j = 0; j < 3; j++) {
      dShift[i][j] = 0;
      spatialMetric[i][j] = 0;
      spatialMetricInv[i][j] = 0;
      for (int k = 0; k < 3; k++) {
        dSpatialMetricInv[i][j][k] = 0;
      }
    }
  }
  spatialMetric[0][0] = 1;
  spatialMetric[1][1] = 1;
  spatialMetric[2][2] = 1;

  spatialMetricInv[0][0] = 1;
  spatialMetricInv[1][1] = 1;
  spatialMetricInv[2][2] = 1;

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      transformMatrix[i][j] = 0;
    }
  }
}

std::array<double, 4> MetricSchwarzschild::transformLif2Lab(
    const std::array<double, 4>& u_lif) {
  std::array<double, 4> u_lab = {0, 0, 0, 0};

  transformMatrix[0][0] = 1 / lapse;                      // sqrt(r/(r-rs));
  transformMatrix[1][1] = lapse;                          // sqrt((r-rs)/r);
  transformMatrix[2][2] = 1 / sqrt(spatialMetric[1][1]);  // 1/r;
  transformMatrix[3][3] = 1 / sqrt(spatialMetric[2][2]);  // 1/(r*sin(theta));

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      u_lab[i] += transformMatrix[i][j] * u_lif[j];
    }
  }

  return u_lab;
}

void MetricSchwarzschild::setPosition(
    [[maybe_unused]] const double time,
    const mogli::ode::Metric::Position& position) {
  // position[0] ... r
  // position[1] ... theta
  // position[2] ... phi
  const double r = position[0];
  const double theta = position[1];
  const double rs = 2 * mass;

  const double r2 = r * r;
  const double r3 = r * r2;

  const double sinTheta = sin(theta);
  const double sinTheta2 = sinTheta * sinTheta;
  const double sinTheta3 = sinTheta * sinTheta2;

  lapse = sqrt(1. - rs / r);
  dLapse[0] = rs / (2. * r2 * lapse);
  spatialMetric[0][0] = r / (r - rs);
  spatialMetric[1][1] = r2;
  spatialMetric[2][2] = r2 * sinTheta2;

  spatialMetricInv[0][0] = 1. - rs / r;
  spatialMetricInv[1][1] = 1. / r2;
  spatialMetricInv[2][2] = 1. / (r2 * sinTheta2);

  dSpatialMetricInv[0][0][0] = rs / r2;
  dSpatialMetricInv[0][1][1] = -2. / r3;
  dSpatialMetricInv[0][2][2] = -2. / (r3 * sinTheta2);
  dSpatialMetricInv[1][2][2] = -2. / r2 * cos(theta) / sinTheta3;
}

}  // namespace ode
}  // namespace mogli
