//
// Created by patrick on 05.03.20.
//
#include "mogli/ode/MetricKerr.h"

#include <cmath>
#include <iostream>

namespace mogli {
namespace ode {

MetricKerr::MetricKerr(const double mass, const double angularMoment)
    : mass(mass), angularMoment(angularMoment) {
  // initialize as a minkowski
  lapse = 1;
  for (int i = 0; i < 3; i++) {
    shift[i] = 0;
    dLapse[i] = 0;
    for (int j = 0; j < 3; j++) {
      dShift[i][j] = 0;
      spatialMetric[i][j] = 0;
      spatialMetricInv[i][j] = 0;
      for (int k = 0; k < 3; k++) {
        dSpatialMetricInv[i][j][k] = 0;
      }
    }
  }
  spatialMetric[0][0] = 1;
  spatialMetric[1][1] = 1;
  spatialMetric[2][2] = 1;

  spatialMetricInv[0][0] = 1;
  spatialMetricInv[1][1] = 1;
  spatialMetricInv[2][2] = 1;

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      transformMatrix[i][j] = 0;
    }
  }
}

std::array<double, 4> MetricKerr::transformLif2Lab(
    const std::array<double, 4>& u_lif) {
  std::array<double, 4> u_lab = {0, 0, 0, 0};
  const double r = position[0];
  const double theta = position[1];
  const double rs = 2 * mass;
  const double a = angularMoment;
  double Sigma = spatialMetric[1][1];
  double Delta = r * r - rs * r + a * a;
  double A = (r * r + a * a) * Sigma + rs * a * a * r * sin(theta) * sin(theta);
  double w = rs * a * r / A;

  transformMatrix[0][0] = sqrt(A / Sigma / Delta);
  transformMatrix[3][0] = sqrt(A / Sigma / Delta) * w;
  transformMatrix[1][1] = sqrt(Delta / Sigma);
  transformMatrix[2][2] = 1. / sqrt(Sigma);
  transformMatrix[3][3] = sqrt(Sigma / A) / sin(theta);

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      u_lab[i] += transformMatrix[i][j] * u_lif[j];
    }
  }

  // alternative implementation of mueller paper starting vectors:
  // not working but returns correct starting angle for intial direction (so can
  // be used to calculate beta0 for photon sphere with parameters Q , Phi and L
  // as given in some papers)
  /*
  double Einv=0;
  const double Phi=-1;
  double Q = -13.+16.*sqrt(2.);
  Q = 12.+8.*sqrt(3.);
  Einv = transformMatrix[0][0] - rs * r * a / sqrt(A*Sigma*Delta) * Phi;
  double O = Q/Einv/Einv;
  double L = Phi/Einv;

  //u_lab[0]=1; //for lightrays
  //u_lab[1]=0; //if lightray is on sphere
  //u_lab[2]=sqrt((O-cos(theta)*cos(theta) *
  (-a*a/Einv/Einv+L*L/sin(theta)/sin(theta)))/Sigma);
  //u_lab[3]=sqrt(Sigma/A)* L/sin(theta);

  //double winkel=atan2(u_lab[2],u_lab[3])*180./M_PI;
  //std::cout.precision(10);
  //std::cout<< winkel;
  */
  return u_lab;
}

void MetricKerr::setPosition([[maybe_unused]] const double time,
                             const mogli::ode::Metric::Position& position) {
  // position[0] ... r
  // position[1] ... theta
  // position[2] ... phi
  this->position = position;
  const double r = position[0];
  const double theta = position[1];
  //  const double rs = 2 * mass;
  const double M = mass;
  const double a = angularMoment;
  const double r2 = r * r;
  const double a2 = a * a;
  //  const double M2 = M * M;
  const double sinTheta = sin(theta);
  const double sinTheta2 = sinTheta * sinTheta;
  const double cosTheta = cos(theta);
  const double cosTheta2 = cosTheta * cosTheta;

  lapse = sqrt(
      1 - (2 * M * r) / (r2 + a2 * cosTheta2) +
      (4 * a2 * pow(M, 2) * r2 * sinTheta2) /
          (pow(r2 + a2 * cosTheta2, 2) *
           (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))));
  shift[2] = (-2 * a * M * r) /
             ((r2 + a2 * cosTheta2) *
              (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)));

  spatialMetric[0][0] = (r2 + a2 * cosTheta2) / (a2 - 2 * M * r + r2);
  spatialMetric[1][1] = r2 + a2 * cosTheta2;
  spatialMetric[2][2] =
      sinTheta2 *
      (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2));
  spatialMetricInv[0][0] = (a2 - 2 * M * r + r2) / (r2 + a2 * cosTheta2);
  spatialMetricInv[1][1] = 1 / (r2 + a2 * cosTheta2);
  spatialMetricInv[2][2] =
      pow(1 / sinTheta, 2) /
      (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2));

  dLapse[0] =
      ((4 * M * r2) / pow(r2 + a2 * cosTheta2, 2) -
       (2 * M) / (r2 + a2 * cosTheta2) -
       (4 * a2 * pow(M, 2) * r2 * sinTheta2 *
        (2 * r - (4 * a2 * M * r2 * sinTheta2) / pow(r2 + a2 * cosTheta2, 2) +
         (2 * a2 * M * sinTheta2) / (r2 + a2 * cosTheta2))) /
           (pow(r2 + a2 * cosTheta2, 2) *
            pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2),
                2)) -
       (16 * a2 * pow(M, 2) * pow(r, 3) * sinTheta2) /
           (pow(r2 + a2 * cosTheta2, 3) *
            (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))) +
       (8 * a2 * pow(M, 2) * r * sinTheta2) /
           (pow(r2 + a2 * cosTheta2, 2) *
            (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)))) /
      (2. * sqrt(1 - (2 * M * r) / (r2 + a2 * cosTheta2) +
                 (4 * a2 * pow(M, 2) * r2 * sinTheta2) /
                     (pow(r2 + a2 * cosTheta2, 2) *
                      (a2 + r2 +
                       (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)))));
  dLapse[1] =
      ((-4 * a2 * M * r * cosTheta * sinTheta) / pow(r2 + a2 * cosTheta2, 2) +
       (8 * a2 * pow(M, 2) * r2 * cosTheta * sinTheta) /
           (pow(r2 + a2 * cosTheta2, 2) *
            (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))) +
       (16 * pow(a, 4) * pow(M, 2) * r2 * cosTheta * pow(sinTheta, 3)) /
           (pow(r2 + a2 * cosTheta2, 3) *
            (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))) -
       (4 * a2 * pow(M, 2) * r2 * sinTheta2 *
        ((4 * a2 * M * r * cosTheta * sinTheta) / (r2 + a2 * cosTheta2) +
         (4 * pow(a, 4) * M * r * cosTheta * pow(sinTheta, 3)) /
             pow(r2 + a2 * cosTheta2, 2))) /
           (pow(r2 + a2 * cosTheta2, 2) *
            pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2),
                2))) /
      (2. * sqrt(1 - (2 * M * r) / (r2 + a2 * cosTheta2) +
                 (4 * a2 * pow(M, 2) * r2 * sinTheta2) /
                     (pow(r2 + a2 * cosTheta2, 2) *
                      (a2 + r2 +
                       (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)))));

  dShift[0][2] =
      (2 * a * M * r *
       (2 * r - (4 * a2 * M * r2 * sinTheta2) / pow(r2 + a2 * cosTheta2, 2) +
        (2 * a2 * M * sinTheta2) / (r2 + a2 * cosTheta2))) /
          ((r2 + a2 * cosTheta2) *
           pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2),
               2)) +
      (4 * a * M * r2) /
          (pow(r2 + a2 * cosTheta2, 2) *
           (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))) -
      (2 * a * M) /
          ((r2 + a2 * cosTheta2) *
           (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)));
  dShift[1][2] =
      (-4 * pow(a, 3) * M * r * cosTheta * sinTheta) /
          (pow(r2 + a2 * cosTheta2, 2) *
           (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2))) +
      (2 * a * M * r *
       ((4 * a2 * M * r * cosTheta * sinTheta) / (r2 + a2 * cosTheta2) +
        (4 * pow(a, 4) * M * r * cosTheta * pow(sinTheta, 3)) /
            pow(r2 + a2 * cosTheta2, 2))) /
          ((r2 + a2 * cosTheta2) *
           pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2),
               2));

  dSpatialMetricInv[0][0][0] =
      (-2 * r * (a2 - 2 * M * r + r2)) / pow(r2 + a2 * cosTheta2, 2) +
      (-2 * M + 2 * r) / (r2 + a2 * cosTheta2);

  dSpatialMetricInv[0][1][1] = (-2 * r) / pow(r2 + a2 * cosTheta2, 2);
  dSpatialMetricInv[0][2][2] =
      -((pow(1 / sinTheta, 2) *
         (2 * r - (4 * a2 * M * r2 * sinTheta2) / pow(r2 + a2 * cosTheta2, 2) +
          (2 * a2 * M * sinTheta2) / (r2 + a2 * cosTheta2))) /
        pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2), 2));

  dSpatialMetricInv[1][0][0] =
      (2 * a2 * (a2 - 2 * M * r + r2) * cosTheta * sinTheta) /
      pow(r2 + a2 * cosTheta2, 2);
  dSpatialMetricInv[1][1][1] =
      (2 * a2 * cosTheta * sinTheta) / pow(r2 + a2 * cosTheta2, 2);
  dSpatialMetricInv[1][2][2] =
      (-2 * 1 / tan(theta) * pow(1 / sinTheta, 2)) /
          (a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2)) -
      (pow(1 / sinTheta, 2) *
       ((4 * a2 * M * r * cosTheta * sinTheta) / (r2 + a2 * cosTheta2) +
        (4 * pow(a, 4) * M * r * cosTheta * pow(sinTheta, 3)) /
            pow(r2 + a2 * cosTheta2, 2))) /
          pow(a2 + r2 + (2 * a2 * M * r * sinTheta2) / (r2 + a2 * cosTheta2),
              2);
}
/*
void MetricKerr::setPosition(
    [[maybe_unused]] const double time,
    const mogli::ode::Metric::Position& position) {
  // position[0] ... r
  // position[1] ... theta
  // position[2] ... phi
  const double r = position[0];
  const double theta = position[1];
  const double rs = 2 * mass;
  const double M2 = mass * mass;
  const double M3 = M2 * mass;
  const double M4 = M2*M2;
  const double M6 = M3*M3;

  auto J=angularMoment;
  const double J2 = J * J;
  const double J4= J2*J2;
  const double J6 = J4*J2;
  const double a = J / mass;
  const double r2 = r * r;
  const double r3 = r * r2;
  const double r6=r3*r3;

  const double sinTheta = sin(theta);
  const double sinTheta2 = sinTheta * sinTheta;
  const double sinTheta3 = sinTheta * sinTheta2;

  const double cosTheta = cos(theta);
  const double cos2Theta = cos(2*theta);
  const double cosTheta2 = cosTheta * cosTheta;
  const double cos2Theta2 = cos2Theta * cos2Theta;
  const double cosTheta3 = cosTheta * cosTheta2;

  const double temp1 = (r2 + (J*J *cosTheta2)/mass*mass);
  const double temp3 = (J2/M2 - 2* mass* r + r2);
  const double temp4 = (J2/M2 + r2)*(J2/M2 + r2);
  const double temp5 = cosTheta*sinTheta;
  const double temp6 = (temp4 - (J2* temp3* sinTheta2)/M2);

  const double g00 = -1 + (2 * mass * r)/(r2 + (J*J *cosTheta2)/mass*mass);
  const double g03 = -((2 * J * r * sinTheta2)/(r2 + (J*J
*cosTheta2)/mass*mass));

  spatialMetric[0][0] = (r2 + (J*J *cosTheta2)/mass*mass)/(J*J/mass/mass - 2 *
mass * r + r2); spatialMetric[1][1] = (r2 + (J*J *cosTheta2)/mass*mass);
  spatialMetric[2][2] = (sinTheta2 *((J2/M2 + r2)*(J2/M2 + r2) - (
      J2 *(J2/M2 - 2 * mass * r + r2)* sinTheta2/M2))/(r2 + (J2
*cosTheta2)/M2));

  spatialMetricInv[0][0] = (pow(J,2)/pow(mass,2) - 2*mass*r + pow(r,2))/
                           (pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2)); spatialMetricInv[1][1] = 1/(pow(r,2)
+ (pow(J,2)*pow(cos(theta),2))/pow(mass,2)); spatialMetricInv[2][2] = ((pow(r,2)
+ (pow(J,2)*pow(cos(theta),2))/pow(mass,2))*pow(1/sin(theta),2))/
                           (pow(pow(J,2)/pow(mass,2) + pow(r,2),2) -
                            (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2));


  Shift shiftCov = {0, 0, g03};
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      shift[i] += spatialMetricInv[i][j] * shiftCov[j];
    }
  }

  lapse = sqrt(1 - (2*mass*r)/(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2)) +
      (4*pow(J,2)*pow(r,2)*pow(sin(theta),2))/
  ((pow(r,2) + (pow(J,2)*pow(cos(theta),2))/pow(mass,2))*
      (pow(pow(J,2)/pow(mass,2) + pow(r,2),2) -
       (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2)))); dLapse[0] = (M3 *pow((J2 + 2 * M2
* r2 + J2 * cos2Theta),2) *(-J6 + 2* M6* r6 + J2 * M4 * r3 * (-4* mass + 3* r) -
J2* (J4 + 2* J2 *M2* r2 + M4* r3* (-4*mass + r))* cos2Theta))/(8* (M2* r2 + J2*
cosTheta2)*(M2* r2 + J2* cosTheta2) * pow(((J2 + M2* r2)*(J2 + M2* r2) - J2 *(J2
+ M2* r* (-2* mass + r))* sinTheta2),2)* sqrt( 1 - (2* mass* r)/(r2 + (J2*
cosTheta2)/M2) + (4* J2* M6* r2* sinTheta2)/((M2* r2 + J2* cosTheta2)* ((J2 + M2
*r2)*(J2 + M2 *r2) - J2 *(J2 + M2* r* (-2* mass + r))* sinTheta2))));

  dLapse[1]=(-((4* J2* r* temp5)/(mass* temp1*temp1)) + (8* J4* r2*
temp3*temp5*sinTheta2)/( M2* temp1* pow(temp6,2)) + (8* J2* r2* temp5)/((r2 +
(J2* cosTheta2)/ M2) *temp6) + (8* J4* r2* pow(temp5,3)/(M2* temp1*temp1
*temp6)))/ (2* sqrt(1 - (2*mass*r)/temp1 + (4*J2* r2* sinTheta2)/((r2 + (J2*
cosTheta2)/ M2) *(temp4 - (J2* temp3*sinTheta2)/M2))));

  dShift[0][2]=(2*J*r*(4*r*(pow(J,2)/pow(mass,2) + pow(r,2)) -
                       (pow(J,2)*(-2*mass +
2*r)*pow(sin(theta),2))/pow(mass,2)))/ pow(pow(pow(J,2)/pow(mass,2) +
pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2),2) - (2*J)/ (pow(pow(J,2)/pow(mass,2)
+ pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2));

  dShift[1][2]=(-4*pow(J,3)*r*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*cos(theta)*sin(theta))/ (pow(mass,2)*pow(pow(pow(J,2)/pow(mass,2) +
pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2),2));


  dSpatialMetricInv[0][0][0] = (-2*r*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2)))/ pow(pow(r,2) + (pow(J,2)*pow(cos(theta),2))/pow(mass,2),2) +
                               (-2*mass + 2*r)/(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2));

  dSpatialMetricInv[1][0][0] =(2*pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*cos(theta)*sin(theta))/ (pow(mass,2)*pow(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2),2));

  dSpatialMetricInv[0][1][1] =(-2*r)/pow(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2),2);

  dSpatialMetricInv[1][1][1] =(2*pow(J,2)*cos(theta)*sin(theta))/
                              (pow(mass,2)*pow(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2),2));


  dSpatialMetricInv[0][2][2] = -(((pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2))*pow(1/sin(theta),2)*
                                  (4*r*(pow(J,2)/pow(mass,2) + pow(r,2)) -
                                   (pow(J,2)*(-2*mass +
2*r)*pow(sin(theta),2))/pow(mass,2)))/ pow(pow(pow(J,2)/pow(mass,2) +
pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r + pow(r,2))*
                                        pow(sin(theta),2))/pow(mass,2),2)) +
                               (2*r*pow(1/sin(theta),2))/
                               (pow(pow(J,2)/pow(mass,2) + pow(r,2),2) -
                                (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2)); dSpatialMetricInv[1][2][2] =
(2*pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r + pow(r,2))* (pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2))*1/tan(theta))/
                               (pow(mass,2)*pow(pow(pow(J,2)/pow(mass,2) +
pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r + pow(r,2))*
                                                  pow(sin(theta),2))/pow(mass,2),2))
- (2*pow(J,2)*1/tan(theta))/ (pow(mass,2)*(pow(pow(J,2)/pow(mass,2) +
pow(r,2),2) - (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r + pow(r,2))*
                                             pow(sin(theta),2))/pow(mass,2))) -
                               (2*(pow(r,2) +
(pow(J,2)*pow(cos(theta),2))/pow(mass,2))*1/tan(theta)* pow(1/sin(theta),2))/
                               (pow(pow(J,2)/pow(mass,2) + pow(r,2),2) -
                                (pow(J,2)*(pow(J,2)/pow(mass,2) - 2*mass*r +
pow(r,2))*pow(sin(theta),2))/ pow(mass,2));
}
*/
}  // namespace ode
}  // namespace mogli
