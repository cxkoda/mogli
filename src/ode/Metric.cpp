//
// Created by patrick on 05.03.20.
//
#include "mogli/ode/Metric.h"

#include <cmath>
#include <numeric>

namespace mogli {
namespace ode {

double Metric::compute_u0_contra(const std::array<double, 3> u_i_contra,
                                 const int epsilon) {
  /**
   * shift = konvariant
   * inverse spatial metric
   */
  Shift shiftCov = {0, 0, 0};
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      shiftCov[i] += spatialMetric[i][j] * shift[j];
    }
  }

  const double g_00 =
      -lapse * lapse +
      std::inner_product(shiftCov.begin(), shiftCov.end(), shift.begin(), 0.);

  const double shiftUiContraction = std::inner_product(
      shiftCov.begin(), shiftCov.end(), u_i_contra.begin(), 0.);

  double gammaUiContraction = 0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      gammaUiContraction += spatialMetric[i][j] * u_i_contra[i] * u_i_contra[j];
    }
  }

  const double u_0_contra =
      -(shiftUiContraction + sqrt(shiftUiContraction * shiftUiContraction -
                                  g_00 * (gammaUiContraction + epsilon))) /
      g_00;

  return u_0_contra;
}

std::array<double, 4> Metric::compute_u_i_cov(
    const std::array<double, 3>& u_i_contra,
    const double u0_contra) {
  std::array<double, 4> u_4_contra = {
      u0_contra, u_i_contra[0], u_i_contra[1],
      u_i_contra[2]};  // kontravariante vierer geschw.

  std::array<double, 4> u_4_cov = {0, 0, 0, 0};

  /**
   * shift = kontravariant <-- wird benötigt
   * shift_i = kovariant <-- wird berechnet
   * smi = inverse spatial metric (kontravariant) <-- wird benötigt
   * sm = spatial metric (kovariant) <-- wird benötigt
   * g kovariant metric
   */
  double g[4][4] = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
  // auto& sm = spatialMetric;
  // auto &smi = spatialMetricInv;

  /**
   * calculate g00
   */
  g[0][0] = -lapse * lapse;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      g[0][0] += spatialMetric[i][j] * shift[i] * shift[j];
    }
  }
  /**
   * calculate shift_i needed in g_{i,j}
   */
  Shift shift_i;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      shift_i[i] += shift[j] * spatialMetric[i][j];
    }
  }

  for (int i = 0; i < 3; i++) {
    g[0][i + 1] = shift_i[i];
    g[i + 1][0] = shift_i[i];
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      g[i + 1][j + 1] = spatialMetric[i][j];
    }
  }

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      u_4_cov[i] += g[i][j] * u_4_contra[j];
    }
  }
  return u_4_cov;
}

}  // namespace ode
}  // namespace mogli
