cmake_minimum_required(VERSION 3.11)
project(mogli VERSION 0.1.0 LANGUAGES C CXX)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

# Options ######################################################################

if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
  option(MOGLI_BUILD_TESTING "Enable to build unit tests" ON)
  option(MOGLI_BUILD_APPS "Enable to build apps" ON)
else()
  option(MOGLI_BUILD_TESTING "Enable to build unit tests" OFF)
  option(MOGLI_BUILD_APPS "Enable to build apps" OFF)
endif ()
option(MOGLI_ENABLE_COVERAGE "Enable to build with code coverage instrumentation" OFF)


# Main project setup ###########################################################

if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
  # Enable testing if this is the main program
  include(CTest)

  if (NOT MOGLI_BUILD_TESTING)
    set(MOGLI_ENABLE_COVERAGE OFF)
  endif ()

  if (MOGLI_ENABLE_COVERAGE)
    set(CMAKE_BUILD_TYPE Debug)
  endif ()

  #set(CMAKE_VERBOSE_MAKEFILE TRUE)
endif ()


# External Dependencies ########################################################

find_package(HDF5 REQUIRED)

# FetchContent added in CMake 3.11, downloads during the configure step
include(FetchContent)
# FetchContent_MakeAvailable was not added until CMake 3.14
if (${CMAKE_VERSION} VERSION_LESS 3.14)
  include(cmake/add_FetchContent_MakeAvailable.cmake)
endif ()


# Tests ########################################################################

if (MOGLI_BUILD_TESTING AND BUILD_TESTING)
  message(STATUS "Building ${PROJECT_NAME} tests")
  include(CTest)
  list(APPEND CMAKE_CTEST_COMMAND " --output-on-failure")

  # Valgrind #################################################################
  set(MEMORYCHECK_COMMAND_OPTIONS "${MEMORYCHECK_COMMAND_OPTIONS} --leak-check=full")
  set(MEMORYCHECK_COMMAND_OPTIONS "${MEMORYCHECK_COMMAND_OPTIONS} --track-fds=yes")
  set(MEMORYCHECK_COMMAND_OPTIONS "${MEMORYCHECK_COMMAND_OPTIONS} --trace-children=yes")
  set(MEMORYCHECK_COMMAND_OPTIONS "${MEMORYCHECK_COMMAND_OPTIONS} --error-exitcode=1")
  add_custom_target(memcheck COMMAND ${CMAKE_CTEST_COMMAND} --test-action memcheck)

  # Coverage #################################################################
  if (MOGLI_ENABLE_COVERAGE)
    message(STATUS "Enabling compilation with code coverage")
    include(CodeCoverage)
    append_coverage_compiler_flags()
    setup_target_for_coverage_gcovr_html(
        NAME coverage
        EXECUTABLE ${CMAKE_CTEST_COMMAND}
        EXCLUDE "${CMAKE_CURRENT_BINARY_DIR}/_deps/*" "app/*" "test/*"
    )
  endif ()

  add_subdirectory(test)
endif ()


# Building #####################################################################

if (MOGLI_BUILD_APPS)
  message(STATUS "Building ${PROJECT_NAME} apps")
  add_subdirectory(app)
endif ()

add_subdirectory(src)