from scipy import misc
import numpy as np
import matplotlib.pyplot as plt
import subprocess

#######################################################################
# remap picture given in longitude-latitude projection into
# other projection of surface of the sphere
# to be adapted by user:
# DPI -> setting of dpi of output image
# skyimage -> name of image to be read from disk
# projection_type -> type of surface projection of sphere
# Requirements: need imagemagick for convert to work 
# -> this can be removed, leading to no autotrim of images
#######################################################################
# Begin user edit
#######################################################################
DPI = 600
skyimage = misc.imread('Galaxy_small.jpg')
# choose type of projection
projection_type='hammer' # sensible values: hammer, aitoff (,mollweide)
#######################################################################
# End user edit
#######################################################################

# load individual color channels and convert to range 0..1
mapcubedata_r = skyimage[:,:,0]/256.
mapcubedata_g = skyimage[:,:,1]/256.
mapcubedata_b = skyimage[:,:,2]/256.

# get extent of image
lon_num = skyimage.shape[1]
lat_num = skyimage.shape[0]

print("Grid:",lon_num,lat_num)

# Make grid of longitudes an latitudes
lons = np.linspace(-np.pi, np.pi, lon_num+1)
lats = np.linspace(np.pi/2, -np.pi/2, lat_num+1)

lonGrid,latGrid = np.meshgrid(lons,lats)

# make full data map
fullmap = np.dstack((mapcubedata_r, mapcubedata_g, mapcubedata_b))

print("shape of full map:",fullmap.shape)

# hack to allow rgb color with pcolormesh
mycolor = fullmap.reshape((fullmap.shape[0]*fullmap.shape[1],fullmap.shape[2]))

# Choose type of projection. Sensible values are:
# hammer
# aitoff
# mollweide

plt.figure()
ax = plt.subplot(111, projection=projection_type)

m = plt.pcolormesh(lonGrid,latGrid,fullmap[:,:,0], color=mycolor)
m.set_array(None)
ax.set_xticks([], [])
ax.set_yticks([], [])


plt.savefig('ReProjection.jpg',dpi=DPI)

# trim white borders
bashCommand = "convert ReProjection.jpg -trim ReProjection.jpg"
process = subprocess.Popen(bashCommand.split())
output, error = process.communicate()