from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
from collections import OrderedDict
import matplotlib.pyplot as plt
import numpy as np
import os
import math
import time
import h5py


class Plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output


    def plot_polar_as_xy(self, filename, dim):
        config=np.loadtxt("config.tsv")
        print(config)
        # load the trajectory
        # Now, plot the thing
        # plot x-y:
        fig, ax = plt.subplots(1, 1)
        #ax.set_title('Trajektorie')
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_aspect('equal')
        map=np.zeros(0)

        def mysignal(coords):
            r=coords[0]
            theta=coords[1]
            phi=coords[2]
            if r < 10:
                return 0
            while theta>np.pi:
                theta=theta-np.pi
            while theta<0:
                theta=theta+np.pi

            while phi>2*np.pi:
                phi=phi-2*np.pi
            while phi<0:
                phi=phi+2*np.pi

            if theta>np.pi/2:
                if phi < np.pi*0.25:
                    return 1
                if phi < np.pi*0.5:
                    return 2
                if phi < np.pi*0.75:
                    return 3
                if phi < np.pi:
                    return 4
                if phi < np.pi*1.25:
                    return 5
                if phi < np.pi*1.5:
                    return 6
                if phi < np.pi*1.75:
                    return 7
                return 8
            else:
                if phi < np.pi*0.25:
                    return -1
                if phi < np.pi*0.5:
                    return -2
                if phi < np.pi*0.75:
                    return -3
                if phi < np.pi:
                    return -4
                if phi < np.pi*1.25:
                    return -5
                if phi < np.pi*1.5:
                    return -6
                if phi < np.pi*1.75:
                    return -7
                return -8
            #return np.cos(phi)*np.sin(theta)
            #return (phi / np.pi) % 2. > 1



        start_time=time.time()

        for j in range(int(config[3])):
            trajectoryDataAccess = tr_data(f"trajectory_cameraMapping_{j}.h5")
            data = (trajectoryDataAccess.get_trajectory(0))[:,:3]
            #for i in range(len(data)):
            #    map=np.append(map,mysignal(data[i][0],data[i][1],data[i][2]))
            arr=np.apply_along_axis(mysignal, 1, data)
            map=np.append(map,arr)

        map=np.reshape(map,(int(config[0]),int(config[1])))



        print("Matrix: ", (time.time()-start_time))
        start_time=time.time()
        cmaps = OrderedDict()
        ax.imshow(map,cmap=plt.get_cmap('Set1'))
        #print(map)

        figDirName = str(filename[:-3] + "_map.pdf")
        figName = os.path.split(figDirName)[1]

        print("Plot: ", (time.time()-start_time))

        plt.savefig(figName, transparent=True)
        plt.show()
        print('saved as', figName)


if __name__ == "__main__":
    # test the stuff
    plotter = Plotter()
    # include an exception bc an error occurs when the pdf is opened in another programm
    plotter.plot_polar_as_xy('trajectory_cameraMapping_0.h5', 6)
