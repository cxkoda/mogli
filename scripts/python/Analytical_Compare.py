import scipy.special as spec
import math
import sys
import numpy as np
import matplotlib.pyplot as plt

from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
from matplotlib import patches
from mpl_toolkits.mplot3d import Axes3D
import os


class Orbit(object):
    def __init__(self, r_init):
        self.deg2rad = math.pi / 180.
        self._r_init = r_init

    def get_xMin(self, asqr):
        aVal = math.sqrt(asqr)
        xMin = math.sqrt(3) / 2. * aVal
        xMin /= math.cos(math.acos(-3 * math.sqrt(3) * aVal / 2.) / 3.)
        return xMin

    def get_asqr(self, xobs, xi):
        # Compute square of a, Eq. (7)
        asqr = xobs ** 2 * (1. - xobs) / math.sin(xi) ** 2
        print("a^2", asqr, 4. / 27.)
        return asqr

    def get_theta(self, asqr):
        print(1. - 27. / 2. * asqr)
        theta = math.acos((1. - 27. / 2. * asqr)) / 3.
        return theta

    def get_f_ring(self, rad_n):
        x_n = 1. / rad_n
        print("Werte fuer f:", x_n, self._xi_ring)
        asqr = self.get_asqr(x_n, self._xi_ring)
        print("a^2", asqr)
        theta = self.get_theta(asqr)
        self.x1 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. - theta)
        self.x2 = 1. / 3. + 2. / 3. * math.cos(theta)
        self.x3 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. + theta)

        fVal = self._get_phi(x_n) - math.pi
        return fVal

    def get_r_ring(self, xi_ring, rad_n):
        # Compute x value for Einstein ringe for given xi
        self._xi_ring = xi_ring * self.deg2rad
        epsilon = 1.e-5

        for i_iter in range(20):
            f_n = self.get_f_ring(rad_n)
            f_n_plus = self.get_f_ring(rad_n + epsilon)
            f_n_minus = self.get_f_ring(rad_n - epsilon)
            rhs = 2 * f_n * epsilon / (f_n_plus - f_n_minus)
            print("rhs:", rhs, f_n, f_n_plus, f_n_minus, f_n_plus - f_n_minus)
            rad_n -= rhs
            print("r_n", rad_n)

        return rad_n

    def get_phi_rel(self, xi_init, rad):
        xi = xi_init * self.deg2rad
        self.xobs = 1. / self._r_init
        xVal = 1. / rad
        print("xobs:", self.xobs)
        asqr = self.get_asqr(self.xobs, xi)
        print("a^2", asqr)

        theta = self.get_theta(asqr)
        self.x1 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. - theta)
        self.x2 = 1. / 3. + 2. / 3. * math.cos(theta)
        self.x3 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. + theta)
        # self.x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
        # self.x2 = 1./3. + 2./3.*math.cos(theta)
        # self.x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)

        print("theta", theta)
        print("x1,x2,x3", self.x1, self.x2, self.x3)
        print("x", xVal)

        # Compute phi relative to initial position
        phi = self._get_phi(xVal) - self._get_phi(self.xobs)
        return phi

    def _get_phi(self, xVal):
        print("in", (xVal - self.x2) / (xVal - self.x1))
        par = math.sqrt((xVal - self.x2) / (xVal - self.x1))
        print("par", par)
        # parInit = math.sqrt((self.xobs - self.x2)/(xVal - self.x1))
        msqr = (self.x1 - self.x3) / (self.x2 - self.x3)
        mVal = math.sqrt(msqr)

        fac = 2. / math.sqrt(self.x2 - self.x3)

        print("parameters:", msqr, par, mVal)

        # print(par, msqr,fac, phi)
        # print(par, mVal,self.x1-self.x3, self.x2-self.x3)
        # phi = fac*spec.ellipkinc(math.sin(par),mVal)

        # Translate parameters according to appendix B
        if (par > 1 and mVal < 1 and par * mVal):
            xLawden = 1. / (par * mVal)
            kLawden = mVal
            prefac = 1.
        elif (mVal > 1):
            xLawden = par * mVal
            kLawden = 1. / mVal
            prefac = 1. / mVal
        else:
            xLawden = par
            kLawden = mVal
            prefac = 1.
        phiAS = math.asin(xLawden)
        mAS = kLawden ** 2

        phi = fac * prefac * spec.ellipkinc(phiAS, mAS)

        # phi = fac*spec.ellipkinc(par,msqr)

        # phiInit = fac*spec.ellipkinc(parInit,mVal)
        print("phi", phi)

        # return (phi-phiInit)/self.deg2rad
        return phi

    def get_rad(self, xi_init, phi, phi_init):

        xi = xi_init * self.deg2rad

        self.xobs = 1. / self._r_init

        asqr = self.get_asqr(self.xobs, xi)
        print("a^2", asqr)

        theta = self.get_theta(asqr)
        self.x1 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. - theta)
        self.x2 = 1. / 3. + 2. / 3. * math.cos(theta)
        self.x3 = 1. / 3. + 2. / 3. * math.cos(2. * math.pi / 3. + theta)
        print("x1,x2,x3", self.x1, self.x2, self.x3)

        phi_init = self._get_phi(self.xobs)
        print("init:", self.xobs, phi_init)
        phi = phi * self.deg2rad

        par = 0.5 * math.sqrt(self.x2 - self.x3) * (phi - phi_init)
        msqr = (self.x1 - self.x3) / (self.x2 - self.x3)
        mVal = math.sqrt(msqr)

        # Test, welche die inverse Transformation ist
        # print("Integrand",par)
        # intHin = spec.ellipkinc(par,msqr)
        # print("Hin:",intHin)
        # intRue = spec.ellipj(intHin, msqr)
        # print("Rueck",intRue)

        integ = spec.ellipj(par, msqr)
        # integ = spec.ellipj(par, mVal)
        print("integral:", integ)
        print("with sin", math.sin(integ[0]))
        # xVal = self.x2 - self.x1*integ[0]**2
        # xVal /= 1.-integ[0]**2
        xVal = self.x2 - self.x1 * integ[0] ** 2
        xVal /= 1. - integ[0] ** 2

        radCurr = 1. / xVal
        print("x:", xVal, "r:", radCurr)

        return radCurr

#trajectories
class Plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output
    def get_trajectory(self,filename, dim, num_of_tr=None):
        data_acc = tr_data(filename)
        num_of_tr = data_acc.get_num_trajectories()

        r_end=[]
        phi_end = []
        total_steps = []

        for i in range(num_of_tr):
            my_Data = self.get_col(data_acc.get_trajectory(i), dim)
            r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
            vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
            total_steps.append(len(r))
            r_end.append(r[-1])
            phi_end.append(phi[-1])
        return r_end, phi_end, total_steps

    def plot_polar_as_xy(self, filename, dim,num_of_tr=None):
        # load the trajectory
        data_acc = tr_data(filename)
        num_of_tr=data_acc.get_num_trajectories()

        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(1, 1, 1)

        for i in range(num_of_tr):
            my_Data = self.get_col(data_acc.get_trajectory(i), dim)
            r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
            vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
            x = r * np.cos(phi) * np.sin(theta)
            y = r * np.sin(phi) * np.sin(theta)
            z = r * np.cos(theta)

            print(180.0/np.pi*np.arctan2(y[1]-y[0],-(x[1]-x[0])))

            self.ax.plot(x, y, linestyle='-',linewidth='0.8', marker="", color="blue", markersize=0.4,)# label=r'trajectory')

        # Now, plot the thing

        self.ax.set_ylabel("y")
        self.ax.set_xlabel("x")
        #self.ax.set_xlim(-2,6)
        #self.ax.set_ylim(-3,3)

        self.ax.set_aspect('equal')
        black_hole=patches.Circle((0,0),radius=1,label=r'black hole',color='black')
        circle = patches.Circle((0, 0), radius=3/2, label=r'3/2 rs', color='black',linestyle='dashed',fill=False)
        self.ax.add_patch(black_hole)
        self.ax.add_patch(circle)
        self.ax.plot(20,20,linestyle='-', marker='',color='blue',markersize=2,label=r"trajectories")
        #ax.plot(0, 0, marker='o',color='black', markersize=86, linewidth=0)
        self.ax.plot([-20,20],[0,0],'g--')
        self.ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        #self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        #self.figure.show()
        #self.figure.savefig(figName, transparent=True)
        print('saved as', figName)
def findpath(file):
    parrent = os.getcwd()[:-14]
    print("parrent:", parrent)
    # if we need find it first
    for root, dirs, files in os.walk(parrent):
        for name in files:
            if name == file:
                print("file found in", root)
                return os.path.abspath(os.path.join(root, name))
##################

r_obs = 5.#5.
x_obs = 1./r_obs
FullOrbit = Orbit(r_obs)
xi_init = 27.9935

#instance plotter class to get numerical solution from file
plotter = Plotter()

# get closest approach
aSqr = FullOrbit.get_asqr(x_obs, xi_init*math.pi/180.)
x_min = FullOrbit.get_xMin(aSqr)
# calculate r_min where the numerical solution should stop
r_min = 1. / x_min
print("minimal dist", r_min)

### numerical r_min  and phi_min where numerical solution exactly stops
# trajectory_50stepsize5.000000.h5
# trajectory_500stepsize5.000000.h5
# trajectory_500stepsizeRK25.000000.h5

figure = plt.figure()
ax = figure.add_subplot(111)
# ax3 = figure.add_subplot(212)
ax2 = ax.twinx()  # figure.add_subplot(212)
# figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.3)

# filelist=['trajectory_100stepsizeRK45.000000.h5','trajectory_100stepsizeRK25.000000.h5','trajectory_100stepsizeHeun_5.000000.h5']
# 'trajectory_8stepsizeRK4_adapt5.000000.h5',
# 'trajectory_100stepsizeFehlbergRK45_5.000000.h5',
# filelist=['trajectory_100stepsizeRK4_adapt_tol_1e-13_5.000000.h5','trajectory_100stepsizeRK4_adapt_tol_1e-15_5.000000.h5',
#          'trajectory_100stepsize_FehlbergRK45_tol_1e-13_5.000000.h5','trajectory_100stepsize_FehlbergRK45_tol_1e-19_5.000000.h5']

# filelist=['trajectory_42stepsize_RK4_5.000000.h5','trajectory_42stepsize_RK2_5.000000.h5','trajectory_42stepsize_Heun_5.000000.h5']

filelist = ['trajectory_40tolRK4_adapt_step_1e-3_5.000000.h5', 'trajectory_40tol_Fehlberg_step_1e-3_5.000000.h5']
timelist = [
    [576.1, 434.075, 623.75, 438.05, 404.475, 264.5, 255.725, 162.05, 484.175, 275.575, 185.55, 2011.72, 5374.5, 102.55,
     89.4, 73.45, 64.55, 76.75, 65.175, 56.975, 78.575, 35.2, 28.925, 25.625, 22.875, 19.575, 18.075, 15.75, 13.45,
     12.725, 17.275, 12.75, 11.875, 10.8, 10.15, 9.675, 9.65, 17.75, 15.05, 19.225],
    [305.075, 257.325, 219.25, 187.275, 144.4, 127.1, 102.325, 102.275, 88.125, 662.05, 94.6, 51.475, 379.4, 39.025,
     55.875, 63.775, 53.125, 39.25, 39.375, 28.575, 22.05, 161.525, 12.65, 10.425, 8.925, 7.725, 6.875, 6.025, 6.025,
     5.5, 5.375, 9.425, 6.425, 12.75, 5.15, 10.475, 9.5, 4.45, 4.675, 9.8]]
names = ['RK4_adaptive', 'Fehlberg']
for j, (name, time, n) in enumerate(zip(filelist, timelist, names)):
    # get numerical values
    r_min_num, phi_min_num, total_steps = plotter.get_trajectory(findpath(name), 6)

    # get analytical values -> phi for numerical r_min to compare with numerical phi_min
    phi_min = []
    stepsizeFB = []
    stepsizeRK = []
    tol = []
    stepsize = []
    for i, r in enumerate(r_min_num):
        phi_min.append(FullOrbit.get_phi_rel(xi_init, r))
        stepsizeFB.append(1e-4 * (1 + i))
        tol.append(1e-15 * (2 ** i))
        stepsize.append(1e-5 * (1.3 ** i))

    phi_min = np.array(phi_min)
    phi_min_num = np.array(phi_min_num)
    # if j<2:
    ax.loglog(tol, abs(phi_min + phi_min_num), 'o', ms=3, label=n + '_global error')  # str(name[15:-21]))
    ax2.loglog(tol, total_steps, 'x', ms=3, label=n + '_steps')
    # ax2.loglog(time, total_steps, 'x', ms=3,label=n+'_steps')
    # ax3.loglog(tol, abs(phi_min + phi_min_num), 'o', ms=3,label=n)
    # ax3.loglog(time, tol, 'o', ms=3, label=n)
    # ax.loglog(stepsize, stepsize, 'k--')#, label='slope = 1')

    # ax2.loglog(stepsize,total_steps)
    # ax3.loglog(stepsize,time,label=n)

    # else:
    #    ax.loglog(stepsizeFB, abs(phi_min + phi_min_num), 'o', ms=3, label=name[11:-11])
    # ax.semilogy(stepsize, phi_min_num,'o',ms=3, label=name)
    # ax.semilogy(stepsize, -phi_min, label='analytical')

# ax3.set_ylabel(r'tolerance')
ax.set_xlabel(r'tolerance')
# ax.set_xlabel('stepsize')
ax.set_ylabel(r'$|\phi_{analytical}-\phi_{numerical}|$')
# ax3.set_xlabel(r'computational time in ms')
ax2.set_ylabel(r'total number of steps')
# ax3.legend(loc='best',fontsize='small')
ax2.legend(loc='upper left', fontsize='small')
ax.legend(loc='upper right', fontsize='small')
plt.tight_layout()
plt.savefig('analytica_numerical_compare5.pdf')
plt.show()
