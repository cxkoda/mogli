from TrajectoryDataAccess import TrajectoryDataAccess
import matplotlib.pyplot as plt


class harmOsz_plotter:
    def __init__(self):
        self.name = "test"

    def get_first(self, trajectory):
        return trajectory[:, 0]

    def plot(self, filename,plotname,start):
        # load the trajectory
        trajectoryDataAccess = TrajectoryDataAccess(filename)
        times = trajectoryDataAccess.get_times(0)
        data = trajectoryDataAccess.get_trajectory(0)
        data = self.get_first(data)
        #print("my data", data)

        # Now, plot the thing
        self.figure = plt.figure()
        ax = self.figure.add_subplot(3, 1, 1)
        ax.plot(times[start:], data[start:], marker="o", markersize=3,color="r")

        # calculate stepsizes
        stepsize = []
        for i in range(1, len(times)):
            stepsize.append(times[i]-times[i-1])
        ax2 = self.figure.add_subplot(3, 1, 2)
        ax2.plot(times[start:-1], stepsize[start:], marker="o", markersize=3,color="r")

        ax3 = self.figure.add_subplot(3, 1, 3)
        ax3.hist(stepsize[50:], bins='auto')

        print(filename)
        print("min value:", min(stepsize[50:]))
        print("max value:", max(stepsize[50:]))
        print("number of steps:", len(stepsize[50:]))

        figName = plotname
        self.figure.savefig(figName, transparent=True)
        


plotter = harmOsz_plotter()
plotter.plot("FehlbergRK_error_1.30666e-08.h5","HarmonicOscillator_FehlbergRK.pdf",36123)
plotter.plot("ARK4_error_1,34306e-08.h5","HarmonicOscillator_AdaptiveRK4.pdf",14970)

