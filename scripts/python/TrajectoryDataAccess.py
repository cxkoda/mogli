import h5py


class TrajectoryDataAccess:
    def __init__(self, filename):
        print(f"Opening file: {filename}")
        self.h5file = h5py.File(filename, 'r')
        self.h5TrajectoryGroup = self.h5file['Trajectories']

    def get_num_trajectories(self):
        return len(self.h5TrajectoryGroup)

    def get_trajectoryDataset(self, i_trajectory):
        assert i_trajectory < self.get_num_trajectories()
        datasetName = str(i_trajectory)
        dataset = self.h5TrajectoryGroup[datasetName]
        return dataset

    def get_times(self, i_trajectory):
        return self.get_trajectoryDataset(i_trajectory)[:, 0]

    def get_trajectory(self, i_trajectory):
        return self.get_trajectoryDataset(i_trajectory)[:, 1:]
