from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
import matplotlib.pyplot as plt
import numpy as np
import os


class Plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output

    def plot(self, filename, dim):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        x, y, z = my_Data[0], my_Data[1], my_Data[2]
        vx, vy, vz = my_Data[3], my_Data[4], my_Data[5]
        #print(my_Data)
        # Now, plot the thing
        self.figure = plt.figure()

        # plot x-t:
        ax = self.figure.add_subplot(2, 1, 1)
        ax.set_title('Trajektorie')
        ax.set_ylabel("Ort")
        #ax2 = ax.twinx()
        #ax2.set_ylabel("Anzahl Jaeger")
        ax.plot(x, marker="o", color="b", markersize=2, label=r'x')
        ax.plot(y, marker="o", color="r", markersize=2, label=r'y')
        ax.plot(z, marker="o", color="k", markersize=2, label=r'z')
        #ax2.plot(x2, marker="o", color="r", markersize=3, label=r'Jaeger')
        #ax2.legend(loc='lower right')
        ax.legend(loc='lower left')

        #for i in range(dim):
        #    ax.plot(my_Data[i], marker="o", markersize=3, label=r'test')

        # plot x-y:
        if dim == 6:
            ax3 = self.figure.add_subplot(2, 1, 2)
            ax3.set_title('Geschwindigkeit')
            ax3.plot(vx, marker="o", color="b", markersize=2, label=r'vx')
            ax3.plot(vy, marker="o", color="r", markersize=2, label=r'vy')
            ax3.plot(vz, marker="o", color="k", markersize=2, label=r'vz')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # try to

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        self.figure.show()
        self.figure.savefig(figName, transparent=True)
        print('saved as', figName)

    def plot_2d(self, filename, dim):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        x, y, z = my_Data[0], my_Data[1], my_Data[2]
        vx, vy, vz = my_Data[3], my_Data[4], my_Data[5]
        # print(my_Data)
        # Now, plot the thing
        self.figure = plt.figure()

        # plot x-y:
        ax = self.figure.add_subplot(2, 1, 1)
        ax.set_ylabel("y")
        ax.set_ylabel("x")

        ax.plot(x, y, marker="o", color="b", markersize=2, label=r'')

        ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # try to

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        self.figure.show()
        self.figure.savefig(figName, transparent=True)
        print('saved as', figName)

    def plot_polar(self, filename, dim):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
        vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
        # print(my_Data)
        # Now, plot the thing
        self.figure = plt.figure()

        # plot x-t:
        ax = self.figure.add_subplot(211, projection='polar')
        ax.set_title('Trajektorie')
        ax.set_ylabel("Ort")

        ax.plot(theta, r)
        ax.set_rmax(2)
        ax.set_rticks([0.5, 1, 1.5, 2])  # less radial ticks
        ax.set_rlabel_position(-22.5)  # get radial labels away from plotted line
        ax.grid(True)
        # ax2.plot(x2, marker="o", color="r", markersize=3, label=r'Jaeger')
        # ax2.legend(loc='lower right')
        ax.legend(loc='lower left')

        # for i in range(dim):
        #    ax.plot(my_Data[i], marker="o", markersize=3, label=r'test')

        # plot x-y:
        if dim == 6:
            ax3 = self.figure.add_subplot(2, 1, 2)
            ax3.set_title('Geschwindigkeit')
            ax3.plot(vr, marker="o", color="b", markersize=2, label=r'vx')
            ax3.plot(vt, marker="o", color="r", markersize=2, label=r'vy')
            ax3.plot(vp, marker="o", color="k", markersize=2, label=r'vz')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # try to

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        self.figure.show()
        self.figure.savefig(figName, transparent=True)
        print('saved as', figName)

    def plot_polar_as_xy(self, filename, dim):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
        vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
        x = r * np.cos(theta)
        y = r * np.sin(theta)

        # Now, plot the thing
        self.figure = plt.figure()
        # plot x-y:
        ax = self.figure.add_subplot(2, 1, 1)
        # ax.set_title('Trajektorie')
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.plot(x, y, marker="o", color="b", markersize=2, label=r'')
        ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        self.figure.show()
        self.figure.savefig(figName, transparent=True)
        print('saved as', figName)

# define a function to find the path to trajector file
def findpath(file):
    parrent = os.getcwd()[:-14]
    print("parrent:", parrent)
    # if we need find it first
    for root, dirs, files in os.walk(parrent):
        for name in files:
            if name == file:
                print("file found in", root)
                return os.path.abspath(os.path.join(root, name))


if __name__ == "__main__":
    # test the stuff
    plotter = Plotter()

    # include an exception bc an error occurs when the pdf is opened in another programm
    try:

        plotter.plot_2d(findpath('trajectory_geo.h5'), 6)
        plotter.plot_polar_as_xy(findpath('trajectory_geo_polar.h5'), 6)
    except OSError:
        print("!!!ERROR - File could not be opened!!!!!!!!")
