import matplotlib.pyplot as plt
import numpy as np
import csv


class stepsizeorder_plotter:
    def __init__(self):
        self.name = "test"
    def plot(self, filename, solver,marksize):
        x = []
        y = []
        with open(filename) as tsvfile:
            tsvreader = csv.reader(tsvfile, delimiter=" ")
            for line in tsvreader:
                print(line)
                x.append(1/abs(float(line[0])))
                y.append(abs(float(line[1])))

        ax.scatter(x, y, label=solver,s=marksize)


# initiate plot
fig = plt.figure()
ax = fig.add_subplot(111)
plotter = stepsizeorder_plotter()

# import/use files
#plotter.plot("OrderExplicitEuler.tsv", "euler",20)
#plotter.plot("OrderRungeKutta2.tsv", "runge",25)
#plotter.plot("OrderHeun.tsv", "heun",10)
plotter.plot("OrderRungeKutta4.tsv", "rungekutta4",25)
plotter.plot("OrderFehlberg_RK4_solver.tsv", "FehlbergRK4",25)
plotter.plot("OrderFehlberg_RK5_solver.tsv", "FehlbergRK5",25)
plotter.plot("OrderARKFull.tsv", "ARK_full",7)
plotter.plot("OrderARKHalf.tsv", "ARK_half",7)
x = np.arange(1, 10**6, 10000)
ax.plot(x, 1/x/5, color='black', label = "slope = -1, -2, -4, -5")
ax.plot(x, 1/x**(2)/10, color='black')
ax.plot(x, 1/x**(4)/500, color='black')
ax.plot(x, 1/x**(5)/3000, color='black')

ax.set_xscale("log")
ax.set_yscale("log")

# set axis range. Maybe have to be changed with different data
ax.set_xlim(1, 1e3)
ax.set_ylim(1e-17, 1)
ax.set_ylabel("Error")
ax.set_xlabel("Time steps")
plt.legend(loc="upper right")
ax.set_title("Results")
figName = "StepsizeOrder.pdf"
fig.savefig(figName, transparent=True)

