from TrajectoryDataAccess import TrajectoryDataAccess
import matplotlib.pyplot as plt


class exp_plotter:
    def __init__(self):
        self.name = "test"

    def get_first(self, trajectory):
        return trajectory[:, 0]

    def plot(self, filename):
        # load the trajectory
        trajectoryDataAccess = TrajectoryDataAccess(filename)
        times = trajectoryDataAccess.get_times(0)
        data = trajectoryDataAccess.get_trajectory(0)
        data = self.get_first(data)
        print("my data", data)

        # Now, plot the thing
        self.figure = plt.figure()
        ax = self.figure.add_subplot(1, 1, 1)
        ax.plot(times, data, marker="o", color="r")
        figName = "expo.pdf"
        self.figure.savefig(figName, transparent=True)


# test the stuff
plotter = exp_plotter()
plotter.plot("trajectory.h5")
