from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
import matplotlib.pyplot as plt
import os


class LotkaVolterra_plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output

    def plot(self, filename, dim):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        my_Times = data_acc.get_times(0)
        x1, x2 = my_Data[0], my_Data[1]

        # Now, plot the thing
        self.figure = plt.figure()

        # plot x-t:
        ax = self.figure.add_subplot(2, 1, 1)
        ax.set_title('Jaeger Beute Population')
        ax.set_ylabel("Anzahl Beute")
        ax2 = ax.twinx()
        ax2.set_ylabel("Anzahl Jaeger")
        ax.plot(my_Times, x1, marker="o", color="b", markersize=3, label=r'Beute')
        ax2.plot(my_Times, x2, marker="o", color="r", markersize=3, label=r'Jaeger')
        ax2.legend(loc='lower right')
        ax.legend(loc='lower left')

        # for i in range(dim):
        #    ax.plot(my_Data[i], marker="o")

        # plot x-y:
        if dim == 2:
            ax3 = self.figure.add_subplot(2, 1, 2)
            ax3.set_title('Jaeger Beute Phasenraum')
            ax3.plot(x1, x2, marker="o")

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # try to

        # self.figure.subplots_adjust(hspace=0.4)
        self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        # self.figure.show()
        self.figure.savefig(figName, transparent=True)
        print('saved as', figName)


# define a function to find the path to trajector file
def findpath(file):
    parrent = os.getcwd()[:-14]
    print("parrent:", parrent)
    # if we need find it first
    for root, dirs, files in os.walk(parrent):
        for name in files:
            if name == file:
                print("file found in", root)
                return os.path.abspath(os.path.join(root, name))


if __name__ == "__main__":
    # test the stuff
    plotter = LotkaVolterra_plotter()

    # include an exception bc an error occurs when the pdf is opened in another programm
    try:
        plotter.plot(findpath('trajectory_euler1.h5'), 2)
        plotter.plot(findpath('trajectory_euler2.h5'), 2)
        plotter.plot(findpath('trajectory_runge1.h5'), 2)  # caution with dimension (works just with 2 for now)
        plotter.plot(findpath('trajectory_runge2.h5'), 2)
        plotter.plot(findpath('trajectory_rungekutta4_1.h5'), 2)
        plotter.plot(findpath('trajectory_rungekutta4_2.h5'), 2)
        plotter.plot(findpath('trajectory_rungekuttaadaptive1.h5'), 2)
        plotter.plot(findpath('trajectory_rungekuttaadaptive2.h5'), 2)
        plotter.plot(findpath('trajectory_FRK45_1.h5'), 2)
        plotter.plot(findpath('trajectory_FRK45_2.h5'), 2)
    except OSError:
        print("!!!ERROR - File could not be opened!!!!!!!!")
