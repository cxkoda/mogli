import matplotlib.pyplot as plt
import numpy as np
import csv


class stepsizeorder_plotter:
    def __init__(self):
        self.name = "test"
    def plot(self, filename, solver):
        x = []
        y = []
        with open(filename) as tsvfile:
            tsvreader = csv.reader(tsvfile, delimiter=" ")
            for line in tsvreader:
                print(line)
                x.append(abs(float(line[0])))
                y.append(abs(float(line[1])))

        ax.scatter(x, y, label=solver)


# initiate plot
fig = plt.figure()
ax = fig.add_subplot(111)
plotter = stepsizeorder_plotter()

# import/use files
plotter.plot("ToleranceGlobalErrorARK4.tsv", "Adaptive Runge Kutta 4")
plotter.plot("ToleranceGlobalErrorFRK45.tsv", "Fehlberg RK45")

ax.set_xscale("log")
ax.set_yscale("log")

# set axis range. Maybe have to be changed with different data
ax.set_xlim(1e-10, 1)
ax.set_ylim(1e-14, 1)
ax.set_ylabel("globale error")
ax.set_xlabel("tolerance")
plt.legend(loc="upper right")
ax.set_title("Results")
figName = "ToleranceGlobalErrorARK4.pdf"
fig.savefig(figName, transparent=True)

