from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
import matplotlib.pyplot as plt
import numpy as np
import os


class Plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output

    def plot_2d(self, filename, dim,subplot=[1,1,1]):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        x, y, z = my_Data[0], my_Data[1], my_Data[2]
        vx, vy, vz = my_Data[3], my_Data[4], my_Data[5]
        # print(my_Data)
        # Now, plot the thing
        self.figure = plt.figure(1)

        # plot x-y:
        ax = self.figure.add_subplot(subplot[0],subplot[1],subplot[2])
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_xlim(-8, 8)
        ax.set_ylim(-8, 8)
        ax.set_aspect('equal')
        ax.plot(x, y, marker="o", color="b", markersize=2, label=r'')
        ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        #self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)
        ax.set_title(figName[:-4])
        #self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        #self.figure.show()
        #self.figure.savefig(figName, transparent=True)
        #print('saved as', figName)

    def plot_polar_as_xy(self, filename, dim,subplot=[1,1,1]):
        # load the trajectory
        data_acc = tr_data(filename)
        my_Data = self.get_col(data_acc.get_trajectory(0), dim)
        #print(my_Data)
        r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
        vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
        x = r * np.cos(phi)* np.sin(theta)
        y = r *np.sin(phi)* np.sin(theta)
        z = r * np.cos(theta)

        # Now, plot the thing
        self.figure = plt.figure(1)
        # plot x-y:
        ax = self.figure.add_subplot(subplot[0],subplot[1],subplot[2])

        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_xlim(-8,8)
        ax.set_ylim(-8,8)
        ax.set_aspect('equal')
        ax.plot(x, y, marker="o", color="b", markersize=2, label=r'')
        ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        #self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)
        ax.set_title(figName[:-4])
        # self.figure.subplots_adjust(hspace=0.4)
        #self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        #self.figure.show()
        #self.figure.savefig(figName, transparent=True)
        #print('saved as', figName)

    def plot_polar_and_xy(self, filename1,filename2, dim,savename='spherical_cart.pdf'):
        # load the trajectory
        data_acc1 = tr_data(filename1)
        my_Data1 = self.get_col(data_acc1.get_trajectory(0), dim)
        r, theta, phi = my_Data1[0], my_Data1[1], my_Data1[2]
        vr, vt, vp = my_Data1[3], my_Data1[4], my_Data1[5]
        x1 = r * np.cos(phi)* np.sin(theta)
        y1 = r *np.sin(phi)* np.sin(theta)
        z1 = r * np.cos(theta)

        data_acc2 = tr_data(filename2)
        my_Data2 = self.get_col(data_acc2.get_trajectory(0), dim)
        x2, y2, z2 = my_Data2[0], my_Data2[1], my_Data2[2]
        vx2, vy2, vz2 = my_Data2[3], my_Data2[4], my_Data2[5]

        # Now, plot the thing
        self.figure = plt.figure(1)
        # plot x-y:
        ax = self.figure.add_subplot(1,1,1)

        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_xlim(-8,8)
        ax.set_ylim(-8,8)
        ax.set_aspect('equal')
        ax.plot(x1, y1, marker="o", color="b", markersize=1, label=r'spherical')
        ax.plot(x2, y2, marker="o", color="r", markersize=1, label=r'cartesian')
        ax.legend(loc='lower left')

        self.figure.tight_layout()
        self.figure.show()
        self.figure.savefig(savename, transparent=True)
        print('saved as', savename)

# define a function to find the path to trajector file
def findpath(file):
    parrent = os.getcwd()[:-14]
    print("parrent:", parrent)
    # if we need find it first
    for root, dirs, files in os.walk(parrent):
        for name in files:
            if name == file:
                print("file found in", root)
                return os.path.abspath(os.path.join(root, name))


if __name__ == "__main__":
    # test the stuff
    plotter = Plotter()

    # include an exception bc an error occurs when the pdf is opened in another programm
    try:

        plotter.plot_2d(findpath('trajectory_geo.h5'), 6,subplot=[1,2,1])
        plotter.plot_polar_as_xy(findpath('trajectory_geo_polar.h5'), 6,subplot=[1,2,2])
        plotter.figure.show()
        plotter.plot_polar_and_xy(findpath('trajectory_geo_polar.h5'),findpath('trajectory_geo.h5'),6)
    except OSError:
        print("!!!ERROR - File could not be opened!!!!!!!!")
