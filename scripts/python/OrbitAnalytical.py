import scipy.special as spec
import math
import sys
import numpy as np
import matplotlib.pyplot as plt

class Orbit(object) :
    def __init__(self, r_init) :
        self.deg2rad = math.pi/180.
        self._r_init = r_init

    def get_xMin(self, asqr) :
        aVal = math.sqrt(asqr)
        xMin = math.sqrt(3)/2.*aVal
        xMin /= math.cos(math.acos(-3*math.sqrt(3)*aVal/2.)/3.)
        return xMin
        
    def get_asqr(self, xobs, xi) :
        # Compute square of a, Eq. (7)
        asqr = xobs**2*(1.-xobs)/math.sin(xi)**2
        print("a^2",asqr,4./27.)
        return asqr

    def get_theta(self, asqr) :
        print(1.-27./2.*asqr)
        theta = math.acos((1.-27./2.*asqr))/3.
        return theta

    def get_f_ring(self, rad_n) :
        x_n = 1./rad_n
        print("Werte fuer f:",x_n,self._xi_ring)
        asqr = self.get_asqr(x_n, self._xi_ring)
        print("a^2",asqr)
        theta = self.get_theta(asqr)
        self.x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
        self.x2 = 1./3. + 2./3.*math.cos(theta)
        self.x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)
        
        fVal = self._get_phi(x_n) - math.pi
        return fVal
        

    def get_r_ring(self, xi_ring, rad_n) :
        # Compute x value for Einstein ringe for given xi
        self._xi_ring = xi_ring*self.deg2rad
        epsilon = 1.e-5

        for i_iter in range(20) :
            f_n = self.get_f_ring(rad_n)
            f_n_plus = self.get_f_ring(rad_n+epsilon)
            f_n_minus = self.get_f_ring(rad_n-epsilon)
            rhs = 2*f_n*epsilon/(f_n_plus - f_n_minus)
            print("rhs:",rhs, f_n, f_n_plus, f_n_minus, f_n_plus-f_n_minus)
            rad_n -= rhs
            print("r_n",rad_n)
            
        return rad_n
        

    def get_phi_rel(self, xi_init, rad) :
        xi = xi_init*self.deg2rad
        self.xobs = 1./self._r_init
        xVal = 1./rad
        print("xobs:",self.xobs)
        asqr = self.get_asqr(self.xobs, xi)
        print("a^2",asqr)

        theta = self.get_theta(asqr)
        self.x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
        self.x2 = 1./3. + 2./3.*math.cos(theta)
        self.x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)
        #self.x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
        #self.x2 = 1./3. + 2./3.*math.cos(theta)
        #self.x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)
        
        print("theta",theta)
        print("x1,x2,x3",self.x1,self.x2,self.x3)
        print("x",xVal)
        
        # Compute phi relative to initial position
        phi = self._get_phi(xVal) - self._get_phi(self.xobs)
        return phi

    def _get_phi(self, xVal) :
        print("in",(xVal - self.x2)/(xVal - self.x1))
        par = math.sqrt((xVal - self.x2)/(xVal - self.x1))
        print("par",par)
        #parInit = math.sqrt((self.xobs - self.x2)/(xVal - self.x1))
        msqr = (self.x1-self.x3)/(self.x2-self.x3)
        mVal = math.sqrt(msqr)
    
        fac = 2./math.sqrt(self.x2-self.x3)
    
        print("parameters:",msqr,par,mVal)
    
        #print(par, msqr,fac, phi)
        #print(par, mVal,self.x1-self.x3, self.x2-self.x3)
        #phi = fac*spec.ellipkinc(math.sin(par),mVal)
        
        # Translate parameters according to appendix B
        if(par>1 and mVal<1 and par*mVal) :
            xLawden = 1./(par*mVal)
            kLawden = mVal
            prefac = 1.
        elif(mVal>1) :
            xLawden = par*mVal
            kLawden = 1./mVal
            prefac = 1./mVal
        else :            
            xLawden = par
            kLawden = mVal
            prefac = 1.
        phiAS = math.asin(xLawden)            
        mAS = kLawden**2
                
        phi = fac*prefac*spec.ellipkinc(phiAS,mAS)
        
        #phi = fac*spec.ellipkinc(par,msqr)
        
        #phiInit = fac*spec.ellipkinc(parInit,mVal)
        print("phi",phi)
    
        #return (phi-phiInit)/self.deg2rad
        return phi
    
    def get_rad(self, xi_init, phi, phi_init):
        
        xi = xi_init*self.deg2rad
        
        self.xobs = 1./self._r_init
        
        
        
        asqr = self.get_asqr(self.xobs, xi)
        print("a^2",asqr)

        theta = self.get_theta(asqr)
        self.x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
        self.x2 = 1./3. + 2./3.*math.cos(theta)
        self.x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)
        print("x1,x2,x3",self.x1,self.x2,self.x3)
        
        phi_init = self._get_phi(self.xobs)
        print("init:",self.xobs, phi_init)
        phi = phi*self.deg2rad
        
        par = 0.5*math.sqrt(self.x2- self.x3)*(phi-phi_init)
        msqr = (self.x1-self.x3)/(self.x2-self.x3)
        mVal = math.sqrt(msqr)
        
        # Test, welche die inverse Transformation ist
        #print("Integrand",par)
        #intHin = spec.ellipkinc(par,msqr)
        #print("Hin:",intHin)
        #intRue = spec.ellipj(intHin, msqr)
        #print("Rueck",intRue)
        
        integ = spec.ellipj(par, msqr)
        #integ = spec.ellipj(par, mVal)
        print("integral:",integ)
        print("with sin",math.sin(integ[0]))
        #xVal = self.x2 - self.x1*integ[0]**2
        #xVal /= 1.-integ[0]**2
        xVal = self.x2 - self.x1*integ[0]**2
        xVal /= 1.-integ[0]**2
        
        radCurr = 1./xVal
        print("x:",xVal,"r:",radCurr)
        
        return radCurr
    
myOrbit = Orbit(5.)    
rad = 4.
phi = myOrbit.get_phi_rel(30,rad)
print("phi:",phi,phi/myOrbit.deg2rad)

#sys.exit()

if(False) :
    print("")
    xi_guess = 28.6
    myOrbit.get_r_ring(xi_guess, 5.)

print("")
myRad = myOrbit.get_rad(50, -90., 0.)
print("get radius:",myRad)

# Visualisierung
r_obs = 5.
x_obs = 1./r_obs
FullOrbit = Orbit(r_obs)

xi_init = 28


# get closest approach
aSqr = FullOrbit.get_asqr(x_obs, xi_init*math.pi/180.)
x_min = FullOrbit.get_xMin(aSqr)
print("minimal dist",1./x_min)

number = 5000
radii = np.linspace(1./x_min,5,num=number)
phis = np.linspace(2,5,num=number)
xVals = np.linspace(2,5,num=number)
yVals = np.linspace(2,5,num=number)
numPoints = radii.size

for iPoint in range(numPoints) :
    phiVal = FullOrbit.get_phi_rel(xi_init,radii[iPoint])
    phis[iPoint] = phiVal
    xVals[iPoint] = radii[iPoint]*math.cos(phiVal)
    yVals[iPoint] = radii[iPoint]*math.sin(phiVal)
    
print("Radii",radii)
print("Angles",phis)    

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(xVals, yVals, color='r')
#ax.plot(xVals, yVals, color='r',linestyle='', marker='o')
ax.set_aspect('equal')

plt.xlim([-6,6])
plt.ylim([-6,6])

figName = "Orbit.pdf"
fig.savefig(figName, transparent=True)



sys.exit()

print(FullOrbit.get_rad(xi_init,0,0))
sys.exit()

phi2 = np.linspace(0,360,num=number)
rad2 = np.linspace(0,360,num=number)

for iPoint in range(numPoints) :
    radVal = FullOrbit.get_rad(xi_init, phi2[iPoint],0.)
    rad2[iPoint] = radVal
    
print("phi2",phi2)    
print("rad2",rad2)


# Cubic equation
xi_rad = 30/180.*math.pi
x_obs = 1./5.
aSqr = FullOrbit.get_asqr(x_obs, xi_rad)
print("a^2:",aSqr)
x_vals = np.linspace(-0.5,1,num=100)
poly_vals = np.linspace(0,1,num=100)

for iPoint in range(100) :
    polynomial = aSqr - (1.-x_vals[iPoint])*x_vals[iPoint]**2
    poly_vals[iPoint] = polynomial
    
    
theta = FullOrbit.get_theta(aSqr)
x1 = 1./3. + 2./3.*math.cos(2.*math.pi/3. - theta)
x2 = 1./3. + 2./3.*math.cos(theta)
x3 = 1./3. + 2./3.*math.cos(2.*math.pi/3. + theta)   

print("Nullstellen:",x1,x2,x3) 

xnulls = np.array([x1,x2,x3])
ynulls = np.array([0,0,0])
    
fig2 = plt.figure()
ax = fig2.add_subplot(1,1,1)
ax.plot(x_vals, poly_vals, color='r')
ax.plot(x_vals,0*x_vals,color='k')
ax.plot(xnulls,ynulls, color='k',linestyle='', marker='x')
figName = "polly.pdf"
fig2.savefig(figName, transparent=True)
