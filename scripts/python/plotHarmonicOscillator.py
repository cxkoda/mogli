from TrajectoryDataAccess import TrajectoryDataAccess
import matplotlib.pyplot as plt


class harmOsz_plotter:
    def __init__(self):
        self.name = "test"

    def get_first(self, trajectory):
        return trajectory[:, 0]

    def plot(self, filename,plotname):
        # load the trajectory
        trajectoryDataAccess = TrajectoryDataAccess(filename)
        times = trajectoryDataAccess.get_times(0)
        data = trajectoryDataAccess.get_trajectory(0)
        data = self.get_first(data)
        print("my data", data)

        # Now, plot the thing
        self.figure = plt.figure()
        ax = self.figure.add_subplot(1, 1, 1)
        ax.plot(times, data, marker="o", markersize=3,color="r")
        figName = plotname
        self.figure.savefig(figName, transparent=True)


plotter = harmOsz_plotter()
plotter.plot("harmOsz_trajectory_FehlbergRK.h5","HarmonicOscillator_FehlbergRK.pdf")
plotter.plot("harmOsz_trajectory_AdaptiveRK4.h5","HarmonicOscillator_AdaptiveRK4.pdf")
