from TrajectoryDataAccess import TrajectoryDataAccess as tr_data
import matplotlib.pyplot as plt
from matplotlib import patches
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import os
from matplotlib.animation import FFMpegWriter
import matplotlib.animation as animation
from matplotlib import rc
from matplotlib.ticker import MultipleLocator
rc('font',size=14)
rc('font',family='serif')
rc('axes',labelsize=18)

def update_line(num, data, line):
    line.set_data(data[..., :num])
    return line,


metadata = dict(title='Movie Test', artist='Matplotlib',
                comment='Movie support!')
writer = FFMpegWriter(fps=15, metadata=metadata)


class Plotter:
    def __init__(self):
        self.name = ""

    def get_col(self, trajectory, number_of_col):
        output = []
        for i in range(number_of_col):
            output.append(trajectory[:, i])
        return output


    def plot_polar_as_xy(self, filename, dim,num_of_tr=None):
        # load the trajectory
        data_acc = tr_data(filename)
        num_of_tr=data_acc.get_num_trajectories()

        self.figure = plt.figure()
        ax = self.figure.add_subplot(1, 1, 1)

        for i in range(num_of_tr):
            my_Data = self.get_col(data_acc.get_trajectory(i), dim)
            r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
            vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
            x = r * np.cos(phi) * np.sin(theta)
            y = r * np.sin(phi) * np.sin(theta)
            z = r * np.cos(theta)

            #print('x=',x[3000])
            #print('y=', y[3000])
            print(180.0/np.pi*np.arctan2(y[1]-y[0],-(x[1]-x[0])))

            ax.plot(x, y, linestyle='-',linewidth='0.8', marker="", color="blue", markersize=0.4,)# label=r'trajectory')

        # Now, plot the thing

        #ax.set_title('Trajektorie')
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_xlim(-20,20)
        #ax.set_ylim(-20,20)
        ax.set_xlim(-10, 10)
        ax.set_ylim(-6.5, 6.5)
        #ax.set_xlim(-5,5)
        ax.set_ylim(-10,10)

        #ax.set_xlim(-5, 5)
        #ax.set_ylim(-5, 5)

        ax.xaxis.set_major_locator(MultipleLocator(3))
        ax.yaxis.set_major_locator(MultipleLocator(3))

        ax.set_aspect('equal')
        #ax.grid()
        black_hole=patches.Circle((0,0),radius=2,label=r'Schwarzes Loch',color='black')
        circle = patches.Circle((0, 0), radius=3, label=r'3/2 rs', color='black',linestyle='dashed',fill=False)
        ax.add_patch(black_hole)
        ax.add_patch(circle)
        #ax.plot(20,20,linestyle='-', marker='',color='blue',markersize=2,label=r"trajectories")
        ###ax.plot(0, 0, marker='o',color='black', markersize=86, linewidth=0)
        #ax.plot([-20,20],[0,0],'g--')
        #ax.legend(loc='lower left')

        # figure title and names
        figDirName = str(filename[:-3] + ".pdf")
        figName = os.path.split(figDirName)[1]
        #self.figure.suptitle(figName[:-4], y=1)  # ,fontsize=16)

        # self.figure.subplots_adjust(hspace=0.4)
        #self.figure.subplots_adjust(top=0.7)  # it does nothing??
        self.figure.tight_layout()
        self.figure.savefig(figName, transparent=True)
        self.figure.show()
        #print('saved as', figName)

    def plot_polar_as_xyz(self, filename, dim,num_of_tr=None):

        self.figure = plt.figure()
        ax = self.figure.add_subplot(111, projection='3d')

        self.figure.subplots_adjust(left=0., bottom=0, right=0.78, top=1, wspace=None, hspace=None)

        ax.xaxis._axinfo['tick']['inward_factor'] = 0
        ax.xaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.yaxis._axinfo['tick']['inward_factor'] = 0
        ax.yaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.zaxis._axinfo['tick']['inward_factor'] = 0
        ax.zaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.zaxis._axinfo['tick']['outward_factor'] = 0.4

        for file in filename:
            # load the trajectory
            data_acc = tr_data(file)
            num_of_tr = data_acc.get_num_trajectories()
            for i in range(num_of_tr):
                my_Data = self.get_col(data_acc.get_trajectory(i), dim)
                r, theta, phi = my_Data[0], my_Data[1], my_Data[2]
                vr, vt, vp = my_Data[3], my_Data[4], my_Data[5]
                x = r * np.cos(phi) * np.sin(theta)
                y = r * np.sin(phi) * np.sin(theta)
                z = r * np.cos(theta)

                #ax.plot3D(np.ones_like(x) * -5, y,z, c='gray', marker="", linestyle='-', markersize=0.5)
                #ax.plot3D(x, np.ones_like(x) * -5,z , c='gray', marker="", linestyle='-', markersize=0.5)
                ax.plot3D(x,y,np.ones_like(x)*-5,c='gray', marker="",linestyle='-', markersize=0.5)
                ax.plot3D(x, y, z, c='blue', marker="",linestyle='-', markersize=0.8)  # label=r'trajectory')
                #data = [x,y,z]
                #l, = ax.plot3D([], [], [],  c='blue', marker="o", markersize=0.8)
                #line_ani = animation.FuncAnimation(self.figure, update_line, 25, fargs=(data, l),
                #                        interval=50, blit=True)
                #line_ani.save('lines.mp4', writer=writer)

        # Now, plot the thing
        # ax.set_title('Trajektorie')
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ax.set_zlabel("z")
        ax.set_xlim(-4.9, 3.9)
        ax.set_ylim(-4.9, 3.9)
        ax.set_zlim(-4.9, 3.9)

        ax.grid(False)
        ax.xaxis.pane.set_edgecolor('black')
        ax.yaxis.pane.set_edgecolor('black')
        ax.xaxis.pane.fill = False
        ax.yaxis.pane.fill = False
        ax.zaxis.pane.fill = False

        ax.xaxis.set_major_locator(MultipleLocator(2))
        ax.yaxis.set_major_locator(MultipleLocator(2))
        ax.zaxis.set_major_locator(MultipleLocator(2))

        u = np.linspace(0, 2 * np.pi, 100)
        v = np.linspace(0, np.pi, 100)
        r = 1
        x = r * np.outer(np.cos(u), np.sin(v))
        y = r * np.outer(np.sin(u), np.sin(v))
        z = r * np.outer(np.ones(np.size(u)), np.cos(v))
        ax.plot_surface(x, y, z, rstride=4, cstride=4, color='gray', linewidth=0, alpha=0.5)

        r = 5
        x = r * np.outer(np.cos(u), np.sin(v))
        y = r * np.outer(np.sin(u), np.sin(v))
        z = r * np.outer(np.ones(np.size(u)), np.cos(v))
        #ax.plot_surface(x, y, np.zeros_like(x), rstride=3, cstride=3, color='b', linewidth=0., alpha=0.2)

        #ax.legend(loc='lower left')
        #plt.tight_layout()
        ax.view_init(25,40)
        #plt.pause(10)

        # rotate the axes and update
        #for angle in range(0, 360):
        #    ax.view_init(angle, 3* angle)
        #    plt.draw()
        #    plt.pause(0.01)
        self.figure.savefig('Kerr2.png', transparent=True)

        plt.pause(10000)

        # self.figure.show()



#        print('saved as', figName)

# define a function to find the path to trajector file
def findpath(file):
    parrent = os.getcwd()[:-14]
    print("parrent:", parrent)
    # if we need find it first
    for root, dirs, files in os.walk(parrent):
        for name in files:
            if name == file:
                print("file found in", root)
                return os.path.abspath(os.path.join(root, name))


if __name__ == "__main__":
    # test the stuff
    plotter = Plotter()

    #HOW TO
    '''
    use plotter.plot_polar_as_xy(findpath('trajectory_name.h5'), 6)
    to plot one or several 2d trajectories created with schwazrschild demo.cpp file
    
    use plotter.plot_polar_as_xyz([findpath('trajectory_name.h5')], 6)
    to plot one or several 2d trajectories created with kerr demo.cpp file
    
    plots not always gets displayed ... sometimes only a pdf with the same name gets created
    
    '''

    # include an exception bc an error occurs when the pdf is opened in another programm
    try:
        #trajectory_1phi10.000000.h5
        # trajectory_100phi10.000000.h5
        # trajectory_100phi20.h5
        # trajectory_1phi3.000000.h5
        # trajectory_1phi3.001000.h5
        # trajectory_1phi2.999000.h5
        # trajectory_1phi2.999990.h5
        # trajectory_1phi3.000010.h5
        # trajectory_additional5.000000.h5
        # trajectory_45deg5.000000.h5

        #sekundärer einsteinring
        #plotter.plot_polar_as_xy(findpath('trajectory_1phi10.000000.h5'), 6)

        #einsteinring
        #plotter.plot_polar_as_xy(findpath('trajectory_1phi2.999999.h5'), 6)
        #plotter.plot_polar_as_xy(findpath('trajectory_1phi3.000000.h5'), 6)
        #plotter.plot_polar_as_xy(findpath('trajectory_1phi3.000001.h5'), 6)

        #plotter.plot_polar_as_xy(findpath('trajectory_1phi3.001000.h5'), 6)
        #plotter.plot_polar_as_xy(findpath('trajectory_1phi3.000000.h5'), 6)

        #viele trajektorien
        #plotter.plot_polar_as_xy(findpath('trajectory_100phi10.000000.h5'), 6)
        #plotter.plot_polar_as_xy(findpath('trajectory_100y.h5'), 6)

        plotter.plot_polar_as_xy(findpath('test.h5'), 6)

        ##files=np.array(['trajectory_0deg5.000000.h5','trajectory_90deg5.000000.h5','trajectory_45deg5.000000.h5'])
        ##plotter.plot_polar_as_xyz([findpath(files[0]),findpath(files[1]),findpath(files[2])],6)
        #trajectory_1Kerr_test23.828427.h5
        #trajectory_1Kerr_test3.828427.h5


        #plotter.plot_polar_as_xyz([findpath('trajectory_1Kerr_test3.828427.h5')], 6)
        #plotter.plot_polar_as_xyz([findpath('trajectory_1Kerr_test42.732051.h5')], 6)

        plotter.plot_polar_as_xyz([findpath('trajectory_1Kerr_test102.732051.h5')], 6)

        #vergl zu schwardschild bild
        #plotter.plot_polar_as_xyz([findpath('trajectory_1Kerr_test310.000000.h5')], 6)


        #plotter.plot_polar_as_xyz(findpath('trajectory_42stepsize_Heun_5.000000.h5'), 6)
    except OSError:
        print("!!!ERROR - File could not be opened!!!!!!!!")
