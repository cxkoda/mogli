#!/usr/bin/env sh

CLANG_FORMAT_BIN=clang-format
SOURCES=$(find src include test -type f -regex '.*\(.cpp\|.h\)$')

INCORRECT=0
TOTAL=$(echo ${SOURCES} | wc -w)

for source in ${SOURCES}; do
  ${CLANG_FORMAT_BIN} -style=file -output-replacements-xml ${source} | grep -q "replacement offset" \
  && echo "Reformatting: ${source}" \
  && INCORRECT=$(($INCORRECT + 1)) \
  && ${CLANG_FORMAT_BIN} -i ${source}
done

echo "Files reformatted: $INCORRECT / $TOTAL"
