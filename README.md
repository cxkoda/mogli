# MOGLI - MOdular Geodesic Line Integration

[![pipeline status](https://git.uibk.ac.at/c706214/imaging-of-black-holes/badges/master/pipeline.svg)](https://git.uibk.ac.at/c706214/imaging-of-black-holes/commits/master)
[![coverage report](https://git.uibk.ac.at/c706214/imaging-of-black-holes/badges/master/coverage.svg)](https://git.uibk.ac.at/c706214/imaging-of-black-holes/commits/master)

This project implements a generic structure for the numerical solution of ODEs in `c++17` , which will be used to compute trajectories of photons around static and rotating black holes.

## Structure

* `app` : Application directory
  + Example applications
  + Applications used to generate images for the Bachelor Thesis
* `include` : Public library header files
  + `mogli/core` : Data structures and IO
  + `mogli/ode` : Various ordinary differential equations
  + `mogli/integrator` : Various numerical integrators for odes
* `src` : Source code folder
* `test` : Unit tests
* `scripts` : Helper scripts for plots, analytic computations and utilities.

## System Dependencies

### Main

* `cmake >= 3.14`
* `c++17` capable compiler, e.g. `gcc >= 7`
* `git`
* `libhdf5`

### Optional

* `clang-format`
* `doxygen`
* `valgrind`
* `gcovr`
* `cppcheck`

## Building

To compile the library, tests and the applications that come with it run the following command.

``` 
mkdir -p build && cd build
cmake ..
make -j <number of threads>
```

### Flags

Append these flags to cmake

* Disable test compilation `-DMOGLI_BUILD_TESTING=FALSE`
* Disable app compilation `-DMOGLI_BUILD_APP=FALSE`

### Testing

To execute all tests run the following command after building

``` 
make test
```

## Code Analysis

### Code-Style check

This project enforces a code-style using `clang-format` .
To check if all source files are formatted correctly run the following helper script from the base directory

``` 
scripts/clang-format-check.sh
```

To automatically format all files run

``` 
scripts/clang-format-apply.sh
```

### Doxygen

To generate the Doxygen documentation under `build/doc/html` run

``` 
doxygen doc/Doxyfile
```

### Coverage

To generate the coverage report one has to recompile the code with the coverage instrumentation enabled and run the tests with

``` 
mkdir -p build && cd build
cmake .. -DMOGLI_ENABLE_COVERAGE=ON
make
make coverage
```

### Static Code Analysis

For a static code analysis run

``` 
cppcheck src/**/*.cpp test/**/*.cpp --verbose --enable=all --language=c++ -Iinclude --error-exitcode=1
```
