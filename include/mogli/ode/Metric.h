//
// Created by dave on 03.03.20.
//

#ifndef MOGLI_METRIC_H
#define MOGLI_METRIC_H

#include <array>
#include <cmath>
namespace mogli {
namespace ode {

/**
 * Metric tensor representation in the framework of Arnowitt–Deser–Misner (ADM).
 *
 * The metric is represented by three functions: lapse, shift and spatial
 * metric. This defines an interface to compute them and their partial, spatial
 * derivatives for a given point in time and space.
 */
class Metric {
 public:
  /**
   * Generic Lapse Type for @f$ \alpha @f$
   */
  using Lapse = double;
  /**
   * Generic Shift Type for @f$ \beta^i @f$
   */
  using Shift = std::array<double, 3>;
  /**
   * Generic Spatial Metric Type for @f$ \gamma_{i j} @f$
   */
  using SpatialMetric = std::array<std::array<double, 3>, 3>;
  /**
   * Generic inverse Spatial Metric Type for @f$ \gamma^{i j} @f$
   */
  using SpatialMetricInv = std::array<std::array<double, 3>, 3>;

  /**
   * Generic Lapse Derivative Type for @f$ \partial_i \alpha @f$
   */
  using DLapse = std::array<double, 3>;
  /**
   * Generic Shift Derivative Type for @f$ \partial_i \beta^j @f$
   */
  using DShift = std::array<std::array<double, 3>, 3>;
  /**
   * Generic inverse Spatial Metric Derivative Type for @f$ \partial_i
   * \gamma^{jk} @f$
   */
  using DSpatialMetricInv = std::array<std::array<std::array<double, 3>, 3>, 3>;
  /**
   *
   */
  using TransformMatrix = std::array<std::array<double, 4>, 4>;

  /**
   * Generic 3-Position Type
   */
  using Position = std::array<double, 3>;

  virtual ~Metric() {}

  /**
   * Get the lapse precomputed by setPosition().
   */
  Lapse getLapse() const { return lapse; }
  /**
   * Get the shift precomputed by setPosition().
   */
  const Shift& getShift() const { return shift; }
  /**
   * Get the spatial metric precomputed by setPosition().
   */
  const SpatialMetric& getSpatialMetric() const { return spatialMetric; }
  /**
   * Get the inverse spatial metric precomputed by setPosition().
   */
  const SpatialMetricInv& getSpatialMetricInv() const {
    return spatialMetricInv;
  }
  /**
   * Get the lapse derivative precomputed by setPosition().
   */
  const DLapse& getDLapse() const { return dLapse; }
  /**
   * Get the shift lapse derivative precomputed by setPosition().
   */
  const DShift& getDShift() const { return dShift; }
  /**
   * Get the inverse spatial metric derivative precomputed by setPosition().
   */
  const DSpatialMetricInv& getDSpatialMetricInv() const {
    return dSpatialMetricInv;
  }

  /**
   * Precomputes all internal variables for a given position in time and space.
   *
   * This function has to be invoked before any other function for new
   * positions.
   */
  virtual void setPosition(const double time, const Position& position) = 0;

  /**
   * compute contravariant u0
   * @param u_i_contra
   * @param epsilon
   * @return
   */
  virtual double compute_u0_contra(const std::array<double, 3> u_i_contra,
                                   const int epsilon);

  /**
   * building metric g_µv for transformation of initial velocity
   * u_v=g_v,µ * u^µ
   * @param u_i_contra
   * @param u0_contra
   * @return
   */
  virtual std::array<double, 4> compute_u_i_cov(
      const std::array<double, 3>& u_i_contra,
      const double u0_contra);

  /**
   * Transform a contravariant four vector from the local inertial frame to the
   * laboratory frame
   * @param u_lif The contravariant vector in the LIF
   * @return The contravariant vector in the LAB
   */
  virtual std::array<double, 4> transformLif2Lab(
      const std::array<double, 4>& u_lif) = 0;

 protected:
  Lapse lapse;
  Shift shift;
  SpatialMetric spatialMetric;
  SpatialMetricInv spatialMetricInv;

  DLapse dLapse;
  DShift dShift;
  DSpatialMetricInv dSpatialMetricInv;

  TransformMatrix transformMatrix;
};

}  // namespace ode
}  // namespace mogli

#endif  // MOGLI_METRIC_H
