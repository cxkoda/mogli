#ifndef MOGLI_POLYNOMIAL_H
#define MOGLI_POLYNOMIAL_H

#include <vector>

#include "mogli/ode/Ode.h"

namespace mogli {
namespace ode {

/**
 * A one-dimensional ODE, which is polynomial in time and independent of the
 * state.
 *
 * The ode is given by @f$ y'(x) = \sum_i c_i x^i @f$.
 */
class Polynomial : public Ode<1> {
 public:
  using Coefficients = std::vector<double>;

  /**
   * Construct a polynomial ODE from given coefficients.
   *
   * @param coefficients The polynomial coefficients in ascending order, i.e.
   * the index corresponds the the respective degree. The length determines the
   * degree of the degree of the polynomial.
   */
  explicit Polynomial(const Coefficients& coefficients);

  /**
   * Implementation of the right-hand side, cfg. Ode<dim>::getRhs.
   */
  RhsVector getRhs(const StateVector& state, const double& time) final;

  /**
   * The analytical solution of the ODE.
   *
   * @param initialState Initial state for which the solution is computed.
   * @param time The time for which the solution is computed.
   * @return The solution state.
   */
  StateVector getAnalytical(const StateVector& initialState,
                            const double& time) const;

 protected:
  /**
   * Evaluate a polynomial function using Horner's scheme.
   *
   * @param position The argument of the polynomial @f$x@f$.
   * @param coefficients The coefficients of the polynomial @f$c_i@f$.
   * @return
   */
  static double applyHornersScheme(const double position,
                                   const Coefficients& coefficients);

  /**
   * Obtain the coefficients of the integral of a polynomial.
   *
   * @param coefficients The coefficients of the polynomial.
   * @return Coefficients The coefficients of the integrated polynomial. One
   * element longer than the input coefficients.
   */
  static Coefficients getIntegratedCoefficients(
      const Coefficients& coefficients);

 private:
  const Coefficients coefficients;
  const Coefficients integralCoefficients;
};

}  // namespace ode
}  // namespace mogli

#endif