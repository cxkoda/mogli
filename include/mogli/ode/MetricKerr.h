//
// Created by patri on 23.03.2020.
//

#ifndef MOGLI_METRICKERR_H
#define MOGLI_METRICKERR_H

#include "Metric.h"

namespace mogli {
namespace ode {

class MetricKerr : public Metric {
 public:
  explicit MetricKerr(const double mass, const double angularMoment);
  virtual void setPosition(const double time, const Position& position) final;
  virtual std::array<double, 4> transformLif2Lab(
      const std::array<double, 4>& u_lib) final;

 private:
  const double mass;
  const double angularMoment;
  Position position;
};

}  // namespace ode
}  // namespace mogli

#endif
