//
// Created by patri on 23.03.2020.
//

#ifndef MOGLI_METRICSCHWARZSCHILD_H
#define MOGLI_METRICSCHWARZSCHILD_H

#include "Metric.h"

namespace mogli {
namespace ode {

class MetricSchwarzschild : public Metric {
 public:
  explicit MetricSchwarzschild(const double mass);
  void setPosition(const double time, const Position& position) final;
  std::array<double, 4> transformLif2Lab(
      const std::array<double, 4>& u_lif) final;

 private:
  const double mass;
};

}  // namespace ode
}  // namespace mogli

#endif  // MOGLI_METRICSCHWARZSCHILD_H
