#ifndef MOGLI_EXPONENTIAL_H
#define MOGLI_EXPONENTIAL_H

#include <math.h>

#include "Ode.h"

namespace mogli {
namespace ode {

/**
 * A Ode resulting in exponential growth or decay.
 *
 * The ode is given by @f$ y'(x) = \lambda y @f$.
 */
template <std::size_t dim>
class Exponential : public Ode<dim> {
 public:
  using RhsVector = typename Ode<dim>::RhsVector;
  using StateVector = typename Ode<dim>::StateVector;

  /**
   * Construct an exponential ODE with given factor lambda.
   *
   * @param lambda The coefficient on the rhs.
   */
  explicit Exponential(const double& lambda) { this->lambda = lambda; }

  /**
   * Implementation of the right-hand side, cfg. Ode<dim>::getRhs.
   */
  RhsVector getRhs(const StateVector& state,
                   [[maybe_unused]] const double& time) final {
    RhsVector RHS;
    for (std::size_t i = 0; i < dim; ++i) {
      RHS[i] = lambda * state[i];
    }
    return RHS;
  }

  /**
   * The analytical solution of the ODE.
   *
   * @param initialState Initial state for which the solution is computed.
   * @param time The time for which the solution is computed.
   * @return The solution state.
   */
  StateVector getAnalytical(const StateVector& initialState,
                            const double& time) const {
    StateVector finalState;
    for (std::size_t i = 0; i < dim; ++i) {
      finalState[0] = initialState[0] * std::exp(lambda * time);
    }
    return finalState;
  }

 private:
  double lambda;
};

}  // namespace ode
}  // namespace mogli

#endif