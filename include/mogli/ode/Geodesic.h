//
// Created by patri on 21.03.2020.
//

#ifndef MOGLI_GEODESIC_H
#define MOGLI_GEODESIC_H

#include "Metric.h"
#include "MetricMinkowski.h"
#include "Ode.h"

namespace mogli {
namespace ode {

class Geodesic : public Ode<6> {
 public:
  /**
   * Implementation of Constructor with metric as pointer (Metric is pure
   * virtual and can't be declared here)
   * @param epsilon
   * @param metric
   */
  Geodesic(const int& epsilon, Metric& metric);
  /**
   * Implementation of the right-hand side, cfg. Ode<dim>::getRhs.
   */
  RhsVector getRhs(const StateVector& state, const double& time) final;

 private:
  int epsilon;
  Metric* metric;
};

}  // namespace ode
}  // namespace mogli

#endif  // MOGLI_GEODESIC_H
