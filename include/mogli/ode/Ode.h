#ifndef MOGLI_ODE_H
#define MOGLI_ODE_H

#include <array>

#include "mogli/core/Trajectory.h"

namespace mogli {
namespace ode {

/**
 * A generic class representing ordinary differential equations of the form
 * @f$ y'(x) = f(x, y) @f$.
 *
 * @tparam dim The dimension of the state space.
 */
template <std::size_t dim>
class Ode {
 public:
  using RhsVector = std::array<double, dim>;
  using StateVector = core::StateVector<dim>;

  Ode(){};
  virtual ~Ode(){};
  /**
   * The right-hand side of the differential equation.
   *
   * @param state The state @f$ y @f$ for which the rhs should be evaluated.
   * @param time The time @f$ x @f$ for which the rhs should be evaluated.
   * @return The value of the right-hand side.
   */
  virtual RhsVector getRhs(const StateVector& state, const double& time) = 0;
};

}  // namespace ode
}  // namespace mogli

#endif