//
// Created by patri on 21.03.2020.
//

#ifndef MOGLI_METRICMINKOWSKI_H
#define MOGLI_METRICMINKOWSKI_H

#include "Metric.h"

namespace mogli {
namespace ode {

class MetricMinkowski : public Metric {
 public:
  MetricMinkowski();
  void setPosition(const double time, const Position& position) final;
  std::array<double, 4> transformLif2Lab(
      const std::array<double, 4>& u_lif) final;
};

}  // namespace ode
}  // namespace mogli
#endif  // MOGLI_METRICMINKOWSKI_H
