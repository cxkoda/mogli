//
// Created by patri on 27.03.2020.
//

#ifndef MOGLI_METRICMINKOWSKIPOLAR_H
#define MOGLI_METRICMINKOWSKIPOLAR_H

#include "Metric.h"

namespace mogli {
namespace ode {

class MetricMinkowskiPolar : public Metric {
 public:
  MetricMinkowskiPolar();
  void setPosition(const double time, const Position& position) final;
  std::array<double, 4> transformLif2Lab(
      const std::array<double, 4>& u_lif) final;
};

}  // namespace ode
}  // namespace mogli
#endif  // MOGLI_METRICMINKOWSKIPOLAR_H
