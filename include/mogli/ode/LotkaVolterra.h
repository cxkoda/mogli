//
// Created by patrick on 05.03.20.
// dummy file for correct structure from Exponential.h
//

#ifndef MOGLI_LOTKAVOLTERRA_H
#define MOGLI_LOTKAVOLTERRA_H

#include "Ode.h"

namespace mogli {
namespace ode {

/**
 * OLD --> A one-dimensional ODE resulting in exponential growth or decay.
 * A two-dimensional ODE
 * OLD --> The ode is given by @f$ y'(x) = \lambda y @f$.
 *  N1' = N1(eps1 - gamma1*N2)
 *  N2' = -N2(eps2 - gamma2*N1)
 */
class LotkaVolterra : public Ode<2> {
 public:
  /**
   * Construct an ODE for the LotkaVolterra equations (Jeager Beute Schema)
   *
   * @param eps1 > 0      Reproduktionsrate Beute
   *        eps2 > 0      Sterberate Raeuber
   *        gamma1 > 0    Fressrate R = Sterberate B
   *        gamma2 > 0    Reproduktionsrate R
   *        The coefficients on the rhs.
   */
  explicit LotkaVolterra(const double& eps1,
                         const double& eps2,
                         const double& gamma1,
                         const double& gamma2);
  /**
   * Implementation of the right-hand side, cfg. Ode<dim>::getRhs.
   */
  RhsVector getRhs(const StateVector& state, const double& time) final;

 private:
  double eps1, eps2, gamma1, gamma2;
};

}  // namespace ode
}  // namespace mogli

#endif  // MOGLI_LOTKAVOLTERRA_H
