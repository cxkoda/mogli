#ifndef MOGLI_HARMONICOSCILLATOR_H
#define MOGLI_HARMONICOSCILLATOR_H

#include <math.h>

#include "Ode.h"

namespace mogli {
namespace ode {

/**
 *
 * The ode is given by x''(t) = - constant/mass * x(t).
 * Swap equation second order in 2 quations first order
 * x1'(x) = x2(x)
 * x2'(x) = - constant/mass * x1(t)
 */
class HarmonicOscillator : public Ode<2> {
 public:
  using RhsVector = typename Ode<2>::RhsVector;
  using StateVector = typename Ode<2>::StateVector;

  /**
   * Construct an harmonic oszillator ODE with given factors mass and constant
   * factor.
   */
  explicit HarmonicOscillator(const double& mass, const double& constant) {
    this->coefficient = constant / mass;
  }

  /**
   * Implementation of the right-hand side, cfg. Ode<dim>::getRhs.
   */

  RhsVector getRhs(const StateVector& state,
                   [[maybe_unused]] const double& time) final {
    RhsVector RHS;

    RHS[0] = state[1];
    RHS[1] = -coefficient * state[0];

    return RHS;
  }

  /**
   * The analytical solution of the ODE.
   *
   * @param initialState Initial state for which the solution is computed.
   * @param time The time for which the solution is computed.
   * @return The solution state.
   */
  StateVector getAnalytical(const StateVector& initialState,
                            const double& time) const {
    StateVector finalState;
    finalState[0] = initialState[0] * cos(sqrt(coefficient) * time);
    finalState[1] =
        initialState[1] * sqrt(coefficient) * sin(sqrt(coefficient) * time);
    return finalState;
  }

 private:
  double coefficient;
};

}  // namespace ode
}  // namespace mogli

#endif