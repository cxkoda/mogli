#ifndef MOGLI_EXPLICITEULER_H
#define MOGLI_EXPLICITEULER_H

#include <algorithm>

#include "mogli/integrator/Integrator.h"

namespace mogli {
namespace integrator {

/**
 * Integrator following an explicit euler scheme.
 */
template <std::size_t dim>
class ExplicitEuler final : public Integrator<dim> {
 public:
  /**
   * Constructs an explict euler solver using a fixed step-size.
   *
   * @param stepSize The stepsize to be used.
   */
  explicit ExplicitEuler(double stepSize) { this->stepSize = stepSize; }

  core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) override {
    // Prepare a trajectory
    core::Trajectory<dim> trajectory;
    trajectory.push_back({0, initialValue});

    // Loop until the stopping criterion is met
    while (
        !stoppingCriterion(trajectory.back().time, trajectory.back().state)) {
      // Reference to the starting state
      const double time = trajectory.back().time;
      const core::StateVector<dim>& oldState = trajectory.back().state;

      // Compute the right-hand-side of the ode
      const auto rhs = ode.getRhs(oldState, time);

      // Compute the new state
      core::StateVector<dim> newState;
      for (size_t iDim = 0; iDim < dim; ++iDim) {
        newState[iDim] = stepSize * rhs[iDim] + oldState[iDim];
      }

      // Append the new state to the trajectory
      trajectory.push_back({time + stepSize, std::move(newState)});
    }

    // Return the final trajectory through state space
    return trajectory;
  }

 private:
  double stepSize;
};

}  // namespace integrator
}  // namespace mogli

#endif