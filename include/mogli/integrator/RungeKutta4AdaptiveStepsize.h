//
// Created by Dominik on 02.04.20
//

#ifndef MOGLI_RUNGEKUTTA4_ADAPTIVE_STEPSIZE_H
#define MOGLI_RUNGEKUTTA4_ADAPTIVE_STEPSIZE_H

#include <algorithm>
#include <cmath>

#include "mogli/integrator/Integrator.h"
#include "mogli/integrator/RungeKutta4.h"

namespace mogli {
namespace integrator {

/**
 * Integrator following an explicit runge kutta 4 with adaptive stepsize.
 */
template <std::size_t dim>
class RungeKutta4AdaptiveStepsize final : public Integrator<dim> {
 public:
  /**
   * Constructs an runge kutta 4 solver.
   *
   * @param stepSize The stepsize to be used.
   */
  explicit RungeKutta4AdaptiveStepsize(double stepSize, double tolerance) {
    this->stepSize0 = stepSize;
    this->tolerance = tolerance;
  }

  core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) override {
    // Prepare a trajectory
    core::Trajectory<dim> trajectory;
    trajectory.push_back({0, initialValue});
    double stepSize = stepSize0;
    // Loop until the stopping criterion is met
    while (
        !stoppingCriterion(trajectory.back().time, trajectory.back().state)) {
      // Calculate the full step with runge kutta 4
      const core::StateVector<dim> fullstep =
          mogli::integrator::RungeKutta4<dim>::doSingleStep(
              ode, trajectory.back().time, trajectory.back().state, stepSize);

      // Calculate 2 half steps with runge kutta 4
      const core::StateVector<dim> halfstep =
          mogli::integrator::RungeKutta4<dim>::doSingleStep(
              ode, trajectory.back().time, trajectory.back().state,
              stepSize * 0.5);

      const core::StateVector<dim> nexthalfstep =
          mogli::integrator::RungeKutta4<dim>::doSingleStep(
              ode, trajectory.back().time + stepSize * 0.5, halfstep,
              stepSize * 0.5);

      // Calculate the error between the results of fullstep and 2 * half step
      double error = 0.;
      core::StateVector<dim> newState;
      for (size_t iDim = 0; iDim < dim; ++iDim) {
        error = error + std::abs(nexthalfstep[iDim] - fullstep[iDim]);
        newState[iDim] =
            nexthalfstep[iDim] + (nexthalfstep[iDim] - fullstep[iDim]) / 15;
      }
      error = error / 15.;
      if (error < tolerance) {
        trajectory.push_back({trajectory.back().time + stepSize, newState});
      }
      stepSize = 0.9 * stepSize * std::pow(tolerance / error, 0.2);
      if (stepSize > 1e150) {
        stepSize = 1e50;
      }
    }
    // Return the final trajectory through state space
    return trajectory;
  }

 private:
  double stepSize0;
  double tolerance;
};  // namespace integrator

}  // namespace integrator
}  // namespace mogli

#endif  // MOGLI_RUNGEKUTTA4_ADAPTIVE_STEPSIZE_H
