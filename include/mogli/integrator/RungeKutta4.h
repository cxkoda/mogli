//
// Created by Dominik on 25.03.20
//

#ifndef MOGLI_RUNGEKUTTA4_H
#define MOGLI_RUNGEKUTTA4_H

#include <algorithm>

#include "mogli/integrator/Integrator.h"

namespace mogli {
namespace integrator {

/**
 * Integrator following an explicit runge kutta 4 scheme.
 */
template <std::size_t dim>
class RungeKutta4 final : public Integrator<dim> {
 public:
  /**
   * Constructs an runge kutta 4 solver using a fixed step-size.
   *
   * @param stepSize The stepsize to be used.
   */
  explicit RungeKutta4(double stepSize) { this->stepSize = stepSize; }

  static core::StateVector<dim> doSingleStep(
      ode::Ode<dim>& ode,
      const double time,
      const core::StateVector<dim>& initialValue,
      const double stepSize) {
    core::Trajectory<dim> trajectoryRK4;

    // Compute the right-hand-side of the ode
    const auto rhs0 = ode.getRhs(initialValue, time);

    // Compute first step
    core::StateVector<dim> state1;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state1[iDim] = stepSize * rhs0[iDim] * 0.5 + initialValue[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs1 = ode.getRhs(state1, time + stepSize * 0.5);

    // Compute second step
    core::StateVector<dim> state2;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state2[iDim] = stepSize * rhs1[iDim] * 0.5 + initialValue[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs2 = ode.getRhs(state2, time + stepSize * 0.5);

    // Compute third step
    core::StateVector<dim> state3;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state3[iDim] = stepSize * rhs2[iDim] + initialValue[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs3 = ode.getRhs(state3, time + stepSize);

    // Compute the new state
    core::StateVector<dim> newState;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      newState[iDim] = initialValue[iDim] + stepSize / 6. *
                                                (rhs0[iDim] + 2. * rhs1[iDim] +
                                                 2. * rhs2[iDim] + rhs3[iDim]);
    }

    return newState;
  }

  core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) override {
    // Prepare a trajectory
    core::Trajectory<dim> trajectory;
    trajectory.push_back({0., initialValue});

    // Loop until the stopping criterion is met
    while (
        !stoppingCriterion(trajectory.back().time, trajectory.back().state)) {
      // Reference to the starting state
      const double time = trajectory.back().time;

      // Append the new state to the trajectory
      trajectory.push_back(
          {time + stepSize,
           doSingleStep(ode, time, trajectory.back().state, stepSize)});
    }

    // Return the final trajectory through state space
    return trajectory;
  }

 private:
  double stepSize;
};

}  // namespace integrator
}  // namespace mogli

#endif  // MOGLI_RUNGEKUTTA4_H