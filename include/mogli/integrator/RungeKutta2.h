//
// Created by Dominik on 10.03.20
//

#ifndef MOGLI_RUNGEKUTTA2_H
#define MOGLI_RUNGEKUTTA2_H

#include <algorithm>

#include "mogli/integrator/Integrator.h"

namespace mogli {
namespace integrator {

/**
 * Integrator following an explicit runge kutta 2 scheme.
 */
template <std::size_t dim>
class RungeKutta2 final : public Integrator<dim> {
 public:
  /**
   * Constructs an runge kutta 2 solver using a fixed step-size.
   *
   * @param stepSize The stepsize to be used.
   */
  explicit RungeKutta2(double stepSize) { this->stepSize = stepSize; }

  core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) override {
    // Prepare a trajectory
    core::Trajectory<dim> trajectory;
    trajectory.push_back({0, initialValue});

    // Loop until the stopping criterion is met
    while (
        !stoppingCriterion(trajectory.back().time, trajectory.back().state)) {
      // Reference to the starting state
      const double time = trajectory.back().time;
      const core::StateVector<dim>& oldState = trajectory.back().state;

      // Compute the right-hand-side of the ode (first step)
      const auto rhs0 = ode.getRhs(oldState, time);

      // Compute first step
      core::StateVector<dim> state1;
      for (size_t iDim = 0; iDim < dim; ++iDim) {
        state1[iDim] = 0.5 * stepSize * rhs0[iDim] + oldState[iDim];
      }

      // Compute the right-hand-side of the ode (second step)
      const auto rhs1 = ode.getRhs(state1, time + stepSize / 2);

      // Compute the new state
      core::StateVector<dim> newState;
      for (size_t iDim = 0; iDim < dim; ++iDim) {
        newState[iDim] = oldState[iDim] + stepSize * rhs1[iDim];
      }

      // Append the new state to the trajectory
      trajectory.push_back({time + stepSize, std::move(newState)});
    }

    // Return the final trajectory through state space
    return trajectory;
  }

 private:
  double stepSize;
};

}  // namespace integrator
}  // namespace mogli

#endif  // MOGLI_RUNGEKUTTA2_H