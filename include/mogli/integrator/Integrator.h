#ifndef MOGLI_INTEGRATOR_H
#define MOGLI_INTEGRATOR_H

#include <functional>
#include <vector>

#include "mogli/ode/Ode.h"

namespace mogli {
namespace integrator {

/**
 * Generic Integrator for ODEs.
 *
 * @tparam dim The dimension of the ODE, i.e. the dimension of the state space.
 */
template <std::size_t dim>
class Integrator {
 public:
  virtual ~Integrator(){};

  /**
   * Solve a given ODE.
   *
   * The integration starts implicitly at time=0
   *
   * @param ode Ordinary differential equation to be solved.
   * @param initialValue The initial state at time=0
   * @param stoppingCriterion A function returning `true`, when the integrator
   * should stop.
   * @return A trajectory through state space.
   */
  virtual core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) = 0;
};

}  // namespace integrator
}  // namespace mogli

#endif  // MOGLI_INTEGRATOR_H
