//
// Created by Dominik on 05.04.20
//

#ifndef MOGLI_FEHLBERGRK45_H
#define MOGLI_FEHLBERGRK45_H

#include <algorithm>
#include <cmath>
#include <numeric>

#include "mogli/integrator/Integrator.h"

namespace mogli {
namespace integrator {

/**
 * Integrator following an explicit fehlberg runge kutta 4-5 scheme.
 */
template <std::size_t dim>
class FehlbergRK45 final : public Integrator<dim> {
 public:
  explicit FehlbergRK45(double stepSize, double tolerance) {
    this->stepSize0 = stepSize;
    this->tolerance = tolerance;
  }

  using RhsStack = std::array<typename ode::Ode<dim>::RhsVector, 6>;

  static RhsStack calcRhs(ode::Ode<dim>& ode,
                          const core::StateVector<dim> oldstate,
                          const double time,
                          const double stepSize) {
    // Compute the right-hand-side of the ode
    const auto rhs0 = ode.getRhs(oldstate, time);

    // Compute first step
    core::StateVector<dim> state1;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state1[iDim] = a[0] * stepSize * rhs0[iDim] + oldstate[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs1 = ode.getRhs(state1, time + t[0] * stepSize);
    // Compute second step
    core::StateVector<dim> state2;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state2[iDim] = a[1] * stepSize * rhs0[iDim] +
                     b[0] * stepSize * rhs1[iDim] + oldstate[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs2 = ode.getRhs(state2, time + t[1] * stepSize);
    // Compute third step
    core::StateVector<dim> state3;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state3[iDim] = a[2] * stepSize * rhs0[iDim] +
                     b[1] * stepSize * rhs1[iDim] +
                     c[0] * stepSize * rhs2[iDim] + oldstate[iDim];
    }
    // Compute the right-hand-side of the ode
    const auto rhs3 = ode.getRhs(state3, time + t[2] * stepSize);
    // Compute fourth step
    core::StateVector<dim> state4;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state4[iDim] = a[3] * stepSize * rhs0[iDim] +
                     b[2] * stepSize * rhs1[iDim] +
                     c[1] * stepSize * rhs2[iDim] +
                     d[0] * stepSize * rhs3[iDim] + oldstate[iDim];
    }
    // Compute the right-hand-side of the ode
    const auto rhs4 = ode.getRhs(state4, time + t[3] * stepSize);

    // Compute fifth step
    core::StateVector<dim> state5;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      state5[iDim] =
          a[4] * stepSize * rhs0[iDim] + b[3] * stepSize * rhs1[iDim] +
          c[2] * stepSize * rhs2[iDim] + d[1] * stepSize * rhs3[iDim] +
          e * stepSize * rhs4[iDim] + oldstate[iDim];
    }

    // Compute the right-hand-side of the ode
    const auto rhs5 = ode.getRhs(state5, time + t[4] * stepSize);

    return {rhs0, rhs1, rhs2, rhs3, rhs4, rhs5};
  }

  static core::StateVector<dim> calcSolutionRK4(
      const RhsStack& rhs,
      const core::StateVector<dim> oldState,
      const double stepSize) {
    core::StateVector<dim> solution1;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      solution1[iDim] =
          stepSize * (sol1[0] * rhs[0][iDim] + sol1[1] * rhs[1][iDim] +
                      sol1[2] * rhs[2][iDim] + sol1[3] * rhs[3][iDim] +
                      sol1[4] * rhs[4][iDim] + sol1[5] * rhs[5][iDim]) +
          oldState[iDim];
    }
    return solution1;
  }

  static core::StateVector<dim> calcSolutionRK5(
      const RhsStack& rhs,
      const core::StateVector<dim> oldState,
      const double stepSize) {
    core::StateVector<dim> solution2;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      solution2[iDim] =
          stepSize * (sol2[0] * rhs[0][iDim] + sol2[1] * rhs[1][iDim] +
                      sol2[2] * rhs[2][iDim] + sol2[3] * rhs[3][iDim] +
                      sol2[4] * rhs[4][iDim] + sol2[5] * rhs[5][iDim]) +
          oldState[iDim];
    }
    return solution2;
  }

  static core::StateVector<dim> calcErrorVector(const RhsStack& rhs,
                                                const double stepSize) {
    core::StateVector<dim> errorVector;
    for (size_t iDim = 0; iDim < dim; ++iDim) {
      errorVector[iDim] =
          stepSize * (diff[0] * rhs[0][iDim] + diff[1] * rhs[1][iDim] +
                      diff[2] * rhs[2][iDim] + diff[3] * rhs[3][iDim] +
                      diff[4] * rhs[4][iDim] + diff[5] * rhs[5][iDim]);
    }
    return errorVector;
  }

  static double calcError(const RhsStack& rhs, const double stepSize) {
    const auto errorVector = calcErrorVector(rhs, stepSize);
    static const auto abssum = [](auto x, auto y) { return x + std::abs(y); };
    const double error =
        std::accumulate(errorVector.begin(), errorVector.end(), 0., abssum);

    return error;
  }

  core::Trajectory<dim> solve(
      ode::Ode<dim>& ode,
      const core::StateVector<dim>& initialValue,
      const std::function<double(double, const core::StateVector<dim>&)>&
          stoppingCriterion) override {
    // Prepare a trajectory
    core::Trajectory<dim> trajectory;
    trajectory.push_back({0, initialValue});
    double stepSize = stepSize0;
    // Loop until the stopping criterion is met
    while (
        !stoppingCriterion(trajectory.back().time, trajectory.back().state)) {
      // Compute coefficients
      const auto rhs = calcRhs(ode, trajectory.back().state,
                               trajectory.back().time, stepSize);

      const double error = calcError(rhs, stepSize);

      if (error < tolerance) {
        trajectory.push_back(
            {trajectory.back().time + stepSize,
             calcSolutionRK5(rhs, trajectory.back().state, stepSize)});
      }
      stepSize = 0.9 * stepSize * std::pow(tolerance / error, 0.2);
      if (stepSize > 1e150) {
        stepSize = 1e50;
      }
    }
    return trajectory;
  }

 private:
  static constexpr std::array<double, 5> t = {1. / 4., 3. / 8., 12. / 13., 1,
                                              1. / 2.};
  static constexpr std::array<double, 5> a = {1. / 4., 3. / 32., 1932. / 2197.,
                                              439. / 216., -8. / 27.};
  static constexpr std::array<double, 4> b = {9. / 32., -7200. / 2197., -8, 2};
  static constexpr std::array<double, 3> c = {7296. / 2197., 3680. / 513.,
                                              -3544. / 2565.};
  static constexpr std::array<double, 2> d = {-845. / 4104., 1859. / 4104.};
  static constexpr double e = -11. / 40.;
  static constexpr std::array<double, 6> sol1 = {
      25. / 216., 0., 1408. / 2565., 2197. / 4104., -1. / 5., 0.};
  static constexpr std::array<double, 6> sol2 = {
      16. / 135., 0., 6656. / 12825., 28561. / 56430., -9. / 50., 2. / 55.};
  static constexpr std::array<double, 6> diff = {
      sol1[0] - sol2[0], sol1[1] - sol2[1], sol1[2] - sol2[2],
      sol1[3] - sol2[3], sol1[4] - sol2[4], sol1[5] - sol2[5]};
  double stepSize0;
  double tolerance;
};

}  // namespace integrator
}  // namespace mogli

#endif  // MOGLI_FEHLBERGRK45_H