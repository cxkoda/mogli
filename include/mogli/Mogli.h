//
// Created by dave on 26.02.20.
//

#ifndef MOGLI_MOGLI_H
#define MOGLI_MOGLI_H

#include "Core.h"
#include "Integrator.h"
#include "Ode.h"
#include "Version.h"

#endif  // MOGLI_MOGLI_H
