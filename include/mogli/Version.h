//
// Created by dave on 04.03.20.
//

#ifndef MOGLI_VERSION_H
#define MOGLI_VERSION_H

#include <string>

namespace mogli {

/**
 * Interface to version information
 */
class Version {
 public:
  /**
   * The Project Version set in Cmake.
   */
  static std::string getVersion();

  /**
   * Is the metadata populated? We may not have metadata if there wasn't a .git
   * directory (e.g. downloaded source code without revision history).
   */
  static bool isGitPopulated();

  /**
   * Were there any uncommitted changes that won't be reflected in the CommitID?
   */
  static bool isGitDirty();

  /**
   * The commit SHA1.
   */
  static std::string getGitCommitSHA1();
};

}  // namespace mogli

#endif  // MOGLI_VERSION_H
