//
// Created by dave on 25.02.20.
//

#ifndef MOGLI_TRAJECTORY_H
#define MOGLI_TRAJECTORY_H

#include <iostream>
#include <tuple>
#include <vector>

#include "mogli/core/StateVector.h"

namespace mogli {
namespace core {

/**
 * A single point on a trajectory through state space.
 */
template <std::size_t dim>
struct TrajectoryPoint {
  double time;
  StateVector<dim> state;
};

/**
 * Equality comparison operator for TrajectoryPoint
 *
 * Only true if both time and state are equal.
 */
template <std::size_t dim>
bool operator==(const TrajectoryPoint<dim>& left,
                const TrajectoryPoint<dim>& right) {
  return std::tie(left.time, left.state) == std::tie(right.time, right.state);
}

/**
 * Inequality comparison operator for TrajectoryPoint
 *
 * True if either time or state are not equal.
 */
template <std::size_t dim>
bool operator!=(const TrajectoryPoint<dim>& left,
                const TrajectoryPoint<dim>& right) {
  return !(left == right);
}

/**
 * Stream operator overload for TrajectoryPoints
 */
template <std::size_t dim>
std::ostream& operator<<(std::ostream& os, const TrajectoryPoint<dim>& point) {
  os << "time=" << point.time << ", state=" << point.state;
  return os;
}

/**
 * A list of state vectors representing a trajectory through state space.
 */
template <std::size_t dim>
using Trajectory = std::vector<TrajectoryPoint<dim> >;

/**
 * Stream operator overload for Trajectory
 *
 * Outputs each TrajectoryPoint.
 */
template <std::size_t dim>
std::ostream& operator<<(std::ostream& os, const Trajectory<dim>& trajectory) {
  for (const auto& point : trajectory) {
    os << point;
    if (&point != &trajectory.back()) {
      os << "\n";
    }
  }
  return os;
}

}  // namespace core
}  // namespace mogli

#endif  // MOGLI_TRAJECTORY_H
