#ifndef MOGLI_STORAGE_H
#define MOGLI_STORAGE_H

#include <hdf5.h>

#include <string>
#include <type_traits>

#include "mogli/core/StateVectorList.h"
#include "mogli/core/Trajectory.h"

namespace mogli {
namespace core {

namespace h5details {
/** HDF5 Dataset RAII wrapper */
class h5Dataset {
 public:
  h5Dataset(hid_t dataset, hid_t dataspace, hid_t datatype);
  ~h5Dataset();
  hid_t dataspace, datatype, dataset;
};

/**
 * Add an attribute to a given hdf5 object (dataset or group)
 * @tparam T data type
 * @param object The HDF5 object, where the attribute belongs to
 * @param attributeName The attribute name
 * @param data The attribute data
 * @return The attribute handle
 */
template <class T>
herr_t addAttribute(const hid_t object,
                    const std::string& attributeName,
                    const T& data);

}  // namespace h5details

/**
 * HDF5 Trajectory Storage interface
 *
 * Each trajectory is stored in an individual rank 2 dataset with rows
 * corresponding to a given timestep and the columns to a state component.
 */
class Storage {
 public:
  /**
   * Create an HDF5 file under a given filename
   */
  explicit Storage(const std::string& filename);
  ~Storage();

  /**
   * Add a trajectory to the file.
   * @tparam dim The state dimensionality.
   * @param trajectory The trajectory to be stored.
   * @return The dataset id.
   */
  template <std::size_t dim>
  h5details::h5Dataset storeTrajectory(const core::Trajectory<dim>& trajectory);

  /**
   * Add a list of state vectors to the file.
   * The list is stored as 2D dataset (iEntry, iComponent)
   * @tparam dim The state dimensionality.
   * @param list The state vector list to be stored.
   * @param datasetName Name of the dataset. If omitted the current number of
   * lists will be used.
   * @return The dataset id.
   */
  template <std::size_t dim>
  h5details::h5Dataset storeStateVectorList(
      const core::StateVectorList<dim>& list,
      std::string datasetName = "");

  /**
   * Add an attribute to a given hdf5 object (dataset or group)
   * @tparam T data type
   * @param object The HDF5 object, where the attribute belongs to
   * @param attributeName The attribute name
   * @param data The attribute data
   * @return The attribute handle
   */
  template <class T>
  static herr_t addAttribute(const h5details::h5Dataset&,
                             const std::string& attributeName,
                             const T& data);

 private:
  template <typename T>
  using Buffer = std::vector<T>;
  using Dimensions = std::vector<hsize_t>;

  hid_t openFile(const std::string& filename);
  herr_t closeFile();

  static herr_t writeVersionInfo(hid_t hdf5file, const std::string& groupName);

  /**
   * Generic function to write a 1D buffer to a dataset with given
   * dimensionality
   * @param datasetName The name of the dataset to be written.
   * @param group The group where the dataset should be created in.
   * @param dimensions The dimensions of the dataset.
   * @param buffer 1D, memory-contiguous buffer to be written.
   * @return The dataset id.
   */
  static h5details::h5Dataset writeBuffer(const std::string& datasetName,
                                          const hid_t group,
                                          const Dimensions& dimensions,
                                          const Buffer<double>& buffer);

  const std::string filename;

  hid_t hdf5file, trajectoryGroup, stateVectorListGroup;
  int num_Trajectories;
  int numStateVectorLists;
};

template <std::size_t dim>
h5details::h5Dataset Storage::storeTrajectory(
    const core::Trajectory<dim>& trajectory) {
  Dimensions dimensions{trajectory.size(), dim + 1};

  // Copy the data to a contiguous write buffer
  Buffer<double> buffer(dimensions[0] * dimensions[1]);

  size_t iStore = 0;
  for (const auto point : trajectory) {
    buffer[iStore++] = point.time;
    for (const auto component : point.state) {
      buffer[iStore++] = component;
    }
  }

  std::string datasetName = std::to_string(num_Trajectories);
  auto dataset = writeBuffer(datasetName, trajectoryGroup, dimensions, buffer);

  num_Trajectories++;
  return dataset;
}

template <std::size_t dim>
h5details::h5Dataset Storage::storeStateVectorList(
    const core::StateVectorList<dim>& list,
    std::string datasetName) {
  Dimensions dimensions{list.size(), dim};

  // Copy the data to a contiguous write buffer
  Buffer<double> buffer(dimensions[0] * dimensions[1]);

  size_t iStore = 0;
  for (const auto point : list) {
    for (const auto component : point) {
      buffer[iStore++] = component;
    }
  }

  if (datasetName.empty()) {
    datasetName = std::to_string(numStateVectorLists);
  }

  auto dataset =
      writeBuffer(datasetName, stateVectorListGroup, dimensions, buffer);

  numStateVectorLists++;
  return dataset;
}

template <class T>
herr_t Storage::addAttribute(const h5details::h5Dataset& dataset,
                             const std::string& attributeName,
                             const T& data) {
  return h5details::addAttribute(dataset.dataset, attributeName, data);
}

namespace h5details {

template <class T>
struct dependent_false : std::false_type {};

/**
 * Returns the Hdf5 datatype for a given c++ type
 * @tparam T The Type
 * @return The respective Hdf5 data
 */
template <class T>
constexpr hid_t getH5DataType() {
  hid_t datatype = 0;

  if constexpr (std::is_same<T, std::string>::value) {
    datatype = H5Tcopy(H5T_C_S1);
    H5Tset_size(datatype, H5T_VARIABLE);
  } else if constexpr (std::is_same<T, int>::value) {
    datatype = H5Tcopy(H5T_NATIVE_INT);
  } else if constexpr (std::is_same<T, double>::value) {
    datatype = H5Tcopy(H5T_NATIVE_DOUBLE);
  } else {
    static_assert(dependent_false<T>::value, "Unsupported Attribute Type");
  }

  return datatype;
}

template <typename T>
struct is_vector : public std::false_type {};

template <typename T, typename A>
struct is_vector<std::vector<T, A>> : public std::true_type {};

template <typename T>
struct is_array : public std::false_type {};

template <typename T, size_t N>
struct is_array<std::array<T, N>> : public std::true_type {};

/** Workaround for a problem with clang.
 * There seems to be a problem with `constexpr std::array<>::size()` in
 * `static_assert`
 * */
template <typename T>
struct getArraySize : public std::false_type {};

template <typename T, size_t N>
struct getArraySize<std::array<T, N>>
    : public std::integral_constant<size_t, N> {};

template <typename T>
herr_t addAttribute(const hid_t object,
                    const std::string& attributeName,
                    const T& data) {
  hid_t datatype;
  hid_t dataspace;

  if constexpr (h5details::is_vector<T>::value) {
    // Write vectors to attribute arrays
    datatype = h5details::getH5DataType<typename T::value_type>();
    dataspace = H5Screate(H5S_SIMPLE);
    hsize_t size = data.size();
    H5Sset_extent_simple(dataspace, 1, &size, NULL);
  } else if constexpr (h5details::is_array<T>::value) {
    // Convert arrays to vectors
    static_assert(getArraySize<T>::value > 0);

    std::vector<typename T::value_type> temp;
    std::copy(std::begin(data), std::end(data), std::back_inserter(temp));
    return addAttribute(object, attributeName, temp);
  } else {
    // Assume scalar attribute
    datatype = h5details::getH5DataType<T>();
    dataspace = H5Screate(H5S_SCALAR);
  }

  hid_t attribute = H5Acreate2(object, attributeName.c_str(), datatype,
                               dataspace, H5P_DEFAULT, H5P_DEFAULT);

  if constexpr (h5details::is_vector<T>::value) {
    H5Awrite(attribute, datatype, data.data());
  } else {
    H5Awrite(attribute, datatype, &data);
  }

  herr_t error = H5Aclose(attribute);
  H5Sclose(dataspace);
  H5Tclose(datatype);

  return error;
}
}  // namespace h5details

}  // namespace core
}  // namespace mogli

#endif  // MOGLI_STORAGE_H