//
// Created by dave on 25.02.20.
//

#ifndef MOGLI_STATEVECTOR_H
#define MOGLI_STATEVECTOR_H

#include <array>
#include <iostream>

namespace mogli {
namespace core {

/**
 * An element of the state space.
 *
 * Used to describe states in physical system, e.g. states in the phase space of
 * a given ODE.
 */
template <std::size_t dim>
using StateVector = std::array<double, dim>;

/**
 * Stream operator overload for StateVector
 *
 * Outputs all components.
 */
template <std::size_t dim>
std::ostream& operator<<(std::ostream& os, const StateVector<dim>& state) {
  os << "(";
  for (const auto& component : state) {
    os << component;
    if (&component != &state.back()) {
      os << ", ";
    }
  }
  os << ")";
  return os;
}

}  // namespace core
}  // namespace mogli

#endif  // MOGLI_STATEVECTOR_H
