//
// Created by dave on 04.06.20.
//

#ifndef MOGLI_STATEVECTORLIST_H
#define MOGLI_STATEVECTORLIST_H

#include <vector>

#include "StateVector.h"

namespace mogli {
namespace core {

/**
 * A list of state vectors.
 */
template <std::size_t dim>
using StateVectorList = std::vector<StateVector<dim> >;

/**
 * Stream operator overload for Trajectory
 *
 * Outputs each TrajectoryPoint.
 */
template <std::size_t dim>
std::ostream& operator<<(std::ostream& os, const StateVectorList<dim>& list) {
  for (const auto& point : list) {
    os << point;
    if (&point != &list.back()) {
      os << "\n";
    }
  }
  return os;
}

}  // namespace core
}  // namespace mogli

#endif  // MOGLI_STATEVECTORLIST_H
