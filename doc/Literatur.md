# Literatur Bacc: Black Holes

## ART, Metric, etc.

* [Ruder, Hanns, and Ruder, Margret. *Die Spezielle Relativitätstheorie*. 1993. Print. Vieweg-Studium.](https://permalink.obvsg.at/AC00623199)
  * Chap. 4.1 & 5
* [Dermer, Charles D., and Govind Menon. High Energy Radiation from Black Holes : Gamma Rays, Cosmic Rays, and Neutrinos. 2009. Print. Princeton Ser. in Astrophysics.](https://permalink.obvsg.at/AC07822370)
  * Chap. 3
* [Carrol, Sean: Lecture Notes on General Relativity](https://arxiv.org/pdf/gr-qc/9712019.pdf)
* [D'Inverno, Ray. Einführung in Die Relativitätstheorie. 1995. Print.](https://permalink.obvsg.at/AC00709359)
  * Chap. 5 & 6

## Numerics

* [Schwarz, Köckler, and Köckler, Norbert. Numerische Mathematik. 8., Aktualisierte Aufl.. ed. 2011. Print. Studium.](https://permalink.obvsg.at/AC08508612)
  * Chap. 8
* [Numerical Recipes](http://www.nrbook.com/a/bookcpdf.php)
  * [Auszug: Chapter 16](https://people.cs.clemson.edu/~dhouse/courses/817/papers/adaptive-h-c16-2.pdf)
* [Wikipedia: Adaptive Stepsizes](https://en.wikipedia.org/wiki/Adaptive_step_size)

## Geodesics

* [Bacchini et al. (2018), *Generalized, Energy-conserving Numerical Simulations of Particles in General Relativity. I. Time-like and Null Geodesics*](https://doi.org/10.3847%2F1538-4365%2Faac9ca)

## Visualisation and Test

* [Müller (2008), *Einstein rings as a tool for estimating distances and the mass of a Schwarzschild black hole*](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.77.124042)

* [Müller and Grave (2010), *GeodesicViewer - A tool for exploring geodesics in the theory of relativity*](https://www.sciencedirect.com/science/article/pii/S0010465509003233?via%3Dihub)

