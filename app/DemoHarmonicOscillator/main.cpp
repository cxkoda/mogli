#include <mogli/ode/HarmonicOscillator.h>
#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<2> state [[maybe_unused]]) {
  if (time > 20.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  std::cout << "Harmonischer Oszillator Demo:\n";
  double mass = 1.;
  double length = 2.;
  mogli::ode::HarmonicOscillator hermOsz_dgl(mass, length);

  // Set initial value x and x'
  mogli::core::StateVector<2> initialValue;
  initialValue[0] = 0.4;
  initialValue[1] = 0;

  // Build different solver for JB with different stepSize
  mogli::integrator::FehlbergRK45<2> FRK(1.e-2,1.e-2);
  mogli::integrator::RungeKutta4AdaptiveStepsize<2> ARK(1.e-2,1.e-8);

  auto trajectory_FRK =
      FRK.solve(hermOsz_dgl, initialValue, stop);
  auto trajectory_ARK =
      ARK.solve(hermOsz_dgl, initialValue, stop);

  mogli::core::Storage storage_FRK(
      "./harmOsz_trajectory_FehlbergRK.h5");
  mogli::core::Storage storage_ARK(
      "./harmOsz_trajectory_AdaptiveRK4.h5");

  storage_FRK.storeTrajectory(trajectory_FRK);
  storage_ARK.storeTrajectory(trajectory_ARK);
}