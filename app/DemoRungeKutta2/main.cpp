#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<1> state [[maybe_unused]]) {
  if (time > 2.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  // Test-case is exponential:
  mogli::ode::Exponential<1> expo(-1);

  // Build Runge-Kutta 2 solver
  mogli::integrator::RungeKutta2<1> runge(1.e-2);

  // Set initial value
  mogli::core::StateVector<1> initialValue;
  initialValue[0] = 1.;

  // Integrate thingy

  // with lambda stopping criterion
  auto trajectory = runge.solve(
      expo, initialValue,
      [](double time, mogli::core::StateVector<1> state [[maybe_unused]]) { return time > 2; });

  // with c++ function
  trajectory = runge.solve(expo, initialValue, stop);

  std::cout << trajectory.back() << std::endl;

  mogli::core::Storage storage("./trajectory_DemoRungeKutta2.h5");
  storage.storeTrajectory(trajectory);
}