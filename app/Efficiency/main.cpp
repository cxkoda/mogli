#include <mogli/ode/HarmonicOscillator.h>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<2> state [[maybe_unused]]) {
  if (time > 500.) {
    return true;
  } else {
    return false;
  }
}

/**
 * Representing a point along an curve
 */
struct Pair {
  double tolerance;
  double time;
  double error;
};

/**
 * Container storing the results of an efficiency test
 */
using Curve = std::vector<Pair>;

/**
 * Streaming operator overload for Curves
 */
std::ostream& operator<<(std::ostream& os, const Curve& curve) {
  for (const auto& point : curve) {
    os << point.tolerance << " " << point.time << " " << point.error;
    if (&point != &curve.back()) {
      os << "\n";
    }
  }
  return os;
}

/**
 * Write the Efficiency curve to an ascii file
 * @param filepath The filepath
 * @param data efficiency results
 * @return Success flag
 */
bool writeAsciiFile(const std::string filepath, const Curve& data) {
  std::ofstream file;
  file.open(filepath, std::ofstream::out | std::ofstream::trunc);

  if (file) {
    file << data;
    return true;
  } else {
    return false;
  }
}

int main(void) {
  int returnValue = 0;
  // HarmonicOscillator
  double mass = 1.;
  double constant = 2.;
  mogli::ode::HarmonicOscillator harmOsc_dgl(mass, constant);

  // Set initial value for harmonic oscillator
  mogli::core::StateVector<2> initialValue;
  initialValue[0] = 0.4;
  initialValue[1] = 0.;
  const size_t nIter = 30; //25;

  Curve results_FRK;
  Curve results_RKA;

  for (size_t i = 0; i < nIter; i++) {
    const double toleranceFRK45 = pow(0.43, 1 + i);
    const double toleranceARK4 = pow(0.47, 1 + i);

    mogli::integrator::FehlbergRK45<2> FRK45(1.e-2, toleranceFRK45);
    mogli::integrator::RungeKutta4AdaptiveStepsize<2> rungekuttaadaptive(
        1.e-2, toleranceARK4);
    // stop time for multiple calculations of the problem to minimize
    // fluctuations
    auto start1 = std::chrono::steady_clock::now();
    for (int iter = 0; iter < 500; iter++) {
      FRK45.solve(harmOsc_dgl, initialValue, stop);
    }
    auto end1 = std::chrono::steady_clock::now();
    double time1 =
        std::chrono::duration_cast<std::chrono::microseconds>(end1 - start1)
            .count() /
        40.;
    // calculate global error
    auto trajectory_FRK = FRK45.solve(harmOsc_dgl, initialValue, stop);
    const auto analytical1 =
        harmOsc_dgl.getAnalytical(initialValue, trajectory_FRK.back().time);
    double error_frk = abs(analytical1[0] - trajectory_FRK.back().state[0]);
    results_FRK.push_back({toleranceFRK45, time1, error_frk});
    std::ostringstream buffer_frk;
    buffer_frk << error_frk;
    std::string str_error_frk = buffer_frk.str();

    // stop time for multiple calculations of the problem to minimize
    // fluctuations
    auto start2 = std::chrono::steady_clock::now();
    for (int iter = 0; iter < 500; iter++) {
      rungekuttaadaptive.solve(harmOsc_dgl, initialValue, stop);
    }
    auto end2 = std::chrono::steady_clock::now();
    double time2 =
        std::chrono::duration_cast<std::chrono::microseconds>(end2 - start2)
            .count() /
        40.;
    // calculate global error
    auto trajectory_ARK4 =
        rungekuttaadaptive.solve(harmOsc_dgl, initialValue, stop);
    const auto analytical2 =
        harmOsc_dgl.getAnalytical(initialValue, trajectory_ARK4.back().time);
    double error_ark = abs(analytical2[0] - trajectory_ARK4.back().state[0]);
    results_RKA.push_back({toleranceARK4, time2, error_ark});

    std::ostringstream iteration;
    iteration << i;
    std::ostringstream buffer_ark;
    buffer_ark << error_ark;
    std::string str_error_ark = buffer_ark.str();
    std::string str_i = iteration.str();
    replace(str_error_ark.begin(), str_error_ark.end(), '.', ',');
    mogli::core::Storage storage_FRK("./"+str_i+"FehlbergRK_error_" +
                                               str_error_frk + ".h5");
    mogli::core::Storage storage_ARK("./"+str_i+"ARK4_error_" + str_error_ark +
                                               ".h5");
    storage_FRK.storeTrajectory(trajectory_FRK);
    storage_ARK.storeTrajectory(trajectory_ARK4);
  }
  returnValue += !writeAsciiFile("EfficiencyFRK.tsv", results_FRK);
  returnValue += !writeAsciiFile("EfficiencyRKA.tsv", results_RKA);
  return returnValue;
}