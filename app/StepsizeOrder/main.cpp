#include <fstream>
#include <iostream>
#include <string>

#include "mogli/Mogli.h"
bool stop(double time, mogli::core::StateVector<1> state [[maybe_unused]]) {
  if (time > 1.) {
    return true;
  } else {
    return false;
  }
}

/**
 * Representing a point along an order curve
 */
struct ErrorPair {
  double stepsize;
  double error;
};

/**
 * Container storing the results of an order test
 */
using OrderCurve = std::vector<ErrorPair>;

/**
 * Streaming operator overload for OrderCurves
 */
std::ostream& operator<<(std::ostream& os, const OrderCurve& curve) {
  for (const auto& point : curve) {
    os << point.stepsize << " " << point.error;
    if (&point != &curve.back()) {
      os << "\n";
    }
  }
  return os;
}

/**
 * Do the order test for a given integrator
 * @tparam integrator_t The integrator type to be tested
 * @param initialStepsize The initial stepsize
 * @param stepsizeFraction Factor by which the stepsize is reduced after each
 * iteration
 * @param nIter Number of iterations
 * @return
 */
template <class integrator_t>
OrderCurve doOrderTest(const double initialStepsize,
                       const double stepsizeFraction,
                       const size_t nIter) {
  mogli::ode::Exponential<1> exponential(-1.);
  mogli::core::StateVector<1> initialValue({1.});

  OrderCurve results;

  double stepsize = initialStepsize;
  for (size_t i = 0; i < nIter; i++) {
    // Build the solver
    integrator_t solver(stepsize);

    // Do the test
    const auto numerical =
        solver.solve(exponential, initialValue, stop).back();
    const auto analytical = exponential.getAnalytical(initialValue, numerical.time);
    const double error = numerical.state[0] - analytical[0];
    results.push_back({stepsize, error});

    stepsize = stepsizeFraction * stepsize;
  }

  return results;
}

/**
 * Write the order curve to an ascii file
 * @param filepath The filepath
 * @param data The order test results
 * @return Success flag
 */
bool writeAsciiFile(const std::string filepath, const OrderCurve& data) {
  std::ofstream file;
  file.open(filepath, std::ofstream::out | std::ofstream::trunc);

  if (file) {
    file << data;
    return true;
  } else {
    return false;
  }
}

int main(void) {
  int returnValue = 0;

  const double initialStepsize = 0.5;
  const double stepsizeFraction = 0.5;
  const size_t nIter = 20;

  const auto resultsExplicitEuler =
      doOrderTest<mogli::integrator::ExplicitEuler<1> >(
          initialStepsize, stepsizeFraction, nIter);
  returnValue +=
      !writeAsciiFile("OrderExplicitEuler.tsv", resultsExplicitEuler);
  std::cout << "Explicit Euler:"
            << "\n"
            << resultsExplicitEuler << std::endl;

  const auto resultsRungeKutta2 =
      doOrderTest<mogli::integrator::RungeKutta2<1> >(initialStepsize,
                                                      stepsizeFraction, nIter);
  returnValue += !writeAsciiFile("OrderRungeKutta2.tsv", resultsRungeKutta2);
  std::cout << "Runge Kutta 2:"
            << "\n"
            << resultsRungeKutta2 << std::endl;

  const auto resultsHeun =
      doOrderTest<mogli::integrator::Heun<1> >(initialStepsize,
                                                      stepsizeFraction, nIter);
  returnValue += !writeAsciiFile("OrderHeun.tsv", resultsHeun);
  std::cout << "Heun:"
            << "\n"
            << resultsHeun << std::endl;

  const auto resultsRungeKutta4 =
      doOrderTest<mogli::integrator::RungeKutta4<1> >(initialStepsize,
                                               stepsizeFraction, nIter);
  returnValue += !writeAsciiFile("OrderRungeKutta4.tsv", resultsRungeKutta4);
  std::cout << "RungeKutta4:"
            << "\n"
            << resultsRungeKutta4 << std::endl;

  return returnValue;
}