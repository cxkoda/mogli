#include <mogli/ode/Geodesic.h>
#include <mogli/ode/MetricMinkowskiPolar.h>
#include <mogli/ode/MetricSchwarzschild.h>

#include <chrono>
#include <iostream>

#include "mogli/Mogli.h"

/**
 *
 * @param x_old
 * @param v_old
 * @param x_new
 * @param v_new
 */
void transform_cartesian_spherical(std::array<double, 3> x_old,
                                   std::array<double, 3> v_old,
                                   std::array<double, 3>& x_new,
                                   std::array<double, 3>& v_new) {
  double r =
      sqrt(x_old[0] * x_old[0] + x_old[1] * x_old[1] + x_old[2] * x_old[2]);

  double theta = 0;  // 0-pi
  if (r == 0)
    theta = M_PI / 2;
  else
    theta = acos(x_old[2] / r);  // acos(z/r)

  // theta = M_PI / 2;
  double phi = atan2(x_old[1], x_old[0]);

  double dirX = v_old[0];
  double dirY = v_old[1];
  double dirZ = v_old[2];

  double vr = 0;
  double vphi = 0;
  double vtheta = 0;

  vr = dirX * sin(theta) * cos(phi) + dirY * sin(theta) * sin(phi) +
       dirZ * cos(theta);  // dirR
  vtheta = dirX * cos(theta) * cos(phi) / r + dirY * cos(theta) * sin(phi) / r -
           dirZ * sin(theta) / r;  // dirTheta
  vphi = -dirX * sin(phi) / r / sin(theta) +
         dirY * cos(phi) / r / sin(theta);  // dirPhi

  x_new = {r, theta, phi};
  v_new = {vr, vtheta, vphi};
}

/**
 * define stop condition for solver
 */
bool stop(double time, mogli::core::StateVector<6> state) {
  if (time > 100 || state[0] <= 2) {  // 1.6375425337086245 //state[0]=radius
    return true;
  } else {
    return false;
  }
}

/**
 * rotate initial direction vector in x
 * @param vec
 * @param chi_deg
 * @return
 */
std::array<double, 4> ret_rot_x(std::array<double, 4> vec, double chi_deg) {
  std::array<double, 4> vec_rot;
  double chi = (chi_deg / 180.0) * M_PI;

  vec_rot[1] = vec[1];
  vec_rot[2] = vec[2] * cos(chi) - sin(chi) * vec[3];
  vec_rot[3] = vec[2] * sin(chi) + cos(chi) * vec[3];

  return vec_rot;
}

/**
 * simulate and save a trajectory for given starting parameters
 * @param xi ... starting point
 * @param alpha ...starting direction
 * @param beta  ... starting direction
 * @param chi_deg ... in case you want to rotate
 * @param stepsize
 * @param tolerance ... in case an adaptive solver is used
 * @param MK ... metric
 * @param DGL
 * @param storage
 */
void save_trajectory(std::array<double, 3> xi,
                     double alpha,
                     double beta,
                     double chi_deg,
                     double stepsize,
                     double tolerance,
                     mogli::ode::MetricSchwarzschild& MK,
                     mogli::ode::Geodesic& DGL,
                     mogli::core::Storage& storage) {
  std::array<double, 4> u_prime = {
      1, -cos(beta) * cos(alpha), sin(beta),
      sin(alpha) * cos(beta)};  // sin(alpha) * cos(beta)};

  u_prime = ret_rot_x(u_prime, chi_deg);

  MK.setPosition(0, xi);
  auto u = MK.transformLif2Lab(u_prime);

  std::array<double, 3> vi = {u[1], u[2], u[3]};
  double v0 = MK.compute_u0_contra(vi, 0);
  u = MK.compute_u_i_cov(vi, v0);

  /*
  std::cout << '\n';
  std::cout << xi[0];
  std::cout << '\t';
  std::cout << xi[1];
  std::cout << '\t';
  std::cout << xi[2];
  std::cout << '\t';
  std::cout << vi[0];
  std::cout << '\t';
  std::cout << vi[1];
  std::cout << '\t';
  std::cout << vi[2];
  std::cout << "\n";
*/

  // Set initial value
  mogli::core::StateVector<6> initialValue;
  initialValue[0] = xi[0];
  initialValue[1] = xi[1];
  initialValue[2] = xi[2];
  initialValue[3] = u[1];
  initialValue[4] = u[2];
  initialValue[5] = u[3];

  // time start (if you want to readout calculation time)
  // auto start2 = std::chrono::steady_clock::now();

  // different solver
  // mogli::integrator::RungeKutta4AdaptiveStepsize<6> rungekutta(stepsize,
  //                                                           tolerance);
  mogli::integrator::FehlbergRK45<6> rungekutta(stepsize, tolerance);
  // mogli::integrator::Heun<6> rungekutta(stepsize);
  auto trajectory = rungekutta.solve(DGL, initialValue, stop);

  // time end (if you want to readout calculation time)
  // auto end2 = std::chrono::steady_clock::now();
  // time diff
  // double delta =
  //  std::chrono::duration_cast<std::chrono::microseconds>(end2 - start2)
  //      .count()/40.;
  // std::cout << delta << ',';

  // display last point for debugging
  // std::cout << trajectory.back() << std::endl;

  // store trajectory into file
  storage.storeTrajectory(trajectory);
}

int main(void) {
  std::cout << "Geodesic Schwarzschild Demo:\n";

  double mass = 1;
  double epsilon = 0.;  // define lighlike particle

  mogli::ode::MetricSchwarzschild MK(mass);  // define Metric
  mogli::ode::Geodesic DGL(epsilon, MK);  // define geodesics for given metric

  double r, phi, theta;

  // starting point
  std::array<double, 3> xi = {r = 3., theta = M_PI / 2., phi = 0.};

  // starting direction
  const double alpha0 = (27.9935 / 180.0) * M_PI;  //* cos(chi);   // 30.722
  const double beta0 = 0;  //(27.9935 / 180.0) * M_PI ;//* sin(chi);  // 30.722

  // in case you want to simulate several trajectories
  const unsigned int n = 1;  // 40;
  std::array<double, n> observ_angle{};
  std::array<double, n> stepsizes{};
  std::array<double, n> tolerances{};

  // different observation angles
  /*for (int i = 0; i < (n / 2); i++) {
    // observ_angle[i]=alpha0 + 0.1*i;
    // observ_angle[i+(n/2)]=-alpha0 - 0.1*i;
    observ_angle[i] = 0.02 * i;
    observ_angle[i + (n / 2)] = -0.02 * i;
  }*/
  observ_angle[0] = alpha0;
  // observ_angle[0]=(41. / 180.0) * M_PI;
  // observ_angle[1]=(28.9 / 180.0) * M_PI;

  // different stepsizes
  for (std::size_t i = 0; i < n; i++) {
    stepsizes[i] = 1e-5 * (pow(1.3, i));
    ;
    tolerances[i] = 1e-15 * (pow(2, i));
    // std::cout << stepsizes[i] << '\t';
  }

  // define storage file name
  std::string filename = "./test.h5";
  //"./trajectory_" + std::to_string(n) + "phi" + std::to_string(r) + ".h5";
  //"./trajectory_" + std::to_string(n) + "tolRK4_adapt_step_1e-3_"
  //+std::to_string(r) + ".h5";
  //"./trajectory_" + std::to_string(n) + "tol_Fehlberg_step_1e-3_" +
  // std::to_string(r) + ".h5";
  //"./trajectory_" + std::to_string(n) + "stepsize_Heun_" + std::to_string(r) +
  //".h5";
  //"./trajectory_45deg" + std::to_string(r) + ".h5";
  mogli::core::Storage storage(filename);
  double stepsize = 1e-3;
  for (std::size_t i = 0; i < n; i++) {
    save_trajectory(xi, alpha0, beta0, 0, stepsize, tolerances[i], MK, DGL,
                    storage);
    // save_trajectory(xi, alpha0, beta0,1e-3, 45, MK, DGL, storage);
    // save_trajectory(xi, alpha0, beta0,1e-3, -45, MK, DGL, storage);
    // save_trajectory(xi, alpha0, beta0,1e-3, 90, MK, DGL, storage);
  }
  std::cout << "Trajectory stored in \n" << filename << std::endl;
}
