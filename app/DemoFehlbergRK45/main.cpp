#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<1> state [[maybe_unused]]) {
  if (time > 2.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  // Test-case is exponential:
  mogli::ode::Exponential<1> expo(-1);

  // Build Fehlberg Runge Kutta 45 solver
  mogli::integrator::FehlbergRK45<1> runge(1.e-2, 1e-7);

  // Set initial value
  mogli::core::StateVector<1> initialValue;
  initialValue[0] = 1.;

  // Integrate thingy

  // with lambda stopping criterion
  auto trajectory = runge.solve(expo, initialValue, stop);

  std::cout << trajectory.back() << std::endl;

  mogli::core::Storage storage(
      "./trajectory_DemoFehlbergRK45.h5");
  storage.storeTrajectory(trajectory);
}