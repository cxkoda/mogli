#include <mogli/ode/Geodesic.h>
#include <mogli/ode/MetricKerr.h>

#include <chrono>
#include <iostream>

#include "mogli/Mogli.h"

void transform_cartesian_spherical(std::array<double, 3> x_old,
                                   std::array<double, 3> v_old,
                                   std::array<double, 3>& x_new,
                                   std::array<double, 3>& v_new) {
  double r =
      sqrt(x_old[0] * x_old[0] + x_old[1] * x_old[1] + x_old[2] * x_old[2]);

  double theta = 0;  // 0-pi
  if (r == 0)
    theta = M_PI / 2;
  else
    theta = acos(x_old[2] / r);  // acos(z/r)

  // theta = M_PI / 2;
  double phi = atan2(x_old[1], x_old[0]);

  double dirX = v_old[0];
  double dirY = v_old[1];
  double dirZ = v_old[2];

  double vr = 0;
  double vphi = 0;
  double vtheta = 0;

  vr = dirX * sin(theta) * cos(phi) + dirY * sin(theta) * sin(phi) +
       dirZ * cos(theta);  // dirR
  vtheta = dirX * cos(theta) * cos(phi) / r + dirY * cos(theta) * sin(phi) / r -
           dirZ * sin(theta) / r;  // dirTheta
  vphi = -dirX * sin(phi) / r / sin(theta) +
         dirY * cos(phi) / r / sin(theta);  // dirPhi

  x_new = {r, theta, phi};
  v_new = {vr, vtheta, vphi};
}

/**
 * define stop condition for solver
 */
bool stop(double time, mogli::core::StateVector<6> state) {
  if (time > 100 || state[0] <= 1) {  // 1.6375425337086245
    return true;
  } else {
    return false;
  }
}

/**
 * simulate and save a trajectory for given starting parameters
 * @param xi ... starting point
 * @param alpha ...starting direction
 * @param beta  ... starting direction
 * @param stepsize
 * @param tolerance ... in case an adaptive solver is used
 * @param MK ... metric
 * @param DGL
 * @param storage
 */
void save_trajectory(std::array<double, 3> xi,
                     double alpha,
                     double beta,
                     double stepsize,
                     double tolerance,
                     mogli::ode::MetricKerr& MK,
                     mogli::ode::Geodesic& DGL,
                     mogli::core::Storage& storage) {
  std::array<double, 4> u_prime = {
      1, -cos(beta) * cos(alpha), sin(beta),
      sin(alpha) * cos(beta)};  // sin(alpha) * cos(beta)};

  MK.setPosition(0, xi);
  auto u = MK.transformLif2Lab(u_prime);

  std::array<double, 3> vi = {u[1], u[2], u[3]};
  double v0 = MK.compute_u0_contra(vi, 0);
  u = MK.compute_u_i_cov(vi, v0);

  std::cout << '\n';
  std::cout << xi[0];
  std::cout << '\t';
  std::cout << xi[1];
  std::cout << '\t';
  std::cout << xi[2];
  std::cout << '\t';
  std::cout << vi[0];
  std::cout << '\t';
  std::cout << vi[1];
  std::cout << '\t';
  std::cout << vi[2];
  std::cout << "\n";

  // Set initial value
  mogli::core::StateVector<6> initialValue;
  initialValue[0] = xi[0];
  initialValue[1] = xi[1];
  initialValue[2] = xi[2];
  initialValue[3] = u[1];
  initialValue[4] = u[2];
  initialValue[5] = u[3];

  // different solver
  // mogli::integrator::RungeKutta4AdaptiveStepsize<6> rungekutta(stepsize,
  //                                                           tolerance);
  mogli::integrator::FehlbergRK45<6> rungekutta(stepsize, tolerance);
  // mogli::integrator::Heun<6> rungekutta(stepsize);
  auto trajectory = rungekutta.solve(DGL, initialValue, stop);

  std::cout << trajectory.back() << std::endl;
  storage.storeTrajectory(trajectory);
}

int main(void) {
  std::cout << "Geodesic Kerr Demo:\n";

  double mass = 1;
  double a = 1;  // Spin parameter

  mogli::ode::MetricKerr MK(mass, a);
  mogli::ode::Geodesic DGL(0.0, MK);

  double r, phi, theta;

  // starting point
  std::array<double, 3> xi = {r = 1. + 1 * sqrt(3), theta = M_PI / 2.,
                              phi = 0.};
  // std::array<double, 3> xi = {r = 10., theta = M_PI / 2., phi = 0.};

  // starting direction
  double alpha0 = (27.9935 / 180.0) * M_PI;  //* cos(chi);   // 30.722
  double beta0 = 0.;  //(27.9935 / 180.0) * M_PI ;//* sin(chi);  // 30.722
  alpha0 = (90 / 180.0) * M_PI;
  beta0 = (151.483499 / 180.0) * M_PI;
  beta0 = (100.0470971 / 180.0) * M_PI;
  // beta0=0;
  const unsigned int n = 1;  // 40;
  std::array<double, n> observ_angle{};
  std::array<double, n> stepsizes{};
  std::array<double, n> tolerances{};

  // different observation angles
  /*for (int i = 0; i < (n / 2); i++) {
    // observ_angle[i]=alpha0 + 0.1*i;
    // observ_angle[i+(n/2)]=-alpha0 - 0.1*i;
    observ_angle[i] = 0.02 * i;
    observ_angle[i + (n / 2)] = -0.02 * i;
  }*/
  observ_angle[0] = alpha0;
  // observ_angle[0]=(41. / 180.0) * M_PI;
  // observ_angle[1]=(28.9 / 180.0) * M_PI;

  // different stepsizes
  for (std::size_t i = 0; i < n; i++) {
    stepsizes[i] = 1e-5 * (pow(1.3, i));
    ;
    tolerances[i] = 1e-15 * (pow(2, i));
    // std::cout << stepsizes[i] << '\t';
  }

  std::string filename = "./trajectory_" + std::to_string(n) + "Kerr_test10" +
                         std::to_string(r) + ".h5";
  //"./trajectory_" + std::to_string(n) + "stepsize_Heun_" + std::to_string(r) +
  //".h5";
  //"./trajectory_45deg" + std::to_string(r) + ".h5";
  mogli::core::Storage storage(filename);
  double stepsize = 1e-3;
  double tolerance = 1e-10;
  for (std::size_t i = 0; i < n; i++) {
    save_trajectory(xi, alpha0, beta0, stepsize, tolerance, MK, DGL, storage);
  }
  std::cout << "Trajectory stored in \n" << filename << std::endl;
}
