#!/bin/bash
#script to start sectorwise camera mapping in parallel.
ncpu=1
camera_width=200
camera_height=100
mass=1
radius=20
angularMomentum=0
camera=0
for((cpu=1;cpu<=ncpu;cpu++))
do
	./CameraMapping $cpu $ncpu $camera_width $camera_height $mass $radius $angularMomentum $camera &
	
done

