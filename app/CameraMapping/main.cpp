#include <math.h>
#include <mogli/ode/Geodesic.h>
#include <mogli/ode/MetricKerr.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>

#include "Camera.h"
#include "mogli/Mogli.h"

double Mass, AngualrMomentum;

bool stop(double time, mogli::core::StateVector<6> state) {
  return state[0] * state[0] - 2 * Mass * state[0] +
                 AngualrMomentum * AngualrMomentum <
             1.e-4 or
         time > 2.e4;
}

bool stop_massZero(double time, [[maybe_unused]] mogli::core::StateVector<6> state) {
  return time > 2.e4;
}

std::array<double, 4> transformCamera2Lif(
    const std::array<double, 3>& cameraVector) {
  std::array<double, 4> lifDirection;
  lifDirection[0] = 1;
  lifDirection[1] = -cameraVector[2];
  lifDirection[2] = -cameraVector[1];
  lifDirection[3] = cameraVector[0];
  return lifDirection;
}

mogli::core::Trajectory<6> calc_trajectory(
    const std::array<double, 4>& lifDirection,
    const std::array<double, 3>& xi,
    double mass,
    double angularMoment) {
  // initialize Metrik
  mogli::ode::MetricKerr MK(mass, angularMoment);
  mogli::ode::Geodesic DGL(0, MK);
  MK.setPosition(0, xi);
  const auto labDirection = MK.transformLif2Lab(lifDirection);
  const std::array<double, 4> ui = MK.compute_u_i_cov(
      {labDirection[1], labDirection[2], labDirection[3]}, labDirection[0]);

  mogli::core::StateVector<6> initialValue;
  initialValue[0] = xi[0];
  initialValue[1] = xi[1];
  initialValue[2] = xi[2];
  initialValue[3] = ui[1];
  initialValue[4] = ui[2];
  initialValue[5] = ui[3];

  mogli::integrator::FehlbergRK45<6> FRK(1.e-2, 1.e-10);
  if (mass == 0) {
    auto trajectory = FRK.solve(DGL, initialValue, stop_massZero);
    mogli::core::Trajectory<6> shortTrajectory;
    shortTrajectory.push_back(trajectory.back());
    return (shortTrajectory);
  } else {
    auto trajectory = FRK.solve(DGL, initialValue, stop);
    mogli::core::Trajectory<6> shortTrajectory;
    shortTrajectory.push_back(trajectory.back());
    return (shortTrajectory);
  }
}

int main([[maybe_unused]] int argc, char** argv) {  // argv: cpu, n_cpus, camera_width,
                                   // camera_height, mass, radius, angular momentum, camera
  // surface of the back of the camera

  double mass,angularMoment,r;
  int camera_width,camera_height,cam,n_cpus;
  size_t cpu = std::stoi(argv[1]);
  n_cpus = std::stoi(argv[2]);
  camera_width = std::stoi(argv[3]);
  camera_height = std::stoi(argv[4]);
  mass = std::stod(argv[5]);
  r = std::stod(argv[6]);
  angularMoment = std::stod(argv[7]);
  cam = std::stoi(argv[8]);
  Mass = mass;
  AngualrMomentum = angularMoment;
  Camera *camera;

  if (cam==0){
    camera = new HoleCamera(1, 1, 1, camera_width, camera_height);
  }
  else {
    camera = new SphericalCamera (camera_width, camera_height);
  }

  cpu = cpu - 1;
  std::cout << "\nResolution: " << camera_width << "x" << camera_height;
  std::cout << "\nProcess:" << cpu + 1 << " " << n_cpus << "\n";
  // start-position
  double theta = M_PI_2;
  double phi = 0.;
  std::array<double, 3> xi = {r, theta, phi};
  std::string filename =
      "./trajectory_cameraMapping_" + std::to_string(cpu + 1) + ".h5";
  mogli::core::Storage storage(filename);

  auto cameraCoordinates = camera->getCameraCoordinates();
  size_t fractionPerCpu = cameraCoordinates.size() / n_cpus;

  mogli::core::Trajectory<6> end_position;
  end_position.reserve(fractionPerCpu);
  for (size_t iTrajectory = cpu * fractionPerCpu;
       iTrajectory < (cpu + 1) * fractionPerCpu; iTrajectory++) {
    std::cout <<"\n"<<iTrajectory;
    const auto cameraDirection =
        camera->getDirection(cameraCoordinates[iTrajectory]);
    const auto lifDirection = transformCamera2Lif(cameraDirection);
    auto point = calc_trajectory(lifDirection, xi, mass, angularMoment);
    end_position.push_back(point.back());

  }
  storage.storeTrajectory(end_position);
  std::ofstream file;    //config file the analysis script
  if (cpu == 0) {
    file.open("config_" + std::to_string(cpu + 1) + ".tsv",
              std::ofstream::out | std::ofstream::trunc);

    if (file) {
      file << camera_width << "\n";
      file << camera_height << "\n";
      file << r << "\n";
      file << n_cpus << "\n";
      file << mass << "\n";
    }
  }
}
