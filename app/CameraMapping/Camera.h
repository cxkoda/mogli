//
// Created by dave on 10.06.20.
//

#ifndef MOGLI_CAMERA_H
#define MOGLI_CAMERA_H

#include <array>
#include <vector>

using CameraCoordinate = std::array<double, 2>;
using CameraPixelDirection = std::array<double, 3>;

/** Generic Camera interface */
class Camera {
 public:
  /**
   * Return the list of available camera coordinates, i.e. the coordinates of
   * the individial pixels.
   */
  virtual std::vector<CameraCoordinate> getCameraCoordinates() = 0;

  /**
   * Compute the direction of the trajectory hitting a given CameraCoordinate
   * inside the camera.
   */
  virtual CameraPixelDirection getDirection(const CameraCoordinate&) = 0;
};

class SphericalCamera final : public Camera {
 public:
  explicit SphericalCamera(size_t nPixelsX, size_t nPixelsY);

  virtual std::vector<CameraCoordinate> getCameraCoordinates() final;

  virtual CameraPixelDirection getDirection(const CameraCoordinate&) final;

 private:
  const size_t nPixelsX, nPixelsY;
};

class HoleCamera final : public Camera {
 public:
  explicit HoleCamera(double holeDistance,
                      double widthX,
                      double widthY,
                      size_t nPixelsX,
                      size_t nPixelsY);
  virtual CameraPixelDirection getDirection(const CameraCoordinate&) final;
  virtual std::vector<CameraCoordinate> getCameraCoordinates() final;

 private:
  const double holeDistance, widthX, widthY;
  const size_t nPixelsX, nPixelsY;
};

#endif  // MOGLI_CAMERA_H
