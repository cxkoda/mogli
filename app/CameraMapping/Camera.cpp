//
// Created by dave on 10.06.20.
//

#include "Camera.h"
#include <cmath>
#include <iostream>

HoleCamera::HoleCamera(double holeDistance,
                       double widthX,
                       double widthY,
                       size_t nPixelsX,
                       size_t nPixelsY)
    : holeDistance(holeDistance),
      widthX(widthX),
      widthY(widthY),
      nPixelsX(nPixelsX),
      nPixelsY(nPixelsY) {}

CameraPixelDirection HoleCamera::getDirection(
    const CameraCoordinate& coordinate) {
  const double normInv = 1. / std::sqrt(holeDistance * holeDistance +
                                        coordinate[0] * coordinate[0] +
                                        coordinate[1] * coordinate[1]);

  std::array<double, 3> direction;
  direction[0] = coordinate[0] * normInv;
  direction[1] = coordinate[1] * normInv;
  direction[2] = holeDistance * normInv;
  return direction;
}

std::vector<CameraCoordinate> HoleCamera::getCameraCoordinates() {
  std::vector<CameraCoordinate> coordinateList;
  coordinateList.reserve(nPixelsX * nPixelsY);

  const double dx = widthX / (nPixelsX - 1);
  const double dy = widthY / (nPixelsY - 1);

  for (size_t iy = 0; iy < nPixelsY; iy++) {
    const double cameraY = iy * dy - 0.5 * widthY;
    for (size_t ix = 0; ix < nPixelsX; ix++) {
      const double cameraX = ix * dx - 0.5 * widthX;
      coordinateList.push_back({cameraX, cameraY});
    }
  }
  return coordinateList;
}

SphericalCamera::SphericalCamera(size_t nPixelsX, size_t nPixelsY)
    : nPixelsX(nPixelsX), nPixelsY(nPixelsY) {}

CameraPixelDirection SphericalCamera::getDirection(
    const CameraCoordinate& coordinate) {  // theta ; phi

  std::array<double, 3> direction;
  direction[0] = sin(coordinate[1]) * sin(coordinate[0]);
  direction[1] = cos(coordinate[0]);
  direction[2] = -cos(coordinate[1]) * sin(coordinate[0]);
  return direction;
}

std::vector<CameraCoordinate> SphericalCamera::getCameraCoordinates() {
  std::vector<CameraCoordinate> coordinateList;
  coordinateList.reserve(nPixelsX * nPixelsY);

  const double dphi = 1. / (nPixelsX - 1) * M_PI * 2;
  const double dtheta = 1. / (nPixelsY - 1) * M_PI;

  for (size_t itheta = 0; itheta < nPixelsY; itheta++) {
    const double cameraTheta = itheta * dtheta;
    for (size_t iphi = 0; iphi < nPixelsX; iphi++) {
      const double cameraPhi = iphi * dphi;
      coordinateList.push_back({cameraTheta, cameraPhi});
    }
  }
  return coordinateList;
}