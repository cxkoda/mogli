#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<1> state [[maybe_unused]]) {
  if (time > 2.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  // Test-case is exponential:
  mogli::ode::Exponential<1> expo(-1);

  // Build Euler solver
  mogli::integrator::ExplicitEuler<1> euler(1.e-2);

  // Set initial value
  mogli::core::StateVector<1> initialValue;
  initialValue[0] = 1.;

  // Integrate thingy

  // with lambda stopping criterion
  auto trajectory = euler.solve(
      expo, initialValue,
      [](double time, mogli::core::StateVector<1> state [[maybe_unused]]) { return time > 2; });

  // with c++ function
  trajectory = euler.solve(expo, initialValue, stop);

  std::cout << trajectory.back() << std::endl;

  mogli::core::Storage storage("./trajectory_DemoExponential.h5");
  storage.storeTrajectory(trajectory);
}