#include <mogli/ode/LotkaVolterra.h>

#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<2> state [[maybe_unused]]) {
  if (time > 2.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  std::cout << "Jaeger-Beute Demo:\n";

  // jaeger_beute dgl:
  mogli::ode::LotkaVolterra JB_dgl(10, 5, 1, 0.1);

  // Set initial value for jaeger beute dgl
  mogli::core::StateVector<2> initialValue;
  initialValue[0] = 100.;
  initialValue[1] = 10.;

  // Build different solver for JB with different stepSize
  mogli::integrator::ExplicitEuler<2> euler1(1.e-2);
  mogli::integrator::ExplicitEuler<2> euler2(5.e-2);
  mogli::integrator::RungeKutta2<2> runge1(1.e-2);
  mogli::integrator::RungeKutta2<2> runge2(5.e-2);
  mogli::integrator::RungeKutta4<2> rungekutta4_1(1.e-2);
  mogli::integrator::RungeKutta4<2> rungekutta4_2(5.e-2);
  mogli::integrator::RungeKutta4AdaptiveStepsize<2> rungekuttaadaptive1(1.e-2,
                                                                        5e-6);
  mogli::integrator::RungeKutta4AdaptiveStepsize<2> rungekuttaadaptive2(5.e-2,
                                                                        1e-4);
  mogli::integrator::FehlbergRK45<2> FRK45_1(1.e-2, 5e-1);
  mogli::integrator::FehlbergRK45<2> FRK45_2(5.e-2, 1);

  // Integrate thingy
  // with lambda stopping criterion for JB
  // auto trajectory = euler.solve(
  //    JB_dgl, initialValue,
  //    [](double time, mogli::core::StateVector<2> state) { return time > 2;
  //    });

  // with c++ function
  auto trajectory_euler1 = euler1.solve(JB_dgl, initialValue, stop);
  auto trajectory_euler2 = euler2.solve(JB_dgl, initialValue, stop);
  auto trajectory_runge1 = runge1.solve(JB_dgl, initialValue, stop);
  auto trajectory_runge2 = runge2.solve(JB_dgl, initialValue, stop);
  auto trajectory_rungekutta4_1 =
      rungekutta4_1.solve(JB_dgl, initialValue, stop);
  auto trajectory_rungekutta4_2 =
      rungekutta4_2.solve(JB_dgl, initialValue, stop);
  auto trajectory_rungekuttaadaptive1 =
      rungekuttaadaptive1.solve(JB_dgl, initialValue, stop);
  auto trajectory_rungekuttaadaptive2 =
      rungekuttaadaptive2.solve(JB_dgl, initialValue, stop);
  auto trajectory_FRK45_1 = FRK45_1.solve(JB_dgl, initialValue, stop);
  auto trajectory_FRK45_2 = FRK45_2.solve(JB_dgl, initialValue, stop);

  // std::cout << trajectory.back()[0] << std::endl;  // Beute gegen 0
  // std::cout << trajectory.back()[1] << std::endl;  // Räuber > 0

  mogli::core::Storage storage_euler1("./trajectory_euler1.h5");
  mogli::core::Storage storage_euler2("./trajectory_euler2.h5");
  mogli::core::Storage storage_runge1("./trajectory_runge1.h5");
  mogli::core::Storage storage_runge2("./trajectory_runge2.h5");
  mogli::core::Storage storage_rungekutta4_1(
      "./trajectory_rungekutta4_1.h5");
  mogli::core::Storage storage_rungekutta4_2(
      "./trajectory_rungekutta4_2.h5");
  mogli::core::Storage storage_rungekuttaadaptive1(
      "./trajectory_rungekuttaadaptive1.h5");
  mogli::core::Storage storage_rungekuttaadaptive2(
      "./trajectory_rungekuttaadaptive2.h5");
  mogli::core::Storage storage_FRK45_1("./trajectory_FRK45_1.h5");
  mogli::core::Storage storage_FRK45_2("./trajectory_FRK45_2.h5");

  storage_euler1.storeTrajectory(trajectory_euler1);
  storage_euler2.storeTrajectory(trajectory_euler2);
  storage_runge1.storeTrajectory(trajectory_runge1);
  storage_runge2.storeTrajectory(trajectory_runge2);
  storage_rungekutta4_1.storeTrajectory(trajectory_rungekutta4_1);
  storage_rungekutta4_2.storeTrajectory(trajectory_rungekutta4_2);
  storage_rungekuttaadaptive1.storeTrajectory(trajectory_rungekuttaadaptive1);
  storage_rungekuttaadaptive2.storeTrajectory(trajectory_rungekuttaadaptive2);
  storage_FRK45_1.storeTrajectory(trajectory_FRK45_1);
  storage_FRK45_2.storeTrajectory(trajectory_FRK45_2);
}