cmake_minimum_required(VERSION 3.4)

# The main program
add_executable(DemoGeodesic_Minkowski main.cpp)
target_link_libraries(DemoGeodesic_Minkowski PRIVATE ${PROJECT_NAME})