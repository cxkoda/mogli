#include <mogli/ode/Geodesic.h>
#include <mogli/ode/MetricMinkowski.h>
#include <mogli/ode/MetricMinkowskiPolar.h>

#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<6> state [[maybe_unused]]) {
  if (time > 5.) {
    return true;
  } else {
    return false;
  }
}

void transform_cartesian_spherical(std::array<double, 3> x_old,
                                   std::array<double, 3> v_old,
                                   std::array<double, 3>& x_new,
                                   std::array<double, 3>& v_new) {
  double r =
      sqrt(x_old[0] * x_old[0] + x_old[1] * x_old[1] + x_old[2] * x_old[2]);

  double theta = 0;  // 0-pi
  if (r == 0)
    theta = M_PI / 2;
  else
    theta = acos(x_old[2] / r);  // acos(z/r)

  // theta = M_PI / 2;
  double phi = atan2(x_old[1], x_old[0]);

  double dirX = v_old[0];
  double dirY = v_old[1];
  double dirZ = v_old[2];

  double vr = 0;
  double vphi = 0;
  double vtheta = 0;

  vr = dirX * sin(theta) * cos(phi) + dirY * sin(theta) * sin(phi) +
       dirZ * cos(theta);  // dirR
  vtheta = dirX * cos(theta) * cos(phi) / r + dirY * cos(theta) * sin(phi) / r -
           dirZ * sin(theta) / r;  // dirTheta
  vphi = -dirX * sin(phi) / r / sin(theta) +
         dirY * cos(phi) / r / sin(theta);  // dirPhi

  x_new = {r, theta, phi};
  v_new = {vr, vtheta, vphi};
}

int main(void) {
  std::cout << "Geodesic Demo:\n";

  // initialize Metrik
  mogli::ode::MetricMinkowski MK;
  mogli::ode::MetricMinkowskiPolar MK_polar;

  // define particle type lightlike
  int epsilon = 0;

  // set start point and start direction in cartesian
  std::array<double, 3> xi = {5.3, 5.3, 0};
  std::array<double, 3> vi = {-1.7, 0, 0};

  // Calculate r theta and phi for spherical coordinates
  std::array<double, 3> xi_polar{};  // = {r, theta, phi};
  std::array<double, 3> vi_polar{};  // = {vr, vtheta, vphi};
  transform_cartesian_spherical(xi, vi, xi_polar, vi_polar);

  // calculate covariant 4-velocity ui for cartesian
  std::array<double, 4> ui{};
  double v0 = 0;
  v0 = MK.compute_u0_contra(vi, epsilon);
  ui = MK.compute_u_i_cov(vi, v0);

  // calculate covariant 4-velocity ui_polar for spherical
  std::array<double, 4> ui_polar{};
  double v0_polar = 0;
  MK_polar.setPosition(0, xi_polar);  // important bc polar metric is not
                                      // initialized with correct parameters
  v0_polar = MK_polar.compute_u0_contra(vi_polar, epsilon);
  ui_polar = MK_polar.compute_u_i_cov(vi_polar, v0_polar);

  // setup geodesic
  mogli::ode::Geodesic DGL(epsilon, MK);
  mogli::ode::Geodesic DGL_polar(epsilon, MK_polar);

  // Set initial value
  mogli::core::StateVector<6> initialValue;
  initialValue[0] = xi[0];
  initialValue[1] = xi[1];
  initialValue[2] = xi[2];
  initialValue[3] = ui[1];
  initialValue[4] = ui[2];
  initialValue[5] = ui[3];

  mogli::core::StateVector<6> initialValue_polar;
  initialValue_polar[0] = xi_polar[0];
  initialValue_polar[1] = xi_polar[1];
  initialValue_polar[2] = xi_polar[2];
  initialValue_polar[3] = ui_polar[1];
  initialValue_polar[4] = ui_polar[2];
  initialValue_polar[5] = ui_polar[3];

  // print initial values for spherical
  std::cout << xi_polar[0];
  std::cout << '\t';
  std::cout << xi_polar[1];
  std::cout << '\t';
  std::cout << xi_polar[2];
  std::cout << '\t';
  std::cout << vi_polar[0];
  std::cout << '\t';
  std::cout << vi_polar[1];
  std::cout << '\t';
  std::cout << vi_polar[2];
  std::cout << "\n";

  mogli::integrator::RungeKutta2<6> rungekutta(1.e-3);
  mogli::integrator::RungeKutta2<6> rungekutta2(1.e-3);

  std::cout << DGL_polar.getRhs(initialValue_polar, 0)[0];  // for testing
  std::cout << "\n";

  // Integrate thingy
  auto trajectory = rungekutta.solve(DGL, initialValue, stop);
  auto trajectory_polar =
      rungekutta2.solve(DGL_polar, initialValue_polar, stop);

  std::cout << trajectory.back() << std::endl;
  std::cout << trajectory_polar.back() << std::endl;

  mogli::core::Storage storage("./trajectory_geo.h5");
  storage.storeTrajectory(trajectory);
  std::cout << "Trajectory stored!\n";

  mogli::core::Storage storage_polar("./trajectory_geo_polar.h5");
  storage_polar.storeTrajectory(trajectory_polar);
  std::cout << "Trajectory stored!\n";
}

/* //old stuff
 * double r = 0;
  double theta = 0;  // 0-pi
  double phi = 0;    //-pi bis pi
  double vr = 0;
  double vphi = 0;
  double vtheta = 0;
  r = sqrt(xi[0] * xi[0] + xi[1] * xi[1] + xi[2] * xi[2]);

  if (r == 0)
    theta = M_PI / 2;
  else
    theta = acos(xi[2] / r);  // acos(z/r)

  // theta = M_PI / 2;
  phi = atan2(xi[1], xi[0]);

  double dirX = vi[0];
  double dirY = vi[1];
  double dirZ = vi[2];

  vr = dirX * sin(theta) * cos(phi) + dirY * sin(theta) * sin(phi) +
       dirZ * cos(theta);  // dirR
  vtheta = dirX * cos(theta) * cos(phi) / r + dirY * cos(theta) * sin(phi) / r -
           dirZ * sin(theta) / r;  // dirTheta
  vphi = -dirX * sin(phi) / r / sin(theta) +
         dirY * cos(phi) / r / sin(theta);  // dirPhi


  std::cout << "\n";
  std::cout << r;
  std::cout << '\t';
  std::cout << theta;
  std::cout << '\t';
  std::cout << phi;
  std::cout << '\t';
  std::cout << vr;
  std::cout << '\t';
  std::cout << vtheta;
  std::cout << '\t';
  std::cout << vphi;
  std::cout << "\n";

 */