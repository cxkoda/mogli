#include <iostream>

#include "mogli/Mogli.h"

bool stop(double time, mogli::core::StateVector<1> state [[maybe_unused]]) {
  if (time > 2.) {
    return true;
  } else {
    return false;
  }
}

int main(void) {
  // Test-case is exponential:
  mogli::ode::Exponential<1> expo(-1);

  // Build Runge-Kutta 4 solver
  mogli::integrator::RungeKutta4<1> rungekutta4(1.e-2);

  // Set initial value
  mogli::core::StateVector<1> initialValue;
  initialValue[0] = 1.;

  // with lambda stopping criterion
  auto trajectory = rungekutta4.solve(
      expo, initialValue,
      [](double time, mogli::core::StateVector<1> state [[maybe_unused]]) { return time > 2; });

  trajectory = rungekutta4.solve(expo, initialValue, stop);

  std::cout << trajectory.back() << std::endl;

  mogli::core::Storage storage("./trajectory_DemoRungeKutta4.h5");
  storage.storeTrajectory(trajectory);
}