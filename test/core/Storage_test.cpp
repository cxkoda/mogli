//
// Created by dave on 03.02.20.
//

#include <mogli/Version.h>
#include <mogli/core/Storage.h>

#include <cmath>
#include <numeric>

#include "doctest_config.h"

TEST_CASE("Write Trajectory Test") {
  const mogli::core::StateVector<2> state1 = {11, 12};
  const mogli::core::StateVector<2> state2 = {21, 22};
  const mogli::core::Trajectory<2> trajectory = {{0, state1}, {1, state2}};

  const std::string filename = std::tmpnam(nullptr);
  {
    mogli::core::Storage storage(filename);
    storage.storeTrajectory(trajectory);
  }

  hid_t hdf5File = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
  hid_t group = H5Gopen2(hdf5File, "/Trajectories", H5P_DEFAULT);
  hid_t dataset = H5Dopen2(group, "0", H5P_DEFAULT);

  std::array<double, 6> data;
  H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
          data.data());
  H5Dclose(dataset);
  H5Gclose(group);
  H5Fclose(hdf5File);

  CHECK_EQ(data[0], trajectory[0].time);
  CHECK_EQ(data[1], trajectory[0].state[0]);
  CHECK_EQ(data[2], trajectory[0].state[1]);
  CHECK_EQ(data[3], trajectory[1].time);
  CHECK_EQ(data[4], trajectory[1].state[0]);
  CHECK_EQ(data[5], trajectory[1].state[1]);
}

TEST_CASE("Write State Vector List Test") {
  const mogli::core::StateVector<2> state1 = {11, 12};
  const mogli::core::StateVector<2> state2 = {21, 22};
  const mogli::core::StateVector<2> state3 = {31, 32};
  mogli::core::StateVectorList<2> list = {state1, state2};
  list.push_back(state3);

  const std::string filename = std::tmpnam(nullptr);
  {
    mogli::core::Storage storage(filename);
    storage.storeStateVectorList(list);
  }

  hid_t hdf5File = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
  hid_t group = H5Gopen2(hdf5File, "/StateVectorList", H5P_DEFAULT);
  hid_t dataset = H5Dopen2(group, "0", H5P_DEFAULT);

  std::array<double, 6> data;
  H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
          data.data());
  H5Dclose(dataset);
  H5Gclose(group);
  H5Fclose(hdf5File);

  CHECK_EQ(data[0], list[0][0]);
  CHECK_EQ(data[1], list[0][1]);
  CHECK_EQ(data[2], list[1][0]);
  CHECK_EQ(data[3], list[1][1]);
  CHECK_EQ(data[4], list[2][0]);
  CHECK_EQ(data[5], list[2][1]);
}

TEST_CASE("Version Attribute Test") {
  const std::string filename = std::tmpnam(nullptr);
  { mogli::core::Storage storage(filename); }

  hid_t hdf5File = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
  hid_t group = H5Gopen2(hdf5File, "/Version", H5P_DEFAULT);

  hid_t attribute = H5Aopen(group, "GitCommitSHA1", H5P_DEFAULT);

  hsize_t dims[1];
  hid_t space = H5Aget_space(attribute);
  H5Sget_simple_extent_dims(space, dims, NULL);
  char** readBuffer = (char**)malloc(dims[0] * sizeof(char*));
  hid_t memtype = mogli::core::h5details::getH5DataType<std::string>();
  H5Aread(attribute, memtype, readBuffer);

  H5Aclose(attribute);
  H5Sclose(space);
  H5Tclose(memtype);
  H5Gclose(group);
  H5Fclose(hdf5File);

  CHECK_EQ(mogli::Version::getGitCommitSHA1(), readBuffer[0]);
}

TEST_CASE("Dataset Attribute Test") {
  const std::string filename = std::tmpnam(nullptr);
  {
    const mogli::core::StateVector<2> state1 = {11, 12};
    const mogli::core::StateVector<2> state2 = {21, 22};
    const mogli::core::StateVectorList<2> list = {state1, state2};
    mogli::core::Storage storage(filename);
    auto dataset = storage.storeStateVectorList(list, "myDataset");

    storage.addAttribute(dataset, "myInt", static_cast<int>(42));
    storage.addAttribute(dataset, "myDouble", static_cast<double>(3.14));
    storage.addAttribute(dataset, "myIntArray", std::array<int, 3>{1, 2, 3});
    storage.addAttribute(dataset, "myDoubleArray",
                         std::vector<double>{1., 2., 3.});
  }

  hid_t hdf5File = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
  hid_t group = H5Gopen2(hdf5File, "/StateVectorList", H5P_DEFAULT);
  hid_t dataset = H5Dopen(group, "myDataset", H5P_DEFAULT);

  {
    int myInt;
    hid_t attribute = H5Aopen(dataset, "myInt", H5P_DEFAULT);
    H5Aread(attribute, mogli::core::h5details::getH5DataType<int>(), &myInt);
    CHECK_EQ(42, myInt);
  }

  {
    double myDouble;
    hid_t attribute = H5Aopen(dataset, "myDouble", H5P_DEFAULT);
    H5Aread(attribute, mogli::core::h5details::getH5DataType<double>(),
            &myDouble);
    CHECK_EQ(3.14, myDouble);
  }

  {
    hsize_t dims[1];
    hid_t attribute = H5Aopen(dataset, "myIntArray", H5P_DEFAULT);
    hid_t type = H5Aget_type(attribute);
    hid_t space = H5Aget_space(attribute);
    H5Sget_simple_extent_dims(space, dims, NULL);

    int npoints = H5Sget_simple_extent_npoints(space);
    std::vector<int> array(npoints);
    H5Aread(attribute, type, array.data());

    H5Aclose(attribute);
    H5Sclose(space);
    H5Tclose(type);

    CHECK_EQ(1, array[0]);
    CHECK_EQ(2, array[1]);
    CHECK_EQ(3, array[2]);
  }

  {
    hsize_t dims[1];
    hid_t attribute = H5Aopen(dataset, "myDoubleArray", H5P_DEFAULT);
    hid_t type = H5Aget_type(attribute);
    hid_t space = H5Aget_space(attribute);
    H5Sget_simple_extent_dims(space, dims, NULL);

    int npoints = H5Sget_simple_extent_npoints(space);
    std::vector<double> array(npoints);
    H5Aread(attribute, type, array.data());

    H5Aclose(attribute);
    H5Sclose(space);
    H5Tclose(type);

    CHECK_EQ(1., array[0]);
    CHECK_EQ(2., array[1]);
    CHECK_EQ(3., array[2]);
  }

  H5Dclose(dataset);
  H5Gclose(group);
  H5Fclose(hdf5File);
}
