//
// Created by dave on 03.02.20.
//

#include "mogli/core/Trajectory.h"

#include <sstream>

#include "doctest_config.h"

TEST_SUITE_BEGIN("Trajectory");

TEST_CASE("Trajectory points can be compared") {
  const mogli::core::TrajectoryPoint<2> point1 = {1, {1, 2}};
  const mogli::core::TrajectoryPoint<2> point2 = {1, {1, 2}};
  const mogli::core::TrajectoryPoint<2> point3 = {2, {1, 2}};
  const mogli::core::TrajectoryPoint<2> point4 = {2, {1, 3}};

  CHECK_EQ(point1, point2);
  CHECK_EQ(point2, point1);
  CHECK_NE(point1, point3);
  CHECK_NE(point1, point4);
  CHECK_NE(point2, point3);
  CHECK_NE(point2, point4);
  CHECK_NE(point3, point4);
}

TEST_CASE("Trajectories can be printed") {
  const mogli::core::Trajectory<2> trajectory = {{0, {1.1, 2.2}},
                                                 {0.1, {3.3, 4.4}}};
  std::stringstream ss;
  ss << trajectory;
  CHECK_EQ(ss.str(), "time=0, state=(1.1, 2.2)\ntime=0.1, state=(3.3, 4.4)");
}

TEST_SUITE_END();