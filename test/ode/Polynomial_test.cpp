//
// Created by dave on 03.02.20.
//

#include "mogli/ode/Polynomial.h"

#include "doctest_config.h"

TEST_CASE("First degree Polynomial") {
  mogli::ode::Polynomial polynomial({1, 1});
  mogli::ode::Polynomial::StateVector initialState({1});

  CHECK_EQ(polynomial.getRhs(initialState, 0.)[0], 1.);
  CHECK_EQ(polynomial.getRhs(initialState, 1.)[0], 2.);
  CHECK_EQ(polynomial.getAnalytical(initialState, 1)[0], 2.5);
}

TEST_CASE("Second degree Polynomial") {
  mogli::ode::Polynomial polynomial({1, 1, 1});
  mogli::ode::Polynomial::StateVector initialState({1});

  CHECK_EQ(polynomial.getRhs(initialState, 0.)[0], 1.);
  CHECK_EQ(polynomial.getRhs(initialState, 1.)[0], 3.);
  CHECK_EQ(polynomial.getRhs(initialState, 2.)[0], 7.);
  CHECK_EQ(polynomial.getAnalytical(initialState, 1)[0],
           doctest::Approx(2.5 + 1. / 3.).epsilon(1e-5));
}

TEST_CASE("Fourth degree Polynomial") {
  mogli::ode::Polynomial polynomial({5., 4., 2., 1., 3.});
  mogli::ode::Polynomial::StateVector initialState({1});

  CHECK_EQ(polynomial.getRhs(initialState, 0)[0], 5.);
  CHECK_EQ(polynomial.getRhs(initialState, 1.1)[0],
           doctest::Approx(17.5433).epsilon(1e-5));
  CHECK_EQ(polynomial.getAnalytical(initialState, 1.1)[0],
           doctest::Approx(11.1397).epsilon(1e-5));
}
