//
// Created by patrick on 10.03.20.
//
#include "mogli/ode/Geodesic.h"

#include <mogli/ode/MetricMinkowskiPolar.h>
#include <mogli/ode/MetricSchwarzschild.h>

#include "doctest_config.h"
#include "mogli/ode/MetricMinkowski.h"

TEST_CASE("Minkowski center") {
  mogli::ode::MetricMinkowski MK;
  mogli::ode::Geodesic DGL(0, MK);
  mogli::ode::Geodesic::StateVector initialState({0, 0, 0, 1, 0, 0});
  CHECK_EQ(DGL.getRhs(initialState, 0.)[0], 1);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[0], 1);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[1], 0);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[1], 0);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[3], 0);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[3], 0);
}

TEST_CASE("Minkowski off-center") {
  mogli::ode::MetricMinkowski MK;
  mogli::ode::Geodesic DGL(0, MK);
  mogli::ode::Geodesic::StateVector initialState({-5, 2, 0, 1, 0, 0});
  CHECK_EQ(DGL.getRhs(initialState, 0.)[0], 1);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[0], 1);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[1], 0);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[1], 0);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[2], 0);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[2], 0);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[3], 0);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[3], 0);
}

TEST_CASE("Minkowski polar") {
  mogli::ode::MetricMinkowskiPolar MK;
  mogli::ode::Geodesic DGL(0, MK);
  mogli::ode::Geodesic::StateVector initialState(
      {7.07107, 1.5708, 0.785398, -0.707107, 0,
       5});  // 5, 5, 0, -1, 0, 0 in cartesian coordinates
  // std::cout << DGL.getRhs(initialState, 0.)[2];
  CHECK_EQ(DGL.getRhs(initialState, 0.)[2], doctest::Approx(0.1).epsilon(1e-2));
}
TEST_CASE("Minkowski polar float") {
  mogli::ode::MetricMinkowskiPolar MK;
  mogli::ode::Geodesic DGL(0, MK);
  mogli::ode::Geodesic::StateVector initialState(
      {7.49533, 1.5708, 0.785398, -1.20208, 0,
       9.01});  // 5.3, 5.3, 0, -1.7, 0, 0 in cartesian coordinates
  // std::cout << DGL.getRhs(initialState, 0.)[2];
  CHECK_EQ(DGL.getRhs(initialState, 0.)[0],
           doctest::Approx(-0.707107).epsilon(1e-2));
  CHECK_EQ(DGL.getRhs(initialState, 0.)[2],
           doctest::Approx(0.0943396).epsilon(1e-2));
}

TEST_CASE("Schwarzschild-RHS[1-3]") {
  mogli::ode::MetricSchwarzschild MK(1);
  mogli::ode::Geodesic DGL(0, MK);
  mogli::ode::Geodesic::StateVector initialState(
      {10, 1.5708, 0, -1.10375, 0, 4.69371});  //
  // std::cout << DGL.getRhs(initialState, 0.)[2];
  CHECK_EQ(DGL.getRhs(initialState, 0.)[0],
           doctest::Approx(-0.722497).epsilon(1e-2));
  CHECK_EQ(DGL.getRhs(initialState, 0.)[2],
           doctest::Approx(0.0384053).epsilon(1e-2));
}

TEST_CASE("Schwarzschild-some_additional_tests") {
  // mogli::ode::MetricMinkowskiPolar MK;
  mogli::ode::MetricSchwarzschild MK(1);
  mogli::ode::Geodesic DGL(0, MK);

  std::array<double, 3> xi = {10, M_PI / 2, 0.2};
  std::array<double, 3> vi = {1, 0, 0};
  std::array<double, 4> u{};  // cov u_µ (0,1,2,3)
  //  std::array<double, 4> du{};  // derivative of u
  double v0 = 0;  // contravariant u_0
  double epsilon = 0;
  double time = 5;

  MK.setPosition(time, xi);
  v0 = MK.compute_u0_contra(vi, epsilon);
  u = MK.compute_u_i_cov(vi, v0);

  // mogli::ode::Geodesic::StateVector initialState(
  //     {xi[0], xi[1], xi[2], u[1], u[2], u[3]});

  //  for (int i = 0; i < 3; i++) {
  //    du[i + 1] = DGL.getRhs(initialState, time)[i + 3];
  //  }

  auto l = MK.getLapse();  // alpha
  // auto s = MK.getShift();               // beta^i
  auto smi = MK.getSpatialMetricInv();  // gamma^ij
  // auto dl = MK.getDLapse();
  // auto ds = MK.getDShift();
  // auto dsmi = MK.getDSpatialMetricInv();

  double u0 = epsilon;
  for (int j = 0; j < 3; j++) {
    for (int k = 0; k < 3; k++) {
      u0 += smi[j][k] * u[j + 1] * u[k + 1];
    }
  }
  u0 = sqrt(u0) / l;  // u0_kontra

  // Kontravariant u0
  CHECK_EQ(u0, v0);
  // Kovariant u0
  CHECK_EQ(double(u[0]), doctest::Approx(-v0 * 0.8).epsilon(1e-4));
  CHECK_EQ(double(u[1]), doctest::Approx(vi[0] * 1.25).epsilon(1e-4));
  CHECK_EQ(double(u[2]), doctest::Approx(0).epsilon(1e-4));
  CHECK_EQ(double(u[3]), doctest::Approx(vi[2] * 100).epsilon(1e-4));
  // lapse
  CHECK_EQ(l, doctest::Approx(sqrt(0.8)).epsilon(1e-4));
}

TEST_CASE("Schwarzschild-RHS[4-6]-Einsteinring") {
  // mogli::ode::MetricMinkowskiPolar MK;
  mogli::ode::MetricSchwarzschild MK(1);
  mogli::ode::Geodesic DGL(0, MK);

  std::array<double, 3> xi = {10., M_PI / 2, 0.};
  // std::array<double, 3> vi = {-0.8830008470363551, 0., 0.046937139253801934};
  // //Berechnet in mathematica
  std::array<double, 3> vi = {-0.883001, 0, 0.0469371};  // Berechnet in Cpp
  std::array<double, 4> u{};                             // cov u_µ (0,1,2,3)
  std::array<double, 4> du{};  // derivative of u (RHS)
  double v0 = 0;               // contravariant u_0
  double epsilon = 0;
  double time = 0;

  MK.setPosition(time, xi);
  v0 = MK.compute_u0_contra(vi, epsilon);
  u = MK.compute_u_i_cov(vi, v0);

  mogli::ode::Geodesic::StateVector initialState(
      {xi[0], xi[1], xi[2], u[1], u[2], u[3]});

  for (int i = 0; i < 3; i++) {
    du[i + 1] = DGL.getRhs(initialState, time)[i + 3];
  }

  CHECK_EQ(double(du[1]), doctest::Approx(-0.004163386334013905)
                              .epsilon(1e-8));  // für anfangswerte aus cpp
  // CHECK_EQ(double(du[1]),
  // doctest::Approx(-0.00416335299085638).epsilon(1e-8)); //für anfangswerte
  // aus mathematica
  CHECK_EQ(double(du[3]), doctest::Approx(0).epsilon(1e-8));
}
TEST_CASE("Schwarzschild-RHS[4-6]-random_init_value") {
  // mogli::ode::MetricMinkowskiPolar MK;
  mogli::ode::MetricSchwarzschild MK(1);
  mogli::ode::Geodesic DGL(0, MK);

  std::array<double, 3> xi = {2.2, 1.3, 3.3};
  std::array<double, 3> vi = {-0.883001, 0.1, 0.0469371};
  std::array<double, 4> u{};   // cov u_µ (0,1,2,3)
  std::array<double, 4> du{};  // derivative of u (RHS)
  double v0 = 0;               // contravariant u_0
  double epsilon = 0;
  double time = 1.1;

  MK.setPosition(time, xi);
  v0 = MK.compute_u0_contra(vi, epsilon);
  u = MK.compute_u_i_cov(vi, v0);

  mogli::ode::Geodesic::StateVector initialState(
      {xi[0], xi[1], xi[2], u[1], u[2], u[3]});

  for (int i = 0; i < 3; i++) {
    du[i + 1] = DGL.getRhs(initialState, time)[i + 3];
  }

  CHECK_EQ(double(du[1]), doctest::Approx(-4.010944862950351).epsilon(1e-10));
  CHECK_EQ(double(du[2]),
           doctest::Approx(0.0002820023578523819).epsilon(1e-10));
  CHECK_EQ(double(du[3]), doctest::Approx(0).epsilon(1e-10));
}