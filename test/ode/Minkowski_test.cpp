//
// Created by dave on 23.04.20.
//
#include <numeric>

#include "doctest_config.h"
#include "mogli/ode/MetricMinkowski.h"

TEST_SUITE_BEGIN("Minkowski");

TEST_CASE("Four Velocity Normalisation") {
  mogli::ode::MetricMinkowski metric;
  metric.setPosition(1.1, {2.2, 3.3, 4.4});
  const std::array<double, 3> contraUi3 = {3.1, 1.2, 2.3};
  const double epsilon = 1;
  const double contraU0 = metric.compute_u0_contra(contraUi3, epsilon);

  const std::array<double, 4> covUi4 =
      metric.compute_u_i_cov(contraUi3, contraU0);
  const std::array<double, 4> contraUi4 = {contraU0, contraUi3[0], contraUi3[1],
                                           contraUi3[2]};

  const double contraction = std::inner_product(
      contraUi4.begin(), contraUi4.end(), covUi4.begin(), 0.);

  CHECK_EQ(contraction, doctest::Approx(-epsilon).epsilon(1e-12));
}

TEST_SUITE_END();