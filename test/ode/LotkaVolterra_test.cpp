//
// Created by patrick on 10.03.20.
//
#include "mogli/ode/LotkaVolterra.h"

#include "doctest_config.h"

TEST_CASE("TestCaseJB") {
  mogli::ode::LotkaVolterra DGL(5, 1, 1, 1);
  mogli::ode::LotkaVolterra::StateVector initialState({100, 10});
  CHECK_EQ(DGL.getRhs(initialState, 0.)[0], -500);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[0], -500);
  CHECK_EQ(DGL.getRhs(initialState, 0.)[1], 990);
  CHECK_EQ(DGL.getRhs(initialState, 1.)[1], 990);
}