//
// Created by dave on 03.02.20.
//
#include "mogli/ode/Exponential.h"

#include "doctest_config.h"

TEST_CASE("Positive lambda") {
  mogli::ode::Exponential<1> exponential(1.3);
  mogli::ode::Exponential<1>::StateVector initialState({2});

  CHECK_EQ(exponential.getRhs(initialState, 0.)[0], 2.6);
  CHECK_EQ(exponential.getRhs(initialState, 1.)[0], 2.6);
  CHECK_EQ(exponential.getAnalytical(initialState, 1)[0],
           doctest::Approx(7.33859).epsilon(1e-5));
}

TEST_CASE("Negative lambda") {
  mogli::ode::Exponential<1> exponential(-2.7);
  mogli::ode::Exponential<1>::StateVector initialState({1});

  CHECK_EQ(exponential.getRhs(initialState, 0.)[0], -2.7);
  CHECK_EQ(exponential.getRhs(initialState, 1.)[0], -2.7);
  CHECK_EQ(exponential.getAnalytical(initialState, 1)[0],
           doctest::Approx(0.0672055).epsilon(1e-5));
}
