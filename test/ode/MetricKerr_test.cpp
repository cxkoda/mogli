//
// Created by dave on 23.04.20.
//
#include "mogli/ode/MetricKerr.h"

#include <mogli/ode/MetricSchwarzschild.h>

#include <numeric>

#include "doctest_config.h"
#include "mogli/ode/MetricMinkowskiPolar.h"

TEST_SUITE_BEGIN("Kerr Metrik");

TEST_CASE("Flat for small mass without rotation") {
  // This should effectively yield the same as the minkowski metric in spherical
  // coordinate
  mogli::ode::MetricKerr metric(1e-100, 1e-150);  // 1e-100?
  mogli::ode::MetricMinkowskiPolar minkowski;

  metric.setPosition(1.1, {2.2, 1.3, 3.3});
  minkowski.setPosition(1.1, {2.2, 1.3, 3.3});

  CHECK_EQ(metric.getLapse(), minkowski.getLapse());

  for (size_t idx = 0; idx < 3; idx++) {
    CHECK_EQ(metric.getShift()[idx],
             doctest::Approx(minkowski.getShift()[idx]));
  }

  for (size_t idx = 0; idx < 3; idx++) {
    for (size_t jdx = 0; jdx < 3; jdx++) {
      CHECK_EQ(metric.getSpatialMetric()[idx][jdx],
               doctest::Approx(minkowski.getSpatialMetric()[idx][jdx]));
    }
  }
}

TEST_CASE("Schwarzschild solution without rotation") {
  // This should effectively yield the same as the schwarzschild metric in
  // spherical coordinate
  mogli::ode::MetricKerr metric(1, 0);
  mogli::ode::MetricSchwarzschild schwarzschild(1);

  metric.setPosition(1.1, {2.2, 1.3, 3.3});
  schwarzschild.setPosition(1.1, {2.2, 1.3, 3.3});

  CHECK_EQ(metric.getLapse(), schwarzschild.getLapse());

  for (size_t idx = 0; idx < 3; idx++) {
    CHECK_EQ(metric.getShift()[idx],
             doctest::Approx(schwarzschild.getShift()[idx]));
    CHECK_EQ(metric.getDLapse()[idx], schwarzschild.getDLapse()[idx]);
  }

  for (size_t idx = 0; idx < 3; idx++) {
    for (size_t jdx = 0; jdx < 3; jdx++) {
      CHECK_EQ(metric.getSpatialMetric()[idx][jdx],
               doctest::Approx(schwarzschild.getSpatialMetric()[idx][jdx]));
      CHECK_EQ(metric.getSpatialMetricInv()[idx][jdx],
               doctest::Approx(schwarzschild.getSpatialMetricInv()[idx][jdx]));
      CHECK_EQ(metric.getDShift()[idx][jdx],
               doctest::Approx(schwarzschild.getDShift()[idx][jdx]));
      for (size_t kdx = 0; kdx < 3; kdx++) {
        CHECK_EQ(metric.getDSpatialMetricInv()[idx][jdx][kdx],
                 doctest::Approx(
                     schwarzschild.getDSpatialMetricInv()[idx][jdx][kdx]));
      }
    }
  }
  for (size_t idx = 0; idx < 4; idx++) {
    CHECK_EQ(metric.transformLif2Lab({1.1, 2.2, 3.3, 4.4})[idx],
             doctest::Approx(
                 schwarzschild.transformLif2Lab({1.1, 2.2, 3.3, 4.4})[idx]));
  }
}

TEST_CASE("Four Velocity Normalisation for small") {
  mogli::ode::MetricKerr metric(1, 0.01);
  metric.setPosition(1.1, {2.2, 1.3, 3.3});
  const std::array<double, 3> contraUi3 = {3.1, 2.3, 1.2};
  const double epsilon = 1;
  const double contraU0 = metric.compute_u0_contra(contraUi3, epsilon);

  // CHECK_EQ(contraU0, doctest::Approx(39.07878373162865).epsilon(1e-12)); not
  // working somehow

  const std::array<double, 4> covUi4 =
      metric.compute_u_i_cov(contraUi3, contraU0);
  const std::array<double, 4> contraUi4 = {contraU0, contraUi3[0], contraUi3[1],
                                           contraUi3[2]};

  const double contraction = std::inner_product(
      contraUi4.begin(), contraUi4.end(), covUi4.begin(), 0.);

  CHECK_EQ(contraction, doctest::Approx(-epsilon).epsilon(1e-12));
}

TEST_CASE("Metric Quantities on given Position") {
  mogli::ode::MetricKerr metric(1., 0.01);
  metric.setPosition(1.1, {2.2, 1.3, 3.3});

  CHECK_EQ(metric.getLapse(), doctest::Approx(0.3015398610072183));

  for (size_t idx = 0; idx < 2; idx++) {
    CHECK_EQ(metric.getShift()[idx], 0);
  }
  CHECK_EQ(metric.getShift()[2], doctest::Approx(-0.0018782126654898628));

  for (size_t idx = 0; idx < 3; idx++) {
    for (size_t jdx = 0; jdx < 3; jdx++) {
      if (idx != jdx) {
        CHECK_EQ(metric.getSpatialMetric()[idx][jdx], 0);
        CHECK_EQ(metric.getSpatialMetricInv()[idx][jdx], 0);
      }
    }
  }
  CHECK_EQ(metric.getSpatialMetric()[0][0],
           doctest::Approx(10.997516826999162));
  CHECK_EQ(metric.getSpatialMetric()[1][1], doctest::Approx(4.840007155562333));
  CHECK_EQ(metric.getSpatialMetric()[2][2],
           doctest::Approx(4.4938419919256285));

  CHECK_EQ(metric.getSpatialMetricInv()[0][0],
           doctest::Approx(0.09092961763377135));
  CHECK_EQ(metric.getSpatialMetricInv()[1][1],
           doctest::Approx(0.20661126478930086));
  CHECK_EQ(metric.getSpatialMetricInv()[2][2],
           doctest::Approx(0.22252673809999635));
  /*
    for (size_t idx = 1; idx < 3; idx++) {
      CHECK_EQ(metric.getDLapse()[idx], 0);
    }
    CHECK_EQ(metric.getDLapse()[0], doctest::Approx(0.6852530558585533));

    for (size_t idx = 0; idx < 3; idx++) {
      for (size_t jdx = 0; jdx < 3; jdx++) {
        if (idx != jdx) {
          CHECK_EQ(metric.getDShift()[idx][jdx], 0);
        }
      }
    }

    for (size_t iDeriv = 0; iDeriv < 3; iDeriv++) {
      for (size_t idx = 0; idx < 3; idx++) {
        for (size_t jdx = 0; jdx < 3; jdx++) {
          if (iDeriv == 0 && idx == 0 && jdx == 0) {
            CHECK_EQ(metric.getDSpatialMetricInv()[iDeriv][idx][jdx],
                     doctest::Approx(0.4132231404958677));
          } else if (iDeriv == 0 && idx == 1 && jdx == 1) {
            CHECK_EQ(metric.getDSpatialMetricInv()[iDeriv][idx][jdx],
                     doctest::Approx(-0.18782870022539438));
          } else if (iDeriv == 0 && idx == 2 && jdx == 2) {
            CHECK_EQ(metric.getDSpatialMetricInv()[iDeriv][idx][jdx],
                     doctest::Approx(-0.2023047421495956));
          } else if (iDeriv == 1 && idx == 2 && jdx == 2) {
            CHECK_EQ(metric.getDSpatialMetricInv()[iDeriv][idx][jdx],
                     doctest::Approx(-0.1235585159384303));
          } else

          {
            CHECK_EQ(metric.getDSpatialMetricInv()[iDeriv][idx][jdx], 0);
          }
        }
      }
    }
    */
}

TEST_SUITE_END();