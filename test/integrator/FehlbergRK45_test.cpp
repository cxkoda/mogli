//
// Created by Dominik on 08.04.20
//

#include "mogli/integrator/FehlbergRK45.h"

#include <cmath>
#include <memory>
#include <numeric>

#include "doctest_config.h"
#include "mogli/ode/Exponential.h"
#include "mogli/ode/Polynomial.h"

TEST_SUITE_BEGIN("FehlbergRK45");

TEST_CASE("Fourth degree polynomial") {
  mogli::ode::Polynomial polynomial({-1.1, 5.3, 3.2, -8.6, 7.2});
  const double stepsize = 1;
  mogli::integrator::FehlbergRK45<1> FRK45(stepsize, 1e-8);
  mogli::core::StateVector<1> initialState({1.3});

  // make a single step
  auto trajectory = FRK45.solve(
      polynomial, initialState,
      [](double time, mogli::core::StateVector<1> state[[maybe_unused]]) {
        return time > 0;
      });

  CHECK_EQ(trajectory.back().state[0],
           doctest::Approx(polynomial.getAnalytical(initialState,
                                                    trajectory.back().time)[0])
               .epsilon(1e-8));
}

TEST_CASE("Check RK4 and RK5 individually") {
  mogli::ode::Exponential<1> exponential(-1.3);
  mogli::core::StateVector<1> initialState({1.1});
  const double stepsize1 = 0.015625;
  const double stepsize2 = 0.5 * stepsize1;
  mogli::integrator::FehlbergRK45<1> FRK45_1(stepsize1, 1e-8);
  mogli::integrator::FehlbergRK45<1> FRK45_2(stepsize2, 1e-8);

  mogli::core::Trajectory<1> trajectoryRK4_1, trajectoryRK4_2;
  trajectoryRK4_1.push_back({0., initialState});
  trajectoryRK4_2.push_back({0., initialState});
  mogli::core::Trajectory<1> trajectoryRK5_1, trajectoryRK5_2;
  trajectoryRK5_1.push_back({0., initialState});
  trajectoryRK5_2.push_back({0., initialState});

  while (trajectoryRK4_1.back().time < 1) {
    trajectoryRK4_1.push_back(
        {trajectoryRK4_1.back().time + stepsize1,
         FRK45_1.calcSolutionRK4(
             FRK45_1.calcRhs(exponential, trajectoryRK4_1.back().state,
                             trajectoryRK4_1.back().time, stepsize1),
             trajectoryRK4_1.back().state, stepsize1)});

    trajectoryRK5_1.push_back(
        {trajectoryRK5_1.back().time + stepsize1,
         FRK45_1.calcSolutionRK5(
             FRK45_1.calcRhs(exponential, trajectoryRK5_1.back().state,
                             trajectoryRK5_1.back().time, stepsize1),
             trajectoryRK5_1.back().state, stepsize1)});
  }

  while (trajectoryRK4_2.back().time < 1) {
    trajectoryRK4_2.push_back(
        {trajectoryRK4_2.back().time + stepsize2,
         FRK45_2.calcSolutionRK4(
             FRK45_2.calcRhs(exponential, trajectoryRK4_2.back().state,
                             trajectoryRK4_2.back().time, stepsize2),
             trajectoryRK4_2.back().state, stepsize2)});

    trajectoryRK5_2.push_back(
        {trajectoryRK5_2.back().time + stepsize2,
         FRK45_2.calcSolutionRK5(
             FRK45_2.calcRhs(exponential, trajectoryRK5_2.back().state,
                             trajectoryRK5_2.back().time, stepsize2),
             trajectoryRK5_2.back().state, stepsize2)});
  }
  const auto analytical1 =
      exponential.getAnalytical(initialState, trajectoryRK4_1.back().time);
  const auto analytical2 =
      exponential.getAnalytical(initialState, trajectoryRK4_2.back().time);
  const double error4_1 = trajectoryRK4_1.back().state[0] - analytical1[0];
  const double error4_2 = trajectoryRK4_2.back().state[0] - analytical2[0];
  const double order4 =
      log10(error4_2 / error4_1) / log10(stepsize2 / stepsize1);
  CHECK_EQ(order4, doctest::Approx(4).epsilon(1e-2));

  const double error5_1 = trajectoryRK5_1.back().state[0] - analytical1[0];
  const double error5_2 = trajectoryRK5_2.back().state[0] - analytical2[0];
  const double order5 =
      log10(error5_2 / error5_1) / log10(stepsize2 / stepsize1);
  CHECK_EQ(order5, doctest::Approx(5).epsilon(1e-2));
}

TEST_CASE("10 Dimensional Test") {
  constexpr std::size_t dim = 10;
  mogli::integrator::FehlbergRK45<dim> FRK45(1e-1, 1e-8);

  // stop after one step
  auto stoppingCriterion = [](double time, auto state[[maybe_unused]]) {
    return time > 0;
  };

  mogli::ode::Exponential<dim> exponential(-1);
  mogli::core::StateVector<dim> initialState;
  std::fill(initialState.begin(), initialState.end(), 1);

  const auto solution =
      FRK45.solve(exponential, initialState, stoppingCriterion).back().state;

  for (const auto component : solution) {
    CHECK_EQ(solution[0], component);
  }
}

TEST_SUITE_END();
