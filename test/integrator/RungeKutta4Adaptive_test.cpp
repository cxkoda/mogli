//
// Created by Dominik on 05.04.20
//

#include <cmath>
#include <memory>
#include <numeric>

#include "doctest_config.h"
#include "mogli/integrator/RungeKutta4AdaptiveStepsize.h"
#include "mogli/ode/Exponential.h"
#include "mogli/ode/Polynomial.h"

TEST_SUITE_BEGIN("RungeKutta4Adaptive");

TEST_CASE("Fourth degree polynomial") {
  mogli::ode::Polynomial polynomial({-1.1, 5.3, 3.2, -8.6, 1.3});
  const double stepsize = 1;
  mogli::integrator::RungeKutta4AdaptiveStepsize<1> rk4adaptive(stepsize, 1e-8);
  mogli::core::StateVector<1> initialState({1.3});

  // make a single step
  auto trajectory = rk4adaptive.solve(
      polynomial, initialState,
      [](double time, mogli::core::StateVector<1> state[[maybe_unused]]) {
        return time > 0;
      });

  CHECK_EQ(trajectory.back().state[0],
           doctest::Approx(polynomial.getAnalytical(initialState,
                                                    trajectory.back().time)[0])
               .epsilon(1e-8));
}

TEST_CASE("10 Dimensional Test") {
  constexpr std::size_t dim = 10;
  mogli::integrator::RungeKutta4AdaptiveStepsize<dim> rk4adaptive(1e-1, 1e-8);

  // stop after one step
  auto stoppingCriterion = [](double time, auto state[[maybe_unused]]) {
    return time > 0;
  };

  mogli::ode::Exponential<dim> exponential(-1);
  mogli::core::StateVector<dim> initialState;
  std::fill(initialState.begin(), initialState.end(), 1);

  const auto solution =
      rk4adaptive.solve(exponential, initialState, stoppingCriterion)
          .back()
          .state;

  for (const auto component : solution) {
    CHECK_EQ(solution[0], component);
  }
}

TEST_SUITE_END();
