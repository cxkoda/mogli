cmake_minimum_required(VERSION 3.4)

# The component/sublibrary name is given by the name of the directory
get_filename_component(COMPONENT_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

# The test is named after its component by default
set(TEST_NAME ${COMPONENT_NAME}_test)

# Add the test source files here
add_executable(${TEST_NAME}
    main.cpp
    ExplicitEuler_test.cpp
    FehlbergRK45_test.cpp
    Heun_test.cpp
    RungeKutta2_test.cpp
    RungeKutta4_test.cpp
    RungeKutta4Adaptive_test.cpp
    )

# The test is linked against the final library
target_link_libraries(${TEST_NAME}
    ${PROJECT_NAME}
    doctest
    )

# Add test includes
target_include_directories(${TEST_NAME}
    PRIVATE ${PROJECT_SOURCE_DIR}/test/include
    )

# Finally add the test target
add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})


# Coverage ######################################################################
if (ENABLE_COVERAGE)
  # Link tests with the coverage library
  target_link_libraries(${TEST_NAME} gcov)
endif (ENABLE_COVERAGE)