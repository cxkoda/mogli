//
// Created by Dominik on 23.03.20
//

#include <mogli/integrator/Heun.h>

#include <cmath>
#include <memory>
#include <numeric>

#include "doctest_config.h"
#include "mogli/ode/Exponential.h"
#include "mogli/ode/Polynomial.h"

TEST_SUITE_BEGIN("Heun");

TEST_CASE("First degree polynomial") {
  mogli::ode::Polynomial polynomial({-1.1, 5.3});
  const double stepsize = 1.;
  mogli::integrator::Heun<1> heun(stepsize);
  mogli::core::StateVector<1> initialState({1.3});

  // make a single step
  auto trajectory = heun.solve(
      polynomial, initialState,
      [](double time, mogli::core::StateVector<1> state[[maybe_unused]]) {
        return time > 0;
      });

  CHECK_EQ(trajectory.back().state[0],
           doctest::Approx(polynomial.getAnalytical(initialState, stepsize)[0])
               .epsilon(1e-8));
}

TEST_CASE("Order Test") {
  mogli::ode::Exponential<1> exponential(-1.3);
  mogli::core::StateVector<1> initialState({1.1});
  constexpr double stopTime = 1;
  auto stoppingCriterion =
      [](double time, mogli::core::StateVector<1> state[[maybe_unused]]) {
        return time >= stopTime;
      };

  const double stepsize1 = 0.015625;
  const double stepsize2 = 0.5 * stepsize1;

  mogli::integrator::Heun<1> solver1(stepsize1);
  mogli::integrator::Heun<1> solver2(stepsize2);

  const auto numerical1 =
      solver1.solve(exponential, initialState, stoppingCriterion).back();
  const auto numerical2 =
      solver2.solve(exponential, initialState, stoppingCriterion).back();

  const auto analytical1 =
      exponential.getAnalytical(initialState, numerical1.time);
  const auto analytical2 =
      exponential.getAnalytical(initialState, numerical2.time);

  const double error1 = numerical1.state[0] - analytical1[0];
  const double error2 = numerical2.state[0] - analytical2[0];

  const double order = log10(error2 / error1) / log10(stepsize2 / stepsize1);

  CHECK_EQ(order, doctest::Approx(2).epsilon(1e-2));
}

TEST_SUITE_END();
